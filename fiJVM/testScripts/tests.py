import subprocess
import unittest

exePath = """/home/fi/PROJECTS/eclipse-cdt-workspace/fiJVM/Debug/fiJVM.exe"""
classPath = """/home/fi/PROJECTS/eclipse-cdt-workspace/fiJVMTests/bin/"""

def getOutput(className):
	cmd = exePath + " " + classPath + " " + className
	result = subprocess.check_output(cmd, shell=True)
	return result.decode("utf-8").replace('\r\n', '\n')

def main():
	unittest.main()

class FiJVMTests(unittest.TestCase):
	def test1(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test1")
		self.assertTrue(res == "123456\n")
	def test2(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test2")
		self.assertTrue(res == "10\n")
	def test3(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test3")
		self.assertTrue(res == "-350\n")
	def test4(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test4")
		self.assertTrue(res == "-45\n-560\n-3\n35\n-3\n-33\n")
	def test5(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test5")
		self.assertTrue(res == "-456450\n2958888\n")
	def test6(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test6")
		self.assertTrue(res == "-25454\n-555\n-34\n")
	def test7(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test7")
		self.assertTrue(res == "-30\n-530\n-46\n-732\n")
	def test8(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test8")
		self.assertTrue(res == "-2\n-3\n")
	def test9(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test9")
		self.assertTrue(res == "-222222\n-333333\n-11111\n")
	def test10(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test10")
		self.assertTrue(res == "-12345\n-87829051\n-628572\n-111111\n" +
                                "-2222222\n-333333\n-314542\n-314542\n-314542\n")
	def test11(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test11")
		self.assertTrue(res == "465\n")
	def test12(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test12")
		self.assertTrue(res == "45\n45\n-\n45.00\n45.00\n45\n")
	def test13(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test13")
		self.assertTrue(res == "976.3039\n")
	def test14(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test14")
		self.assertTrue(res == "5\n45\n")
	def test15(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test15")
		self.assertTrue(res == "2305843009213693936\n-16\n32945\n12366\n" +
                                "45311\n2820\n1444288\n5641\n-45134\n32591\n57677\n3\n566115762\n7505\nF\n")
	def test16(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test16")
		self.assertTrue(res == "-8688.58\n-8002.11\n2864408.41\n24.31\n" +
                                "8345.34\n-107.72\n-8345.34\n10\nS\n")
	def test17(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test17")
		self.assertTrue(res == "A\nB\nC\n-1111\n-44444\n10\n")
	def test18(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test18")
		self.assertTrue(res == "false\n0\n0\n0.0\n0\n")
	def test19(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test19")
		self.assertTrue(res == "fizz\n")
	def test20(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test20")
		self.assertTrue(res == "-34563.453\n")
	def test21(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test21")
		self.assertTrue(res == "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n")
	def test22(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test22")
		self.assertTrue(res == "-5\n-4\n-3\n-2\n-1\n0\n1\n2\n3\n4\n")
	def test23(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test23")
		self.assertTrue(res == "-555\n-55\n")
	def test24(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test24")
		self.assertTrue(res == "-5\n-4\n-3\n-2\n-1\n0\n1\n2\n3\n4\n")
	def test25(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test25")
		self.assertTrue(res == "false\nfalse\ntrue\nfalse\nfalse\ntrue\nfalse\nfalse\ntrue\nfalse\n")
	def test26(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test26")
		self.assertTrue(res == "ABCDEFGHIJ")
	def test27(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test27")
		self.assertTrue(res == "-5.0\n-4.0\n-3.0\n-2.0\n-1.0\n0.0\n1.0\n2.0\n3.0\n4.0\n")
	def test28(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test28")
		self.assertTrue(res == "-45.0\n-5.0\n-4.0\n-9.0\n-12.0")
	def test29(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test29")
		self.assertTrue(res == "fizzbuzz\n")
	def test30(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test30")
		self.assertTrue(res == "true true true true false true\n")
	def test31(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test31")
		self.assertTrue(res == "Exception..\neho\n")
	def test32(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test32")
		self.assertTrue(res == "ExcpConstr eee\nfinally\n")
	def test33(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test33")
		self.assertTrue(res == "inner_static_class\ninner_non_static_class\n")
	def test34(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test34")
		self.assertTrue(res == "true\nfalse\n")
	def test35(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test35")
		self.assertTrue(res == "interface_method\n")
	def test36(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test36")
		data=""
		with open ("test36.txt", "r") as myfile:
                        data=myfile.read()
		self.assertTrue(res == data)
	def test37(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test37")
		data=""
		with open ("test37.txt", "r") as myfile:
                        data=myfile.read()
		self.assertTrue(res == data)
	def test38(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test38")
		self.assertTrue(res == "true\n")
	def test39(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test39")
		data=""
		with open ("test39.txt", "r") as myfile:
                        data=myfile.read()
		self.assertTrue(res == data)
	def test40(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test40")
		self.assertTrue(res == "200\n")
	def test41(self):
		res = getOutput("com.geecodemonkeys.fijvm.tests.source.Test41")
		self.assertTrue(res == "before wait\nafter notify.\nsuper after wait.\n")
		


if __name__ == "__main__":
	main()
