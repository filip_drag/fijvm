#include <pthread.h>
#include <stdio.h>
#include "threads.h"
#include "util.h"

int threadCount = 0;
int maxThreadNumber = 0;
#define MAX_THREAD_COUNT (1024)

Thread* threadList[MAX_THREAD_COUNT];

pthread_mutex_t threadListMutex = PTHREAD_MUTEX_INITIALIZER;

Thread* createThread() {
	pthread_mutex_lock(&threadListMutex);

	Thread* thread = malloc(sizeof(Thread));
	thread->callStackPointer = -1;
	thread->nextThread = NULL;
	thread->state = THREAD_NOT_STARTED;
	threadList[maxThreadNumber] = thread;
	threadCount++;
	maxThreadNumber++;
	if (maxThreadNumber >= 1024) {
		printf("Its time for thread garbage collection!\n");
	}
	pthread_mutex_unlock (&threadListMutex);
	return thread;
}

void deleteThread(Thread* thread) {
	int i;
	pthread_mutex_lock(&threadListMutex);

	for (i = 0; i < maxThreadNumber; i++) {
		if (thread == threadList[i]) {
			free(thread);
			threadList[i] = NULL;
			threadCount--;
			break;
		}
	}
	pthread_mutex_unlock (&threadListMutex);
}

void initThreading() {
	memset(threadList, 0, MAX_THREAD_COUNT * sizeof(threadList[0]));
}

void destroyThreading() {
	pthread_mutex_destroy(&threadListMutex);
}
