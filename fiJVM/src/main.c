#include <stdlib.h>
#include <string.h>
#include "class.h"
#include "classes.h"
#include "cpu.h"
#include "util.h"
#include "printf.h"
#include "string-pool.h"
#include "jni-methods.h"
#include "threads.h"
#include "debugger.h"

int DEBUGGER_ON;

void initVM();

int main(int argv, char **argc) {
	setvbuf (stdout, NULL, _IONBF, 0);

	//tests();
	//return 0;

	if (argv < 2 ) {
		PRINTF("Path arguments is missing!");
		return 1;
	}
	//argc[1] path argument
	//argc[2] main class name
	char* className = argc[2];
	size_t classNameLength = strlen(className);
	size_t i;
	for(i = 0; i < classNameLength; i++) {
		if (className[i] == '.') {
			className[i] = '/';
		}
	}
	DEBUGGER_ON = 0;
	setClassSearchPath(argc[1]);
	setSystemClassSearchPath("/home/fi/PROJECTS/eclipse-cdt-workspace/fiJVM/classlib/");

	initVM();

	startMain(className);

	if (DEBUGGER_ON) {
		closeDebugger();
	}

	destroyStringPool();
	freeAllClasses();
	destroyJni();
	destroyThreading();
	
	return 0;
}

void initVM() {
	initThreading();
	initJni();
	initStringPool();

	if (DEBUGGER_ON) {
		initDebugger(5052);
	}
}


