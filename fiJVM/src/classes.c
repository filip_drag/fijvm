#include <string.h>
#include <stdlib.h>
#include "classes.h"
#include "strmap.h"
#include "static-heap.h"
#include "cpu.h"
#include "util.h"
#include "access-flags.h"
#include "strmap.h"
#include "descriptors.h"
#include "heap.h"
#include "jni-helper.h"
#include "threads.h"
#include "debugger.h"
#include "java-object-consts.h"


size_t classEntryCount = 0;
ClassEntry classList[MAX_CLASS_COUNT];
char paths[3][MAX_PATH_CHARS];
#define BUFF_SIZE 1024
char buff[BUFF_SIZE];
pthread_mutex_t bootstrapMutex;
pthread_mutex_t classListMutex;
StrMap *mutexMap;

StrMap *primitiveClasses;
pthread_mutex_t primitiveClassesMutex = PTHREAD_MUTEX_INITIALIZER;

#define TEMP_BUFF_SIZE (sizeof(uintptr_t) * 2 + 1)

//the list of the classes which clinit methods where skipped in phase 1
Class* skippedClasses[500];
//the counter of the skipped classes
int skippedClassesCounter = 0;

int classLoadingPhase;

//the list of the classes which clinit methods where skipped in phase 1
Class* classInitSkippedClasses[500];
//the counter of the skipped classes
int classInitSkippedClassesCounter = 0;

JavaObject* createJavaLangClass(Thread* threadId, char* className, int returnAddress);
pthread_mutex_t* getClassLoadMutex(const char* name);
void deleteMutex(const char *key, const char *value, const void *obj);
JavaObject* createIfNotExistsPrimitiveClassInner(Thread* thread, char* fullName);
Class* createPrimitiveArrayClass(Thread* thread, const char* name);

void setSystemClassSearchPath(char* pathParam) {
	memset(paths[0], 0, MAX_PATH_CHARS * sizeof(char));
	strcat(paths[0], pathParam);
}

void setClassSearchPath(char* pathParam) {
	memset(paths[1], 0, MAX_PATH_CHARS * sizeof(char));
	strcat(paths[1], pathParam);
}

Class* findClassByQualifiedName(const char* name) {
	size_t i;
	Class* clazz = NULL;

	pthread_mutex_lock(&classListMutex);

	for (i = 0; i < classEntryCount; i++) {
		if (strcmp(classList[i].className, name) == 0) {
			clazz = classList[i].clazz;
			break;
		}
	}
	pthread_mutex_unlock(&classListMutex);

	return clazz;
}

//does not initialize super interfaces
Class* loadClassByQualifiedName(Thread* threadId, const char* name, int returnAddress) {
	Class* clazz = NULL;
	Class* loadedClass = findClassByQualifiedName(name);
	if (loadedClass != NULL) {
		return loadedClass;
	}

	if (name[0] == '[') {
		return createPrimitiveArrayClass(threadId, name);
	}

	pthread_mutex_t* mutex = getClassLoadMutex(name);

	pthread_mutex_lock(mutex);

	//double check if the value has changed
	loadedClass = findClassByQualifiedName(name);
	if (loadedClass != NULL) {
		clazz = loadedClass;
		goto unlockAndReturn;
	}

	clazz = loadClassByQualifiedNameInternall(threadId, name, returnAddress);
	if (clazz == NULL) {
		goto unlockAndReturn;
	}
	if (clazz->vtable != NULL) {
		goto unlockAndReturn;
	}

	if (!clazz->isInterface) {
		initializeParentArray(clazz);
		initializeVTable(clazz);
	}

	if (classLoadingPhase != 0) {
		if (strcmp(clazz->className, "java/lang/Class") != 0) {
			JavaObject* javaLangClass = createJavaLangClass(threadId, clazz->className, returnAddress);
			clazz->java_Lang_Class = javaLangClass;
		}
	} else {
		classInitSkippedClasses[classInitSkippedClassesCounter++] = clazz;
		if (classInitSkippedClassesCounter >= 500) {
			printf("The classInitSkippedClassesCounter exceeded its limit, possible SEGV!!!!\n");
		}
	}

	initializeInterfaces(threadId, clazz, returnAddress);

	if (classLoadingPhase == 2) {
		if (DEBUGGER_ON) {
			checkClassPrepare(threadId, clazz);
		}
		pthread_mutex_lock(&clazz->mutex);
		if (runClinitMethod(threadId, clazz, returnAddress) != 0) {
			pthread_mutex_unlock(&clazz->mutex);
			clazz = NULL;
			goto unlockAndReturn;
		}
		pthread_mutex_unlock(&clazz->mutex);
	} else {
		skippedClasses[skippedClassesCounter++] = clazz;
		if (skippedClassesCounter >= 500) {
			printf("The skippedClassesCounter exceeded its limit, possible SEGV!!!!\n");
		}
	}

unlockAndReturn:
	pthread_mutex_unlock(mutex);
	return clazz;
}

int runSkippedClinits(Thread* threadId, int returnAddress) {
	int i;
	for (i = 0; i < skippedClassesCounter; i++) {
		if (DEBUGGER_ON) {
			checkClassPrepare(threadId, skippedClasses[i]);
		}
		pthread_mutex_lock(&skippedClasses[i]->mutex);
		int flag = runClinitMethod(threadId, skippedClasses[i], returnAddress);
		pthread_mutex_unlock(&skippedClasses[i]->mutex);
		if (flag != 0) {
			return flag;
		}
	}
	return 0;
}

int runSkippedClassInits(Thread* threadId, int returnAddress) {
	int i;
	for (i = 0; i < classInitSkippedClassesCounter; i++) {
		Class* clazz = classInitSkippedClasses[i];
		pthread_mutex_lock(&clazz->mutex);
		if (strcmp(clazz->className, "java/lang/Class") != 0) {
			JavaObject* javaLangClass = createJavaLangClass(threadId, clazz->className, returnAddress);
			clazz->java_Lang_Class = javaLangClass;
		}
		pthread_mutex_unlock(&clazz->mutex);
	}
	return 0;
}

void initializeParentArray(Class* clazz) {
	size_t parentCount = 0;
	Class* currClazz = clazz->superClassPtr;
	if (currClazz == NULL) {
		clazz->classChain = NULL;
		clazz->classChainCount = 0;
		return;
	}
	while (currClazz != NULL) {
		currClazz = currClazz->superClassPtr;
		parentCount++;
	}
	clazz->classChain = malloc(sizeof(Class*) * (parentCount + 1));
	clazz->classChainCount = parentCount + 1;

	//The last class in the chain is the class itself
	clazz->classChain[clazz->classChainCount - 1] = clazz;
	currClazz = clazz->superClassPtr;
	size_t i = clazz->classChainCount - 2;
	while (currClazz != NULL) {
		clazz->classChain[i] = currClazz;
		currClazz = currClazz->superClassPtr;
		i--;
	}
}

void initializeVTable(Class* clazz) {
	size_t i, j, methodCount = 0;
	char methodPtrBuff[sizeof(uintptr_t) * 2 + 1];
	size_t methodPtrBuffSize = sizeof(uintptr_t) * 2 + 1;
	StrMap *stringMap;
	clazz->vtable = malloc(sizeof(VTable));
	stringMap = sm_new(50);
	clazz->vtable->map = stringMap;
#define KEY_BUFF_SIZE 1024
	char keyBuffer[KEY_BUFF_SIZE];
	if (stringMap == NULL) {
	    /* Handle allocation failure... */
	}
	for (i = 0; i < clazz->classChainCount; i++) {
		Class* superClazz = clazz->classChain[i];
		for (j = 0; j < superClazz->methodCount; j ++) {
			MethodInfo* method = &superClazz->methodArray[j];
			if (isVTableMethod(method) == 0) {
				continue;
			}
			getMethodKey(method, keyBuffer, KEY_BUFF_SIZE);
			int result = sm_exists(stringMap, keyBuffer);
			if (result == 0) {
				methodCount++;
			}
			//replace the old refference
			encodePointerIntoString(method, methodPtrBuff, methodPtrBuffSize);
			sm_put(stringMap, keyBuffer, methodPtrBuff);
		}
	}
	clazz->vtable->vtableSize = (u4) methodCount;
}

int isVTableMethod(MethodInfo* method) {
	if (((method->accessFlags & ACC_STATIC) != 0) ||
			((method->accessFlags & ACC_PRIVATE) != 0) ||
			((method->accessFlags & ACC_SYNTHETIC) != 0)) {
		return 0;
	}
	char* name = getUtfStringAtIndex(method->clazz, (size_t)method->nameIndex - 1);
	if (strcmp("<init>", name) == 0 ||  strcmp("<clinit>", name) == 0) {
		return 0;
	}
	return 1;
}

Class* loadClassByQualifiedNameInternall(Thread* threadId, const char* name, int returnAddress) {

	char fullPath[1024];
	memset(fullPath, 0 , sizeof(char) * 1024);
	strcat(fullPath, paths[0]);
	strcat(fullPath, name);
	strcat(fullPath, ".class");
	Class* clazz = loadClass(fullPath);
	if (clazz == NULL) {
		memset(fullPath, 0 , sizeof(char) * 1024);
		strcat(fullPath, paths[1]);
		strcat(fullPath, name);
		strcat(fullPath, ".class");
		clazz = loadClass(fullPath);
		if (clazz == NULL) {
			return NULL;
		}
	}
	initializeStaticFields(clazz);

	pthread_mutex_lock(&classListMutex);
	if (classEntryCount >= MAX_CLASS_COUNT) {
		printf("PermGen overflow!!!!");
	}
	strcpy(classList[classEntryCount].className, name);
	classList[classEntryCount].clazz = clazz;
	classEntryCount++;
	pthread_mutex_unlock(&classListMutex);

	//LOAD parent classes

	if (clazz->superClass > 0) {
		int superIndex = clazz->constantPoolArray[clazz->superClass - 1].nameIndex - 1;
		char* parentName = getUtfStringAtIndex(clazz, (size_t) superIndex);
		Class* parentClass = loadClassByQualifiedName(threadId, parentName, returnAddress);
		clazz->superClassPtr = parentClass;
	} else {
		clazz->superClassPtr = NULL;
	}

	return clazz;

}

void initializeInterfaces(Thread* threadId, Class* clazz, int returnAddress) {
	size_t i;
#define MAX_QUEUE_SIZE (2000)
	Class* queue[MAX_QUEUE_SIZE];
	for( i =0 ; i < clazz->interfaceCount; i++) {
		u2 index = clazz->interfaces[i];
		ConstantPoolInfo info = clazz->constantPoolArray[index - 1];
		char* name = getUtfStringAtIndex(clazz, (size_t) info.nameIndex - 1);
		loadClassByQualifiedName(threadId, name, returnAddress);
	}
	size_t superInterfaceCount = 0;

	int queuePointer = -1;
	queue[++queuePointer] = clazz;
	while (queuePointer >= 0) {
		Class* currClass = queue[queuePointer--];
		for( i =0 ; i < currClass->interfaceCount; i++) {
			u2 index = currClass->interfaces[i];
			ConstantPoolInfo info = currClass->constantPoolArray[index - 1];
			char* name = getUtfStringAtIndex(currClass, (size_t) info.nameIndex - 1);
			Class* interface = loadClassByQualifiedName(threadId, name, returnAddress);
			if (queuePointer >= (MAX_QUEUE_SIZE - 1)) {
				printf("Oppsss MAX_QUEUE_SIZE overflowww!!!!");
			}
			queue[++queuePointer] = interface;
			superInterfaceCount++;
		}
	}

	if (superInterfaceCount == 0) {
		return;
	}

	clazz->interfaceChainCount = superInterfaceCount;
	clazz->interfaceChain = malloc(sizeof(Class*) * superInterfaceCount);

	queuePointer = -1;
	int ctr = 0;
	queue[++queuePointer] = clazz;
	while (queuePointer >= 0) {
		Class* currClass = queue[queuePointer--];
		for( i =0 ; i < currClass->interfaceCount; i++) {
			u2 index = currClass->interfaces[i];
			ConstantPoolInfo info = currClass->constantPoolArray[index - 1];
			char* name = getUtfStringAtIndex(currClass, (size_t) info.nameIndex - 1);
			Class* interface = loadClassByQualifiedName(threadId, name, returnAddress);
			queue[++queuePointer] = interface;
			clazz->interfaceChain[ctr] = interface;
			ctr++;
		}
	}

}
JavaObject* createJavaLangClass(Thread* threadId, char* className, int returnAddress) {
	size_t j;
	Class* javaLangClass = loadClassByQualifiedName(threadId, "java/lang/Class", returnAddress);
	JavaObject* javaLangClassPtr = allocObject(javaLangClass);
	Class* destClass = loadClassByQualifiedName(threadId, className, returnAddress);
	javaLangClassPtr->type.nativeObject = destClass;

	//only the java vm creates a class objects
	//the constructor is private
	MethodInfo* initMethod = findMethodNoAccessFlags(javaLangClass, "<init>", "()V");
	Value parentStack[1];
	parentStack[0].type = POINTER_JVM;
	parentStack[0].val.pointer = javaLangClassPtr;
	int parentStackPointer = 0;
	runMethod(threadId, initMethod, returnAddress, parentStack, &parentStackPointer);

	FieldInfo* nameField = findField(javaLangClass, "name", "Ljava/lang/String;");
	size_t k = strlen(className) + 1;
	char* newChar = malloc(k);
	newChar[k - 1] = '\0';
	memcpy(newChar, className, k - 1);
	for(j = 0; j < k; j++) {
		if (newChar[j] == '/') {
			newChar[j] = '.';
		}
	}
	JavaObject* nameString = charArrayToString(threadId, newChar);
	free(newChar);

	Value val;
	val.type = POINTER_JVM;
	val.val.pointer = nameString;
	setField(&val, javaLangClassPtr, nameField);
	return javaLangClassPtr;
}

pthread_mutex_t* getClassLoadMutex(const char* name) {
	char ptrBuffer[TEMP_BUFF_SIZE];
	pthread_mutex_lock(&bootstrapMutex);

	int result = sm_get(mutexMap, name, ptrBuffer, TEMP_BUFF_SIZE);
	if (result != 0) {
		void* ptr = decodePointerFromString(ptrBuffer);
		pthread_mutex_unlock(&bootstrapMutex);
		return ptr;
	}
	pthread_mutex_t * mutex = malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(mutex, NULL);
	encodePointerIntoString(mutex, ptrBuffer, TEMP_BUFF_SIZE);
	sm_put(mutexMap, name, ptrBuffer);
	pthread_mutex_unlock(&bootstrapMutex);
	return mutex;
}

JavaObject* createIfNotExistsPrimiteClass(Thread* thread, char* primitiveName) {
	if (strcmp(primitiveName, "float") == 0) {
		return createIfNotExistsPrimitiveClassInner(thread, "java/lang/Float");
	}

	if (strcmp(primitiveName, "double") == 0) {
		return createIfNotExistsPrimitiveClassInner(thread, "java/lang/Double");
	}

	if (strcmp(primitiveName, "int") == 0) {
		return createIfNotExistsPrimitiveClassInner(thread, "java/lang/Integer");
	}

	if (strcmp(primitiveName, "boolean") == 0) {
		return createIfNotExistsPrimitiveClassInner(thread, "java/lang/Boolean");
	}

	if (strcmp(primitiveName, "long") == 0) {
		return createIfNotExistsPrimitiveClassInner(thread, "java/lang/Long");
	}

	if (strcmp(primitiveName, "char") == 0) {
		return createIfNotExistsPrimitiveClassInner(thread, "java/lang/Character");
	}

	if (strcmp(primitiveName, "byte") == 0) {
		return createIfNotExistsPrimitiveClassInner(thread, "java/lang/Byte");
	}

	if (strcmp(primitiveName, "short") == 0) {
		return createIfNotExistsPrimitiveClassInner(thread, "java/lang/Short");
	}

	if (strcmp(primitiveName, "void") == 0) {
		return createIfNotExistsPrimitiveClassInner(thread, "java/lang/Void");
	}

	if (primitiveName[0] == '[') {
		return createIfNotExistsPrimitiveClassInner(thread, primitiveName);
	}

	printf("OOOPPPSSSS add primite class for %s\n", primitiveName);

	return NULL;
}

JavaObject* createIfNotExistsPrimitiveClassInner(Thread* thread, char* fullName) {
	char ptrBuffer[TEMP_BUFF_SIZE];

	pthread_mutex_lock(&primitiveClassesMutex);
	int result = sm_get(primitiveClasses, fullName, ptrBuffer, TEMP_BUFF_SIZE);
	if (result != 0) {
		void* ptr = decodePointerFromString(ptrBuffer);
		pthread_mutex_unlock(&primitiveClassesMutex);
		return ptr;
	}
	pthread_mutex_unlock(&primitiveClassesMutex);

	JavaObject* primitiveClass = createJavaLangClass(thread, fullName, 0);
	encodePointerIntoString(primitiveClass, ptrBuffer, TEMP_BUFF_SIZE);

	pthread_mutex_lock(&primitiveClassesMutex);
	sm_put(primitiveClasses, fullName, ptrBuffer);
	pthread_mutex_unlock(&primitiveClassesMutex);

	return primitiveClass;
}

Class* createPrimitiveArrayClass(Thread* thread, const char* name) {
	pthread_mutex_t* mutex = getClassLoadMutex(name);

	pthread_mutex_lock(mutex);

	Class* class = newClass();
	memset(class, 0, sizeof(Class));

	class->className = malloc(strlen(name) + 1);
	memset(class->className, 0, strlen(name) + 1);
	strcat(class->className, name);

	//the class for array object has java.lang.Object as super class
	Class* javaLangObject = findClassByQualifiedName("java/lang/Object");
	class->superClassPtr = javaLangObject;
	class->classChainCount = 2;
	class->classChain = malloc(sizeof(Class*) * 2);

	//the last class in the chain is itself
	class->classChain[1] = class;
	class->classChain[0] = javaLangObject;

	initializeVTable(class);

	//add the class to the list of the classes
	pthread_mutex_lock(&classListMutex);
	if (classEntryCount >= MAX_CLASS_COUNT) {
		printf("PermGen overflow!!!!");
	}
	strcpy(classList[classEntryCount].className, name);
	classList[classEntryCount].clazz = class;
	classEntryCount++;
	pthread_mutex_unlock(&classListMutex);

	class->java_Lang_Class = createIfNotExistsPrimiteClass(thread, name);


	pthread_mutex_unlock(mutex);

	return class;
}


void freeAllClasses() {
	size_t i;
	for (i = 0; i < classEntryCount; i++) {
		ClassEntry entry = classList[i];
		if (entry.clazz != NULL) {
			pthread_mutex_destroy(&entry.clazz->mutex);
			freeClass(entry.clazz);
			entry.clazz = NULL;
		}
	}
	if (mutexMap != NULL) {
		sm_enum(mutexMap, deleteMutex, NULL);
		sm_delete(mutexMap);
	}
	pthread_mutex_destroy(&bootstrapMutex);
	pthread_mutex_destroy(&classListMutex);

	if (primitiveClasses != NULL) {
		sm_delete(primitiveClasses);
	}
	pthread_mutex_destroy(&primitiveClassesMutex);
}

void deleteMutex(const char *key, const char *value, const void *obj) {
	char ptrBuffer[TEMP_BUFF_SIZE];
	memset(ptrBuffer, 0 , TEMP_BUFF_SIZE);
	strcat(ptrBuffer, value);
	pthread_mutex_t* ptr = (pthread_mutex_t*) decodePointerFromString(ptrBuffer);
	pthread_mutex_destroy(ptr);
	free(ptr);
}

void initClassHolder() {
	memset(classList, 0, MAX_CLASS_COUNT * sizeof(ClassEntry));
	pthread_mutex_init(&bootstrapMutex, NULL);
	pthread_mutex_init(&classListMutex, NULL);

	mutexMap = sm_new(100);
	if (mutexMap == NULL) {
	   printf("mutexMap init error\n");
	}

	primitiveClasses = sm_new(100);
	if (primitiveClasses == NULL) {
	   printf("primitiveClasses init error\n");
	}
}

