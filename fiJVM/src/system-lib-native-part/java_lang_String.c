#include <string.h>
#include "java_lang_String.h"
#include "string-pool.h"
#include "threads.h"

void  Java_java_lang_String_intern(Thread* threadId, Value* retVal, Value* args) {
	void* objPtr = args->val.pointer;
	char* str = convertStringToNativeCharArr(objPtr);

	void* newObj = addToStringPoolIfNotExists(threadId, str);
	retVal->val.pointer = newObj;
	retVal->type = POINTER_JVM;

	if (str != NULL) {
		free(str);
	}
}
