#ifndef SRC_SYSTEM_LIB_NATIVE_PART_JAVA_IO_FILEDESCRIPTOR_H_
#define SRC_SYSTEM_LIB_NATIVE_PART_JAVA_IO_FILEDESCRIPTOR_H_
#include "type-union.h"
#include "threads.h"

void  Java_java_io_FileDescriptor_initIDs(Thread* threadId, Value* retVal, Value* args);

#endif
