#ifndef JAVA_LANG_THROWABLE_H_
#define JAVA_LANG_THROWABLE_H_

#include "type-union.h"
#include "threads.h"

void  Java_java_lang_Throwable_fillInStackTrace(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Throwable_getStackTraceDepth(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Throwable_getStackTraceElement(Thread* threadId, Value* retVal, Value* args);

#endif
