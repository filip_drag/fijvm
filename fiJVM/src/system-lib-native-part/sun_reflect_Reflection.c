#include "sun_reflect_Reflection.h"
#include <string.h>

void  Java_sun_reflect_Reflection_getCallerClass(Thread* threadId, Value* retVal, Value* args) {

	int i;
	StackFrame fr;
	for (i = threadId->callStackPointer; i >= 0 ; i--) {
		StackFrame frame = threadId->callStack[i];
		if (strcmp(frame.clazz->className, "java/lang/reflect/Method") == 0) {
			continue;
		}
		fr = frame;
		break;
	}

	retVal->type = POINTER_JVM;
	retVal->val.pointer = fr.clazz->java_Lang_Class;
}

void  Java_sun_reflect_Reflection_getClassAccessFlags(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* obj = args->val.pointer;
	Class* clazz = obj->type.nativeObject;
	retVal->type = INT_JVM;
	retVal->val.intVal = (int32_t) clazz->accessFlags;
}


