#include "java_lang_System.h"
#include "heap.h"
#include "array.h"
#include <string.h>
#include "class-def.h"
#include "type-union.h"
#include "jni-helper.h"
#include "java-object-consts.h"
#include "threads.h"
#include "util.h"
#include "classes.h"

void initSystemProperties(Thread* threadId);

void  Java_java_lang_System_arraycopy(Thread* threadId, Value* retVal, Value* args) {
	//(Object src, int srcPos, Object dest, int destPos, int length)

	u4 elementSize;
	int32_t length = args->val.intVal;
	args--;
	int32_t destPos = args->val.intVal;
	args--;
	JavaObject* destObj = args->val.pointer;
	args--;
	int32_t srcPos = args->val.intVal;
	args--;
	JavaObject* srcObj = args->val.pointer;

	if (isArrayOfRefs(srcObj)) {
		elementSize = arraySize((u1) ARR_TYPE_REF);
	} else {
		elementSize = arraySize((u1) srcObj->type.primitiveType);
	}
	u1* srcPtr = ((u1*)srcObj) + ARRAY_HEADER_SIZE;
	u1* dstPtr = ((u1*)destObj) + ARRAY_HEADER_SIZE;
	memcpy(dstPtr + (u4) destPos * elementSize, srcPtr + (u4) srcPos * elementSize, (u4) length * elementSize );
}

void  Java_java_lang_System_currentTimeMillis(Thread* threadId, Value* retVal, Value* args)  {
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	retVal->val.longVal = ts.tv_nsec;
	retVal->type = LONG_JVM;
}

void  Java_java_lang_System_nanoTime(Thread* threadId, Value* retVal, Value* args)  {
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	retVal->val.longVal = ts.tv_nsec;
	retVal->type = LONG_JVM;
}

void  Java_java_lang_System_identityHashCode(Thread* threadId, Value* retVal, Value* args)  {
	JavaObject* obj = args->val.pointer;
	retVal->val.intVal = obj->objectId;
	retVal->type = INT_JVM;
}

void  Java_java_lang_System_registerNatives(Thread* threadId, Value* retVal, Value* args) {

	Class* fileDescriptorClass = loadClassByQualifiedName(threadId, "java/io/FileDescriptor", 0);

	Value val;
	FieldInfo* field = findField(fileDescriptorClass, "out", "Ljava/io/FileDescriptor;");
	getRefValue(&val, field->staticValuePointer, "Ljava/io/FileDescriptor;");
	//JavaObject* systemOutDescriptor = val.val.pointer;

	Class* fileOutputStreamClass = loadClassByQualifiedName(threadId, "java/io/FileOutputStream", 0);
	JavaObject* systemOut = allocObject(fileOutputStreamClass);
	FieldInfo* fdField = findField(fileOutputStreamClass, "fd", "Ljava/io/FileDescriptor;");
	setField(&val, systemOut, fdField);

	Class* systemClass = loadClassByQualifiedName(threadId, "java/lang/System", 0);
	FieldInfo* outField = findField(systemClass, "out", "Ljava/io/PrintStream;");
	val.val.pointer = systemOut;
	setFieldValue(outField->staticValuePointer, &val, "Ljava/io/PrintStream;");
}


