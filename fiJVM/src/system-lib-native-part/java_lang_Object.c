#include <pthread.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <errno.h>
#include "java_lang_Object.h"
#include "jni-helper.h"
#include "threads.h"
#include "heap.h"
#include "java-object-consts.h"
#include "array.h"

void  Java_java_lang_Object_registrerNatives(Thread* threadId, Value* retVal, Value* args) {

}

void  Java_java_lang_Object_getClass(Thread* threadId, Value* retVal, Value* args) {

	void* objPtr = args->val.pointer;
	Class* clazz = getClass(objPtr);
	retVal->val.pointer = clazz->java_Lang_Class;
	retVal->type = POINTER_JVM;
}

void  Java_java_lang_Object_wait(Thread* threadId, Value* retVal, Value* args) {
	int64_t timeout = args->val.longVal;
	args--;
	JavaObject* thisObj = args->val.pointer;

	struct timespec ts;
	int res;
	if (timeout != 0) {
		clock_gettime(CLOCK_REALTIME, &ts);
		ts.tv_nsec += timeout;
		res = pthread_cond_timedwait(&thisObj->condVar, &thisObj->mutex, &ts);
	} else {
		res = pthread_cond_wait(&thisObj->condVar, &thisObj->mutex);
	}

	//FIXME add exception throws
	switch (res) {
		case EPERM:
			//IllegalMonitorStateException
			break;
		case ETIMEDOUT:
			//timeout out
		default:
			break;
	}
}

void  Java_java_lang_Object_notify(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* thisObj = args->val.pointer;
	int res = pthread_cond_signal(&thisObj->condVar);
	//FIXME add exception throws
}

void  Java_java_lang_Object_notifyAll(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* thisObj = args->val.pointer;
	int res = pthread_cond_broadcast(&thisObj->condVar);
	//FIXME add exception throws
}

void  Java_java_lang_Object_hashCode(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* obj = args->val.pointer;
	retVal->val.intVal = obj->objectId;
	retVal->type = INT_JVM;
}

void  Java_java_lang_Object_clone(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* thisObj = args->val.pointer;

	if (thisObj->flags == 0) {
		retVal->val.pointer = NULL;
		return;
	}

	u4 size = thisObj->extra.length;
	JavaObject* newArr = allocArray(threadId, ARR_TYPE_REF, size, thisObj->type.elementClazz);

	u4 elementSize;
	if (isArrayOfRefs(thisObj)) {
		elementSize = arraySize((u1) ARR_TYPE_REF);
	} else {
		elementSize = arraySize((u1) thisObj->type.primitiveType);
	}
	u1* srcPtr = ((u1*)thisObj) + ARRAY_HEADER_SIZE;
	u1* dstPtr = ((u1*)newArr) + ARRAY_HEADER_SIZE;
	memcpy(dstPtr, srcPtr, (u4) size * elementSize );

	retVal->type = POINTER_JVM;
	retVal->val.pointer = newArr;
}
