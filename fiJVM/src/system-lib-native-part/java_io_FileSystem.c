/*
 * java_io_FileSystem.c
 *
 *  Created on: Sep 6, 2015
 *      Author: fi
 */

#include "java_io_FileSystem.h"
#include "heap.h"
#include "classes.h"
#include "cpu.h"
#include "jni-helper.h"

void  Java_java_io_FileSystem_getFileSystem(Thread* threadId, Value* retVal, Value* args) {

#ifdef LINUX
	Class* unixFileSystem = loadClassByQualifiedName(threadId, "java/io/UnixFileSystem", 0);
	JavaObject* obj = allocObject(unixFileSystem);
	Value parentStack;
	parentStack.val.pointer = obj;
	parentStack.type = POINTER_JVM;
	int parentStackPointer = 0;
	MethodInfo* initMethod = findMethodNoAccessFlags(unixFileSystem, "<init>", "()V");
	runMethod(threadId, initMethod, 0, &parentStack, &parentStackPointer);
	retVal->val.pointer = obj;
	retVal->type = POINTER_JVM;
#endif
#ifdef WIN
#endif
}

