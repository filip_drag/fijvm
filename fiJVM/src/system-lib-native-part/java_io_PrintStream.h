#ifndef JAVA_IO_PRINTSTREAM_H_
#define JAVA_IO_PRINTSTREAM_H_
#include "jni.h"
#include "threads.h"

JNIEXPORT void JNICALL Java_java_io_PrintStream_print(Thread* threadId, Value* retVal, Value* args);

#endif
