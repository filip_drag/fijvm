#include "java_lang_Double.h"
#include "threads.h"

void  Java_java_lang_Double_doubleToRawLongBits(Thread* threadId, Value* retVal, Value* args) {

	double d = args->val.doubleVal;
	retVal->val.longVal = *((int64_t*)&d);
	retVal->type = LONG_JVM;
}

void  Java_java_lang_Double_longBitsToDouble(Thread* threadId, Value* retVal, Value* args) {

	int64_t i = args->val.longVal;
	retVal->val.doubleVal = *((double*)&i);
	retVal->type = DOUBLE_JVM;
}
