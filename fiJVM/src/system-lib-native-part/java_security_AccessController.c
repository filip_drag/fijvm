#include "java_security_AccessController.h"
#include <stdio.h>
#include "jni-helper.h"
#include "cpu.h"

void  Java_java_security_AccessController_getStackAccessControlContext(Thread* threadId, Value* retVal, Value* args) {
	retVal->val.pointer = NULL;
	retVal->type = POINTER_JVM;
}

void  Java_java_security_AccessController_doPrivileged(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* priviledgedAction = args->val.pointer;
	if (priviledgedAction == NULL) {
		printf("NullPointerException in doPriviledged!\n");
	}
	MethodInfo* method = findVirtualMethod(priviledgedAction->vtable, "run", "()V");
	Value val[2];
	val[0].val.pointer = priviledgedAction;
	val[0].type = POINTER_JVM;
	int parentPointer = 0;
	runMethod(threadId, method, 0, val, &parentPointer);
	retVal->val.pointer = val[parentPointer].val.pointer;
	retVal->type = POINTER_JVM;
}

void  Java_java_security_AccessController_doPrivileged1(Thread* threadId, Value* retVal, Value* args) {
	Java_java_security_AccessController_doPrivileged(threadId,retVal,args);
}
