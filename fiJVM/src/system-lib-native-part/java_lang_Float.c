#include "java_lang_Float.h"
#include "threads.h"

void  Java_java_lang_Float_floatToRawIntBits(Thread* threadId, Value* retVal, Value* args) {

	float f = args->val.floatVal;
	retVal->val.intVal = *((int32_t*)&f);
	retVal->type = INT_JVM;
}


void  Java_java_lang_Float_intBitsToFloat(Thread* threadId, Value* retVal, Value* args) {

	int32_t i = args->val.intVal;
	retVal->val.floatVal = *((float*)&i);
	retVal->type = FLOAT_JVM;
}
