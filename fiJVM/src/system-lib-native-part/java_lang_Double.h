#ifndef JAVA_LANG_DOUBLE_H_
#define JAVA_LANG_DOUBLE_H_
#include "type-union.h"
#include "threads.h"

void  Java_java_lang_Double_doubleToRawLongBits(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Double_longBitsToDouble(Thread* threadId, Value* retVal, Value* args);

#endif
