#ifndef SRC_SYSTEM_LIB_NATIVE_PART_SUN_MISC_UNSAFE_H_
#define SRC_SYSTEM_LIB_NATIVE_PART_SUN_MISC_UNSAFE_H_
#include "type-union.h"
#include "threads.h"

void  Java_sun_misc_Unsafe_registerNatives(Thread* threadId, Value* retVal, Value* args);
void  Java_sun_misc_Unsafe_arrayBaseOffset(Thread* threadId, Value* retVal, Value* args);
void  Java_sun_misc_Unsafe_arrayIndexScale(Thread* threadId, Value* retVal, Value* args);
void  Java_sun_misc_Unsafe_addressSize(Thread* threadId, Value* retVal, Value* args);

#endif
