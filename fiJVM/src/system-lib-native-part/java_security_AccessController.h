#ifndef SRC_SYSTEM_LIB_NATIVE_PART_JAVA_SECURITY_ACCESSCONTROLLER_H_
#define SRC_SYSTEM_LIB_NATIVE_PART_JAVA_SECURITY_ACCESSCONTROLLER_H_
#include "type-union.h"
#include "threads.h"

void  Java_java_security_AccessController_getStackAccessControlContext(Thread* threadId, Value* retVal, Value* args);
void  Java_java_security_AccessController_doPrivileged(Thread* threadId, Value* retVal, Value* args);
void  Java_java_security_AccessController_doPrivileged1(Thread* threadId, Value* retVal, Value* args);
#endif
