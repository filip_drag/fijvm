
#ifndef SRC_SYSTEM_LIB_NATIVE_PART_SUN_REFLECT_REFLECTION_H_
#define SRC_SYSTEM_LIB_NATIVE_PART_SUN_REFLECT_REFLECTION_H_
#include "type-union.h"
#include "threads.h"
void  Java_sun_reflect_Reflection_getCallerClass(Thread* threadId, Value* retVal, Value* args);
void  Java_sun_reflect_Reflection_getClassAccessFlags(Thread* threadId, Value* retVal, Value* args) ;

#endif
