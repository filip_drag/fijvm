/*
 * sun_reflect_NativeConstructorAccessorImpl.h
 *
 *  Created on: Sep 27, 2015
 *      Author: fi
 */

#ifndef SRC_SYSTEM_LIB_NATIVE_PART_SUN_REFLECT_NATIVECONSTRUCTORACCESSORIMPL_H_
#define SRC_SYSTEM_LIB_NATIVE_PART_SUN_REFLECT_NATIVECONSTRUCTORACCESSORIMPL_H_
#include "type-union.h"
#include "threads.h"

void  Java_sun_reflect_NativeConstructorAccessorImpl_newInstance0(Thread* threadId, Value* retVal, Value* args);

#endif /* SRC_SYSTEM_LIB_NATIVE_PART_SUN_REFLECT_NATIVECONSTRUCTORACCESSORIMPL_H_ */
