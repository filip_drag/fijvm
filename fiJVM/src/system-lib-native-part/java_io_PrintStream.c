#include <stdio.h>
#include "type-union.h"
#include "java_io_PrintStream.h"
#include "threads.h"

void  Java_java_io_PrintStream_print(Thread* threadId, Value* retVal, Value* args) {
	char c = (char) args->val.charVal;
	printf("%c", c);
}
