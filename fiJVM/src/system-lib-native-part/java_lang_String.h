#ifndef JAVA_LANG_STRING_H_
#define JAVA_LANG_STRING_H_
#include "type-union.h"
#include "threads.h"

void  Java_java_lang_String_intern(Thread* threadId, Value* retVal, Value* args);

#endif
