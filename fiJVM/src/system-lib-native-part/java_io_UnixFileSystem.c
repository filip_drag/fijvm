/*
 * java_io_UnixFileSystem.c
 *
 *  Created on: Sep 6, 2015
 *      Author: fi
 */

#include "java_io_UnixFileSystem.h"
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "string-pool.h"
#include "jni-helper.h"
#include "classes.h"

#define BA_EXISTS 0x01
#define BA_REGULAR 0x02
#define BA_DIRECTORY 0x04
#define BA_HIDDEN 0x08

void  Java_java_io_UnixFileSystem_initIDs(Thread* threadId, Value* retVal, Value* args) {

}

void  Java_java_io_UnixFileSystem_canonicalize0(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* string = args->val.pointer;

	char* nativePath = convertStringToNativeCharArr(string);
	char* realPath = realpath(nativePath, NULL);

	JavaObject* toReturn = charArrayToString(threadId, realPath);

	retVal->val.pointer = toReturn;
	retVal->type = POINTER_JVM;

	free(realPath);
	free(nativePath);
}

void  Java_java_io_UnixFileSystem_getBooleanAttributes0(Thread* threadId, Value* retVal, Value* args) {

	JavaObject* file = args->val.pointer;

	Class* fileClass = findClassByQualifiedName("java/io/File");
	FieldInfo* pathField = findField(fileClass, "path", "Ljava/lang/String;");
	Value pathVal;
	fieldValue(&pathVal, file, pathField);


	char* path = convertStringToNativeCharArr(pathVal.val.pointer);
	struct stat sb;
	int mode;
	if (stat(path, &sb) == 0) {
		mode = (int) sb.st_mode;
	}

	free(path);

	int fmt = mode & S_IFMT;

	int rv = (int) (((fmt == S_IFREG) ? BA_REGULAR : 0)
			| ((fmt == S_IFDIR) ? BA_DIRECTORY : 0));

	retVal->val.intVal = rv;
	retVal->type = INT_JVM;
}
