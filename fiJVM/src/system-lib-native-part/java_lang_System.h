#ifndef JAVA_LANG_SYSTEM_H_
#define JAVA_LANG_SYSTEM_H_
#include "type-union.h"
#include "threads.h"

void Java_java_lang_System_arraycopy(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_System_currentTimeMillis(Thread* threadId, Value* retVal, Value* args) ;
void  Java_java_lang_System_nanoTime(Thread* threadId, Value* retVal, Value* args) ;
void  Java_java_lang_System_identityHashCode(Thread* threadId, Value* retVal, Value* args) ;
void  Java_java_lang_System_registerNatives(Thread* threadId, Value* retVal, Value* args) ;


#endif
