#include "java_lang_Class.h"
#include <stdio.h>
#include <string.h>
#include "util.h"
#include "class-def.h"
#include "jni-helper.h"
#include "string-pool.h"
#include "classes.h"
#include "threads.h"
#include "reflection.h"

void  Java_java_lang_Class_getName0(Thread* threadId, Value* retVal, Value* args) {
	void* classRef = args->val.pointer;

	Class* clazz = getClass(classRef);
	FieldInfo* nameField = findField(clazz, "name", "Ljava/lang/String;");
	Value val;
	fieldValue(&val, classRef, nameField);
	retVal->type= POINTER_JVM;
	retVal->val.pointer = val.val.pointer;
}

void  Java_java_lang_Class_getPrimitiveClass(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* className = args->val.pointer;
	char* name = convertStringToNativeCharArr(className);
	JavaObject* primitiveClass = createIfNotExistsPrimiteClass(threadId, name);
	free(name);

	retVal->type =POINTER_JVM;
	retVal->val.pointer = primitiveClass;

}

void  Java_java_lang_Class_registerNatives(Thread* threadId, Value* retVal, Value* args) {
}

//FIXME sett in allocObject and allocArray the loading classloader of a class
//all system classes must have classloader = NULL
void  Java_java_lang_Class_getClassLoader0(Thread* threadId, Value* retVal, Value* args) {
	retVal->val.pointer = NULL;
	retVal->type = POINTER_JVM;
}

void  Java_java_lang_Class_desiredAssertionStatus0(Thread* threadId, Value* retVal, Value* args) {

	//for every class return a desired assertion status false
	retVal->val.intVal = 0;
	retVal->type = INT_JVM;
}

void  Java_java_lang_Class_isAssignableFrom(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* classObjectParam = args->val.pointer;
	args--;
	JavaObject* thisClass = args->val.pointer;

	retVal->val.intVal = isAssignableFrom(classObjectParam->type.nativeObject, thisClass->type.nativeObject);
	retVal->type = BOOL_JVM;
}

void  Java_java_lang_Class_getSuperclass(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* thisClass = args->val.pointer;

	Class* clazz = thisClass->type.nativeObject;
	void* ptr = NULL;
	if (clazz->superClassPtr != NULL ) {
		ptr = clazz->superClassPtr->java_Lang_Class;
	}
	retVal->val.pointer = ptr;
	retVal->type = POINTER_JVM;

}

void  Java_java_lang_Class_isInterface(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* thisClass = args->val.pointer;

	Class* clazz = thisClass->type.nativeObject;
	retVal->val.intVal = clazz->isInterface;
	retVal->type = INT_JVM;
}


char* modifyName(char* name) {
	char* ptr = name;
	while (*ptr != '\0') {
		if (*ptr == '.') {
			*ptr = '/';
		}
		ptr++;
	}
	return name;
}

void  Java_java_lang_Class_forName0(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* classLoader = args->val.pointer;
	args--;
	//FIXME use initilize paramter, now it is not used add classes are always initialized
	int initialize = args->val.intVal;
	if (initialize == 0) {
		printf("OOPPPPPPSSS baddd. initilialize = 0 in Class.forName0()\n");
	}
	args--;
	JavaObject* className = args->val.pointer;
	char* name = convertStringToNativeCharArr(className);
	name = modifyName(name);
	Class* clazz = loadClassByQualifiedName(threadId, name, 0);
	clazz->classLoader = classLoader;
	retVal->val.pointer = clazz->java_Lang_Class;

	free(name);

}

void  Java_java_lang_Class_getDeclaredConstructors0(Thread* threadId, Value* retVal, Value* args) {

	int isPublic = args->val.intVal;
	args--;
	JavaObject* thisClass = args->val.pointer;

	Class* clazz = thisClass->type.nativeObject;

	JavaObject* constructorArray = getClassConstructors(threadId, clazz, isPublic);

	retVal->type = POINTER_JVM;
	retVal->val.pointer = constructorArray;
}

void  Java_java_lang_Class_getModifiers(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* obj = args->val.pointer;
	Class* clazz = obj->type.nativeObject;
	retVal->type = INT_JVM;
	retVal->val.intVal = clazz->accessFlags;
}

void  Java_java_lang_Class_isArray(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* javaClass =  args->val.pointer;
	Class* clazz = javaClass->type.nativeObject;

	retVal->val.intVal = clazz->className[0] == '[' ? 1 : 0;
	retVal->type = BOOL_JVM;
}

void  Java_java_lang_Class_isPrimitive(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* javaClass =  args->val.pointer;
	FieldInfo* nameField = findField(javaClass->clazz, "name", "Ljava/lang/String;");
	Value val;
	fieldValue(&val, javaClass, nameField);
	char* primitiveName = convertStringToNativeCharArr(val.val.pointer);
	int isPrimitive = 0;
	if (strcmp(primitiveName, "float") == 0) {
		isPrimitive = 1;
	} else if (strcmp(primitiveName, "double") == 0) {
		isPrimitive = 1;
	} else if (strcmp(primitiveName, "int") == 0) {
		isPrimitive = 1;
	} else if (strcmp(primitiveName, "boolean") == 0) {
		isPrimitive = 1;
	} else if (strcmp(primitiveName, "long") == 0) {
		isPrimitive = 1;
	} else if (strcmp(primitiveName, "char") == 0) {
		isPrimitive = 1;
	} else if (strcmp(primitiveName, "byte") == 0) {
		isPrimitive = 1;
	} else if (strcmp(primitiveName, "short") == 0) {
		isPrimitive = 1;
	} else if (strcmp(primitiveName, "void") == 0) {

	}

	free(primitiveName);

	retVal->val.intVal = isPrimitive;
	retVal->type = BOOL_JVM;
}

void  Java_java_lang_Class_getComponentType(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* javaClass = args->val.pointer;
	Class* clazz = javaClass->type.nativeObject;

	if (clazz->className[0] != '[') {
		retVal->val.pointer = NULL;
		retVal->type = POINTER_JVM;
		return;
	}
	size_t length =  strlen(clazz->className) + 1;
	char* name = malloc(length);
	memset(name, 0, length);
	int i = 0;
	while (clazz->className[i] == '[') {
		i++;
	}

	if (clazz->className[i] == 'L') {
		i++;
		strcat(name, &clazz->className[i]);
		//remove the ; at the end
		name[strlen(name) - 1] = '\0';
	} else {
		strcat(name, &clazz->className[i]);
	}

	Class* componentClass;
	char* nameArg;
	if (strlen(name) == 1) {
		switch (name[0]) {
			case 'I': nameArg = "java/lang/Integer"; break;
			case 'J': nameArg = "java/lang/Long"; break;
			case 'F': nameArg = "java/lang/Float"; break;
			case 'D': nameArg = "java/lang/Double"; break;
			case 'C': nameArg = "java/lang/Character"; break;
			case 'B': nameArg = "java/lang/Byte"; break;
			case 'S': nameArg = "java/lang/Short"; break;
			case 'Z': nameArg = "java/lang/Boolean"; break;
		}
	} else {
		nameArg = name;
	}
	componentClass = loadClassByQualifiedName(threadId, nameArg, 0);
	free(name);

	retVal->val.pointer = componentClass->java_Lang_Class;
	retVal->type = POINTER_JVM;
}

void  Java_java_lang_Class_getDeclaredFields0(Thread* threadId, Value* retVal, Value* args) {

}
