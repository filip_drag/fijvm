#ifndef JAVA_LANG_CLASS_H_
#define JAVA_LANG_CLASS_H_
#include "type-union.h"
#include "threads.h"

void  Java_java_lang_Class_getName0(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_registerNatives(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_getPrimitiveClass(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_getClassLoader0(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_desiredAssertionStatus0(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_isAssignableFrom(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_getSuperclass(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_forName0(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_isInterface(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_getDeclaredConstructors0(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_getModifiers(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_isArray(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_getComponentType(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_isPrimitive(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Class_getDeclaredFields0(Thread* threadId, Value* retVal, Value* args);

#endif
