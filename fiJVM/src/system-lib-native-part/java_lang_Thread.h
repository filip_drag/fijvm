#ifndef JAVA_LANG_THREAD_H_
#define JAVA_LANG_THREAD_H_

#include "type-union.h"
#include "threads.h"

void  Java_java_lang_Thread_start0(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Thread_registerNatives(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Thread_currentThread(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Thread_setPriority0(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Thread_isAlive(Thread* threadId, Value* retVal, Value* args);

#endif
