#ifndef JAVA_LANG_OBJECT_H_
#define JAVA_LANG_OBJECT_H_
#include "type-union.h"
#include "threads.h"

void  Java_java_lang_Object_getClass(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Object_wait(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Object_notify(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Object_notifyAll(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Object_registrerNatives(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Object_hashCode(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Object_clone(Thread* threadId, Value* retVal, Value* args);

#endif
