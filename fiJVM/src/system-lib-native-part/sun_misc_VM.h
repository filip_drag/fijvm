#ifndef SRC_SYSTEM_LIB_NATIVE_PART_SUN_MISC_VM_H_
#define SRC_SYSTEM_LIB_NATIVE_PART_SUN_MISC_VM_H_
#include "type-union.h"
#include "threads.h"

void  Java_sun_misc_VM_initialize(Thread* threadId, Value* retVal, Value* args);


#endif
