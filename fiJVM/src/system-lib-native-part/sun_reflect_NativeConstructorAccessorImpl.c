/*
 * sun_reflect_NativeConstructorAccessorImpl.c
 *
 *  Created on: Sep 27, 2015
 *      Author: fi
 */

#include <stdio.h>
#include "sun_reflect_NativeConstructorAccessorImpl.h"
#include "heap.h"
#include "cpu.h"
#include "array.h"
#include "jni-helper.h"

void  Java_sun_reflect_NativeConstructorAccessorImpl_newInstance0(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* paramArray = args->val.pointer;
	args--;
	JavaObject* constructor = args->val.pointer;

	//get the constructor class
	FieldInfo* clazzField = findField(constructor->clazz, "clazz", "Ljava/lang/Class;");
	Value val;
	fieldValue(&val, constructor, clazzField);
	JavaObject* clazzJavaObject = (JavaObject*) val.val.pointer;
	Class* clazz = clazzJavaObject->type.nativeObject;

	//get the constructor slot
	FieldInfo* slotField = findField(constructor->clazz, "slot", "I");
	fieldValue(&val, constructor, slotField);
	MethodInfo* method = &clazz->methodArray[val.val.intVal];

	JavaObject* newObject = allocObject(clazz);

	Value dummy;
	Value* params;
	int parentPointer  = -1;
	if (paramArray != NULL) {

		params = malloc(sizeof(Value)* paramArray->extra.length);
		int i, j;
		for (i = (int) paramArray->extra.length - 1, j = 0; i >= 0; i--, j++) {
			loadRefFromArray(paramArray, j, (uintptr_t*) &params[j]);
		}
		parentPointer = (int) paramArray->extra.length - 1;
	} else {
		params = &dummy;
	}

	//FIXME check for exception later
	int res = runMethod(threadId, method, 0, params, &parentPointer);
	if (res != 0) {
		printf("Exception in sun_reflect_NativeConstructorAccessorImpl_newInstance0\n");
	}

	if (paramArray != NULL) {
		free(params);
	}

	retVal->val.pointer = newObject;
	retVal->type = POINTER_JVM;

}
