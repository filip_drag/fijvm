/*
 * java_io_UnixFileSystem.h
 *
 *  Created on: Sep 6, 2015
 *      Author: fi
 */

#ifndef SRC_SYSTEM_LIB_NATIVE_PART_JAVA_IO_UNIXFILESYSTEM_H_
#define SRC_SYSTEM_LIB_NATIVE_PART_JAVA_IO_UNIXFILESYSTEM_H_
#include "type-union.h"
#include "threads.h"
void  Java_java_io_UnixFileSystem_initIDs(Thread* threadId, Value* retVal, Value* args);
void  Java_java_io_UnixFileSystem_canonicalize0(Thread* threadId, Value* retVal, Value* args);
void  Java_java_io_UnixFileSystem_getBooleanAttributes0(Thread* threadId, Value* retVal, Value* args);

#endif /* SRC_SYSTEM_LIB_NATIVE_PART_JAVA_IO_UNIXFILESYSTEM_H_ */
