#include "java_lang_Thread.h"
#include "cpu.h"
#include "threads.h"
#include "jni-helper.h"
#include "classes.h"

void  Java_java_lang_Thread_start0(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* obj = args->val.pointer;
	Thread* thread = createThread();
	thread->state = THREAD_STARTING;
	obj->type.nativeObject = thread;

	MethodInfo* runMethod = findMethodNoAccessFlags(obj->clazz, "run", "()V");

	Class* threadClass = findClassByQualifiedName("java/lang/Thread");
	FieldInfo* tidField = findField(threadClass, "tid", "J");
	Value tidVal;
	fieldValue(&tidVal, obj, tidField);
	thread->tid = tidVal.val.longVal;
	thread->javaThread = obj;

	ThreadParams* params = malloc(sizeof(ThreadParams));
	params->method = runMethod;
	params->thread = thread;
	params->parentStack = malloc(sizeof(Value));
	params->parentStack->val.pointer = obj;
	params->parentStack->type = POINTER_JVM;
	params->parentStackPointer = 0;
	pthread_create(&thread->nativeThread, NULL, (void * (*)(void *)) startMethodInThread, (void *) params);

}

void  Java_java_lang_Thread_registerNatives(Thread* threadId, Value* retVal, Value* args) {

}

void Java_java_lang_Thread_setPriority0(Thread* threadId, Value* retVal, Value* args) {
	//FIXME make this method really set the priority of the thread
}

void  Java_java_lang_Thread_currentThread(Thread* threadId, Value* retVal, Value* args) {
	retVal->val.pointer = threadId->javaThread;
	retVal->type = POINTER_JVM;
}

void  Java_java_lang_Thread_isAlive(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* thisObj = args->val.pointer;
	Thread* th = thisObj->type.nativeObject;
	int flag = (th != NULL && ( th->state == THREAD_RUNNING || th->state == THREAD_STARTING)) ? 1 : 0;
	retVal->val.intVal = flag;
	retVal->type = POINTER_JVM;
}
