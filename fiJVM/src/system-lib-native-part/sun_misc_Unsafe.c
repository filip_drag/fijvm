#include "sun_misc_Unsafe.h"
#include "array.h"
#include <string.h>
#include "class-def.h"
#include "type-union.h"
#include "jni-helper.h"
#include "java-object-consts.h"
#include "threads.h"

void  Java_sun_misc_Unsafe_registerNatives(Thread* threadId, Value* retVal, Value* args) {

}

void  Java_sun_misc_Unsafe_addressSize(Thread* threadId, Value* retVal, Value* args) {
	retVal->val.intVal = REF_SIZE;
	retVal->type = INT_JVM;
}

void  Java_sun_misc_Unsafe_arrayBaseOffset(Thread* threadId, Value* retVal, Value* args) {
	retVal->val.intVal = (int32_t) ARRAY_HEADER_SIZE;
	retVal->type = INT_JVM;
}

void  Java_sun_misc_Unsafe_arrayIndexScale(Thread* threadId, Value* retVal, Value* args) {
	JavaObject* thisClass = args->val.pointer;
	char* name = thisClass->clazz->className;
	int i, size = INT_SIZE;
	for (i = 0; i < (int)strlen(name); i++) {
		if (name[i] != '[') {
			break;
		}
	}

	if (name[i] == 'L') {
		size = REF_SIZE;
	} else if (name[i] == 'D' || name[i] == 'J') {
		size = DOUBLE_SIZE;
	}


	retVal->val.intVal = size;
	retVal->type = INT_JVM;
}


