/*
 * java_io_FileSystem.h
 *
 *  Created on: Sep 6, 2015
 *      Author: fi
 */

#ifndef SRC_SYSTEM_LIB_NATIVE_PART_JAVA_IO_FILESYSTEM_H_
#define SRC_SYSTEM_LIB_NATIVE_PART_JAVA_IO_FILESYSTEM_H_
#include "type-union.h"
#include "threads.h"
void  Java_java_io_FileSystem_getFileSystem(Thread* threadId, Value* retVal, Value* args);

#endif
