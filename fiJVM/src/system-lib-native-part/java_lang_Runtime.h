/*
 * java_lang_Runtime.h
 *
 *  Created on: Sep 6, 2015
 *      Author: fi
 */

#ifndef SRC_SYSTEM_LIB_NATIVE_PART_JAVA_LANG_RUNTIME_H_
#define SRC_SYSTEM_LIB_NATIVE_PART_JAVA_LANG_RUNTIME_H_

#include "type-union.h"
#include "threads.h"

void  Java_java_lang_Runtime_freeMemory(Thread* threadId, Value* retVal, Value* args);

#endif
