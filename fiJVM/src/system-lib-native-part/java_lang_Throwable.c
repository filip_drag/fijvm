#include "java_lang_Throwable.h"
#include <string.h>
#include "cpu.h"
#include "util.h"
#include "heap.h"
#include "classes.h"
#include "jni-helper.h"
#include "array.h"
#include "java-object-consts.h"
#include "threads.h"

void* stackTraceElementAt(Thread* threadId, int i);
size_t getCallStackSize(Thread* threadId);
Class* getStackTraceElementClass();
int getLineNumber(StackFrame* frame);
Class* stackTraceElementClass = NULL;

void  Java_java_lang_Throwable_fillInStackTrace(Thread* threadId, Value* retVal, Value* args) {
	int i, j;
	args--;
	void* ref = args->val.pointer;
	loadClassByQualifiedName(threadId, "java/lang/StackTraceElement", 0);
	Class* throwableClass = findClassByQualifiedName("java/lang/Throwable");
	FieldInfo* field = findField(throwableClass, "backtrace", "Ljava/lang/Object;");

	//get call stack size skipping the frames from java.lang.Throwable
	size_t callStackSize = getCallStackSize(threadId);

	//this cheat is used to record the line of the new Exception() rather than the line
	//of the current function call
	threadId->callStack[callStackSize - 1].returnAddress = threadId->callStack[callStackSize - 1].ip;
	Class* clazz = getStackTraceElementClass();
	void* array = allocArray(threadId, ARR_TYPE_REF, (u4) (callStackSize), clazz);
	Value val;
	val.type = POINTER_JVM;
	val.val.pointer = array;
	setField(&val, ref, field);

	for (i = 0, j = (int) callStackSize - 1; i < (int) callStackSize; i++, j--) {
		JavaObject* el = stackTraceElementAt(threadId, (int) i);

		//FIXME later propagate the ArrayIndexOutOfBounds exception from the native method
		int retCode = storeRefIntoArray(array, j, (uintptr_t) el);
	}

	retVal->type = POINTER_JVM;
	retVal->val.pointer = ref;

}
void  Java_java_lang_Throwable_getStackTraceDepth(Thread* threadId, Value* retVal, Value* args) {
	void* ref = args->val.pointer;
	Class* throwableClass = findClassByQualifiedName("java/lang/Throwable");
	FieldInfo* field = findField(throwableClass, "backtrace", "Ljava/lang/Object;");
	Value val;
	fieldValue(&val, ref, field);
	u4 arrayLength = getArrayLength(val.val.pointer);

	retVal->type = INT_JVM;
	retVal->val.intVal = (int32_t) arrayLength;
}
void  Java_java_lang_Throwable_getStackTraceElement(Thread* threadId, Value* retVal, Value* args) {
	int32_t index = args->val.intVal;
	args--;
	JavaObject* ref = args->val.pointer;
	Class* throwableClass = findClassByQualifiedName("java/lang/Throwable");
	FieldInfo* field = findField(throwableClass, "backtrace", "Ljava/lang/Object;");
	Value val;
	fieldValue(&val, ref, field);
	uintptr_t elementAtIndex;

	//FIXME propagate the ArrayIndexOutOfBounds exception throw the native method
	int res = loadRefFromArray(val.val.pointer, index, &elementAtIndex);

	retVal->type = POINTER_JVM;
	retVal->val.pointer = (void*) elementAtIndex;
}

Class* getStackTraceElementClass() {
	if (stackTraceElementClass == NULL) {
		stackTraceElementClass = findClassByQualifiedName("java/lang/StackTraceElement");
	}

	return stackTraceElementClass;
}

void* stackTraceElementAt(Thread* threadId, int i) {
	size_t k, j;

	StackFrame frame = threadId->callStack[i];
	Class* clazz = getStackTraceElementClass();
	JavaObject* stackTraceElement = allocObject(clazz);

	Value val;
	val.type = POINTER_JVM;
	FieldInfo* classField = findField(clazz, "declaringClass", "Ljava/lang/String;");
	k = strlen(frame.clazz->className) + 2;
	char* newChar = malloc(k);
	memset(newChar, 0 , k);
	strcat(newChar, frame.clazz->className);
	for(j = 0; j < k; j++) {
		if (newChar[j] == '/') {
			newChar[j] = '.';
		}
	}
	JavaObject* declaringClassString = charArrayToString(threadId, newChar);
	free(newChar);
	val.val.pointer = declaringClassString;
	setField(&val, stackTraceElement, classField);

	FieldInfo* methodField = findField(clazz, "methodName", "Ljava/lang/String;");
	JavaObject* methodNameString = charArrayToString(threadId, frame.method->methodName);
	val.val.pointer = methodNameString;
	setField(&val, stackTraceElement, methodField);

	FieldInfo* sourceFileField = findField(clazz, "fileName", "Ljava/lang/String;");
	JavaObject* sourceFileString = charArrayToString(threadId, frame.clazz->sourceFile);
	val.val.pointer = sourceFileString;
	setField(&val, stackTraceElement, sourceFileField);

	FieldInfo* lineNumberField = findField(clazz, "lineNumber", "I");
	val.val.intVal = getLineNumber(&frame);
	setField(&val, stackTraceElement, lineNumberField);

	return stackTraceElement;

}

//skip the subclasses of java.lang.Throwable
size_t getCallStackSize(Thread* threadId) {
	int i;
	int ctr  = 0;
	Class* throwableClass = findClassByQualifiedName("java/lang/Throwable");
	for (i = threadId->callStackPointer; i >= 0 ; i--) {
		StackFrame frame = threadId->callStack[i];
		if (hasSuperClass(frame.clazz, throwableClass)) {
			ctr++;
		}
	}

	return (size_t) (threadId->callStackPointer + 1 - ctr);
}

int getLineNumber(StackFrame* frame) {
	int i;
	u2 pc = (u2) frame->returnAddress;
	MethodInfo* method = frame->method;

	u2 prevPc = 0;
	u2 prevLine = 0;
	u1* ptr = method->lineNumberTable;
	for (i = 0; i < method->lineNumberTableLength; i++) {
		u2 currPc = BYTES_TO_U2(ptr);
		u2 currLine = BYTES_TO_U2(ptr + 2);
		if (pc >= prevPc && pc < currPc) {
			return (int) prevLine;
		}

		prevPc = currPc;
		prevLine = currLine;

		ptr += 4;
	}

	return (int) prevLine;
}
