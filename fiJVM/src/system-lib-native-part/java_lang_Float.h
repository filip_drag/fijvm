#ifndef JAVA_LANG_FLOAT_H_
#define JAVA_LANG_FLOAT_H_
#include "type-union.h"
#include "threads.h"

void  Java_java_lang_Float_floatToRawIntBits(Thread* threadId, Value* retVal, Value* args);
void  Java_java_lang_Float_intBitsToFloat(Thread* threadId, Value* retVal, Value* args);

#endif
