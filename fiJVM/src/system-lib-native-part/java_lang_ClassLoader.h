#ifndef SRC_SYSTEM_LIB_NATIVE_PART_JAVA_LANG_CLASSLOADER_H_
#define SRC_SYSTEM_LIB_NATIVE_PART_JAVA_LANG_CLASSLOADER_H_

#include "type-union.h"
#include "threads.h"

void  Java_java_lang_ClassLoader_registerNatives(Thread* threadId, Value* retVal, Value* args);
#endif
