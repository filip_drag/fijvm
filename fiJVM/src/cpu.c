#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include "cpu.h"
#include "access-flags.h"
#include "bytecodes.h"
#include "class.h"
#include "classes.h"
#include "util.h"
#include "printf.h"
#include "heap.h"
#include "descriptors.h"
#include "array.h"
#include "string-pool.h"
#include "jni-methods.h"
#include "jni-helper.h"
#include "java-object-consts.h"
#include "threads.h"
#include "debugger.h"

#define BRANCH_BY_TWO_BYTES(byte1, byte2, currIp, refIp) {\
				currIp = refIp + (short) ((byte1 << 8) + byte2);}

#define READ_NEXT_TWO_BYTES(currIp) u1 byte1 = *(currIp++);\
									u1 byte2 = *(currIp++)

#define GET_INDEX_BY_TWO_BYTES(instPointer, index) {\
					u1 byte1 = *(instPointer++);\
					u1 byte2 = *(instPointer++);\
					index = (size_t) ((byte1 << 8) + byte2) - 1;\
}\

#define POP_TWO_INTS(operandStack, operandStackPointer) int32_t val2 = operandStack[operandStackPointer--].val.intVal;\
										int32_t val1 = operandStack[operandStackPointer--].val.intVal

#define POP_TWO_FLOATS(operandStack, operandStackPointer) float val2 = operandStack[operandStackPointer--].val.floatVal;\
										float val1 = operandStack[operandStackPointer--].val.floatVal

#define POP_TWO_DOUBLES(operandStack, operandStackPointer) double val2 = operandStack[operandStackPointer--].val.doubleVal;\
										double val1 = operandStack[operandStackPointer--].val.doubleVal

#define POP_TWO_LONGS(operandStack, operandStackPointer) int64_t val2 = operandStack[operandStackPointer--].val.longVal;\
										int64_t val1 = operandStack[operandStackPointer--].val.longVal

#define POP_TWO_PTRS(operandStack, operandStackPointer) void* val2 = operandStack[operandStackPointer--].val.pointer;\
										void* val1 = operandStack[operandStackPointer--].val.pointer
/*
#define ARRAY_LOAD(type, methodWord, typeUpperCase, propName) int32_t index = operandStack[operandStackPointer--].val.intVal;\
					void* ptr = operandStack[operandStackPointer--].val.pointer;\
					#type value;\
					int code = load ## methodWord ## FromArray(ptr, index, &value);\
					if (code != 0) {\
					}\
					operandStack[++operandStackPointer].type = typeUpperCase;\
					operandStack[operandStackPointer].val. # propVal = value;\*/


#define EXCEPTION_HIT() Value val = operandStack[operandStackPointer--];\
						operandStackPointer = 0;\
						operandStack[operandStackPointer] = val;\
						void* exception = val.val.pointer;\
						u1* newIp = exceptionJump(currMethod, exception, (int) ( currInstPointer - code));\
						if (newIp != NULL) {\
							instPointer = newIp;\
							break;\
						} else {\
							*parentStackPointer = *parentStackPointer + 1;\
							parentStack[*parentStackPointer] = val;\
							isRunning = 0;\
							returnCode = EXCEPTION_RET_CODE;\
							break;\
						}\

#define THROW_EXCEPTION(className, message) {\
					*parentStackPointer = *parentStackPointer + 1;\
					parentStack[*parentStackPointer].val.pointer = createException(threadId, className, message, (int) ( currInstPointer - code));\
					parentStack[*parentStackPointer].type = POINTER_JVM;\
					isRunning = 0;\
					returnCode = EXCEPTION_RET_CODE;\
					break;\
						}\

#define SET_OBJECT_FIELD(obj, value, fieldName, descriptor ) {\
							FieldInfo* field = findField(obj->clazz, fieldName, descriptor);\
							Value val;\
							val.val.pointer = value;\
							val.type = POINTER_JVM;\
							setField(&val, obj, field);\
							}\

#define SET_INT_FIELD(obj, value, fieldName , descriptor) {\
							FieldInfo* field = findField(obj->clazz, fieldName, descriptor);\
							Value val;\
							val.val.intVal = value;\
							val.type = POINTER_JVM;\
							setField(&val, obj, field);\
							}\

#define SET_LONG_FIELD(obj, value, fieldName ) {\
							FieldInfo* field = findField(obj->clazz, fieldName, "J");\
							Value val;\
							val.val.longVal = value;\
							val.type = POINTER_JVM;\
							setField(&val, obj, field);\
							}\

#define EXCEPTION_RET_CODE (5)
#define ERROR_RET_CODE     (6)
#define RUNTIME_EXCEPTION_RET_CODE (7)

#define CAST_TO_U64(res) (*((uint64_t*)&(res)))
#define CAST_TO_S32(res) (*((int32_t*)&(res)))

int checkArrays(JavaObject* ref, char* descriptor);
void printStackTrace(Thread* threadId, JavaObject* obj, int returnAddress);
JavaObject* createMainJavaThread();
void threadDead(void* arg);
void initSystemClassLoader(Thread* thread);
void initSystemProperties(Thread* thread);

typedef struct {
	Thread* thread;
	char* className;
} MainParams;

int startMain(char* className) {
	Thread* threadId = createThread();

	classLoadingPhase = 0;
	loadClassByQualifiedName(threadId, "java/lang/Class", 0);
	loadClassByQualifiedName(threadId, "java/lang/String", 0);
	classLoadingPhase = 1;
	runSkippedClassInits(threadId, 0);

	JavaObject* mainJavaThread = createMainJavaThread();
	classLoadingPhase = 2;

	threadId->javaThread = mainJavaThread;

	//bind the native thread to the java thread
	mainJavaThread->type.nativeObject = threadId;

	//set tid
	Class* threadClass = findClassByQualifiedName("java/lang/Thread");
	FieldInfo* tidField = findField(threadClass, "tid", "J");
	Value tidVal;
	fieldValue(&tidVal, mainJavaThread, tidField);
	threadId->tid = tidVal.val.longVal;

	//initialize System properties
	initSystemProperties(threadId);
	//initialize context class loader
	initSystemClassLoader(threadId);

	MainParams params;
	params.className = className;
	params.thread = threadId;
	pthread_create(&threadId->nativeThread, NULL, (void * (*)(void *)) start, (void *) &params);
	int returnValue;
	int retVal = 0;
	int runFlag = 1;
	int i = 0;
	Thread* thread = NULL;
	while (runFlag) {
		pthread_mutex_lock(&threadListMutex);
		thread = threadList[i];
		pthread_mutex_unlock(&threadListMutex);

		if (thread == NULL) {
			i++;
			continue;
		}

		pthread_join(thread->nativeThread, (void*) &returnValue);

		if (returnValue != 0) {
			retVal = returnValue;
		}

		pthread_mutex_lock(&threadListMutex);
		if (threadCount <= 0) {
			runFlag = 0;
		}
		pthread_mutex_unlock(&threadListMutex);

		i++;
		if (i >= MAX_THREAD_COUNT) {
			i = 0;
		}
	}

	return retVal;
}

void threadDead(void* arg) {
	Thread* th = (Thread*)arg;
	th->state = THREAD_DEAD;
	JavaObject* javaThread = th->javaThread;
	pthread_mutex_lock(&javaThread->mutex);
	javaThread->type.nativeObject = NULL;
	pthread_mutex_unlock(&javaThread->mutex);

	//FIXME add exception throws
	//broadcast on this thead condvar if someone is joining on the thread
	int res = pthread_cond_broadcast(&javaThread->condVar);
	deleteThread(th);
}

void* start(void* params) {
	MainParams* p = (MainParams*) params;
	Thread* threadId = p->thread;
	int code;
	p->thread->state = THREAD_RUNNING;
	pthread_cleanup_push(threadDead, threadId);

	//run the skipped during the classloading
	runSkippedClinits(threadId, 0);

	Class* mainClass = loadClassByQualifiedName(threadId, p->className, 0);
	MethodInfo* mainMethod = findMethod(mainClass, "main", "([Ljava/lang/String;)V", ACC_PUBLIC | ACC_STATIC);
	if (mainMethod == NULL) {
		PRINTF("main method not  found\n");
		return (void*) (intptr_t) -1;
	}

	Value parentStack;
	parentStack.val.pointer = NULL;
	parentStack.type = POINTER_JVM;
	int parentStackPointer = 0;
	code = runMethod(threadId, mainMethod , 0, &parentStack, &parentStackPointer);
	if (code == EXCEPTION_RET_CODE) {
		// print stack trace;
		printStackTrace(threadId, parentStack.val.pointer, 0);
	}
	pthread_cleanup_pop(1);
	return (void*) (intptr_t)code;
}

void initThread(Thread* threadId) {
	threadId->callStackPointer = -1;
}

void startMethodInThread(void* params) {
	Value parentStack;
	int parentStackPointer = 0;
	ThreadParams* p = (ThreadParams*) params;
	p->thread->state = THREAD_RUNNING;
	//this is function is called if the thread is kill abnormally
	pthread_cleanup_push(threadDead, p->thread);

	if (p->parentStack->val.pointer != NULL) {
		//use the passed arguments if any
		parentStack.val.pointer = p->parentStack->val.pointer;
		parentStack.type = POINTER_JVM;
		parentStackPointer = p->parentStackPointer;
	} else {
		parentStack.val.pointer = NULL;
		parentStack.type = POINTER_JVM;
	}
	int code = runMethod(p->thread, p->method , 0, &parentStack, &parentStackPointer);
	if (code == EXCEPTION_RET_CODE) {
		// print stack trace;
		printStackTrace(p->thread, parentStack.val.pointer, 0);
	}
	pthread_cleanup_pop(1);
	free(p->parentStack);
	free(p);
}

int runMethod(Thread* threadId, MethodInfo* currMethod , int returnAddress, Value* parentStack, int* parentStackPointer) {
	u2 maxStack = currMethod->maxStack,
			maxLocals = currMethod->maxLocals;
	u4 codeLength = currMethod->codeLength;
	u1* code = currMethod->code;
	Class* clazz = currMethod->clazz;
	PRINTF("maxStack=%d maxLocals=%d, codeLength=%d code=%p\n", maxStack, maxLocals, codeLength, code);
	
	int retCode = pushStackFrame(threadId, returnAddress, maxLocals, maxStack, currMethod);
	if (retCode != 0) {
		PRINTF("Opps bad code!\n");
		return retCode;
	}
	Value* operandStack = threadId->callStack[threadId->callStackPointer].operandStack;
	Value* locals = threadId->callStack[threadId->callStackPointer].localVariables;
	u1* localIndexes = currMethod->localVarsIndexes;
	JavaObject* thisReference = NULL;
	if (parentStack != NULL) {
		size_t startIndex = 0;
		if ((currMethod->accessFlags & ACC_STATIC ) == 0) {
			startIndex = 1;
		}
		initLocals(locals, parentStack, parentStackPointer, currMethod->parameterCount, (int) startIndex, localIndexes);
		if ((currMethod->accessFlags & ACC_STATIC ) == 0) {
			locals[0] = parentStack[(*parentStackPointer)--];
			//this is used for synchronized methods
			thisReference = locals[0].val.pointer;
		} else {
			//this is used for synchronized methods of a static method
			thisReference = currMethod->clazz->java_Lang_Class;
		}
	}

	if ((currMethod->accessFlags & ACC_SYNCHRONIZED) != 0) {
		pthread_mutex_lock (&thisReference->mutex);
	}

	int operandStackPointer = -1;

	//tells us is the next instruction must be considered with wide index
	int wideFlag = 0;

	//holds the current instruction location
	u1* currInstPointer;
	u1* instPointer = code;
	int isRunning = 1;
	size_t index;
	int returnCode = 0;
	while (isRunning) {
		u1 instruction = *instPointer;
		currInstPointer = instPointer;
		instPointer++;
		switch (instruction) {
			case ICONST_M1:
				operandStack[++operandStackPointer].val.intVal = -1;
				operandStack[operandStackPointer].type = INT_JVM;
				break;
			case ICONST_0:
				operandStack[++operandStackPointer].val.intVal = 0;
				operandStack[operandStackPointer].type = INT_JVM;
				break;
			case ICONST_1:
				operandStack[++operandStackPointer].val.intVal = 1;
				operandStack[operandStackPointer].type = INT_JVM;
				break;
			case ICONST_2:
				operandStack[++operandStackPointer].val.intVal = 2;
				operandStack[operandStackPointer].type = INT_JVM;
				break;
			case ICONST_3:
				operandStack[++operandStackPointer].val.intVal = 3;
				operandStack[operandStackPointer].type = INT_JVM;
				break;
			case ICONST_4:
				operandStack[++operandStackPointer].val.intVal = 4;
				operandStack[operandStackPointer].type = INT_JVM;
				break;
			case ICONST_5:
				operandStack[++operandStackPointer].val.intVal = 5;
				operandStack[operandStackPointer].type = INT_JVM;
				break;
			case FCONST_0:
				operandStack[++operandStackPointer].val.floatVal = 0.0f;
				operandStack[operandStackPointer].type = FLOAT_JVM;
				break;
			case FCONST_1:
				operandStack[++operandStackPointer].val.floatVal = 1.0f;
				operandStack[operandStackPointer].type = FLOAT_JVM;
				break;
			case FCONST_2:
				operandStack[++operandStackPointer].val.floatVal = 2.0f;
				operandStack[operandStackPointer].type = FLOAT_JVM;
				break;
			case ACONST_NULL:
				operandStack[++operandStackPointer].val.pointer = NULL;
				operandStack[operandStackPointer].type = POINTER_JVM;
				break;
			case DCONST_0:
				operandStack[++operandStackPointer].val.doubleVal = 0.0;
				operandStack[operandStackPointer].type = DOUBLE_JVM;
				break;
			case DCONST_1:
				operandStack[++operandStackPointer].val.doubleVal = 1.0;
				operandStack[operandStackPointer].type = DOUBLE_JVM;
				break;
			case LCONST_0:
				operandStack[++operandStackPointer].val.longVal = 0L;
				operandStack[operandStackPointer].type = LONG_JVM;
				break;
			case LCONST_1:
				operandStack[++operandStackPointer].val.longVal = 1L;
				operandStack[operandStackPointer].type = LONG_JVM;
				break;
			case LSTORE:
			case DSTORE:
			case FSTORE:
			case ASTORE:
			case ISTORE: {
					if (wideFlag == 1) {
						GET_INDEX_BY_TWO_BYTES(instPointer, index)
						wideFlag = 0;
					} else {
						index = *(instPointer++);
					}
					locals[index] = operandStack[operandStackPointer--];
				}
				break;
			case LSTORE_0:
			case DSTORE_0:
			case FSTORE_0:
			case ASTORE_0:
			case ISTORE_0:
				locals[0] = operandStack[operandStackPointer--];
				break;
			case LSTORE_1:
			case DSTORE_1:
			case FSTORE_1:
			case ASTORE_1:
			case ISTORE_1:
				locals[1] = operandStack[operandStackPointer--];
				break;
			case LSTORE_2:
			case DSTORE_2:
			case FSTORE_2:
			case ASTORE_2:
			case ISTORE_2:
				locals[2] = operandStack[operandStackPointer--];
				break;
			case LSTORE_3:
			case DSTORE_3:
			case FSTORE_3:
			case ASTORE_3:
			case ISTORE_3:
				locals[3] = operandStack[operandStackPointer--];
				break;
			case LLOAD:
			case DLOAD:
			case FLOAD:
			case ALOAD:
			case ILOAD: {
					if (wideFlag == 1) {
						GET_INDEX_BY_TWO_BYTES(instPointer, index)
						wideFlag = 0;
					} else {
						index = *(instPointer++);
					}
					operandStack[++operandStackPointer] = locals[index];
				}
				break;
			case LLOAD_0:
			case DLOAD_0:
			case FLOAD_0:
			case ALOAD_0:
			case ILOAD_0:
				operandStack[++operandStackPointer] = locals[0];
				break;
			case LLOAD_1:
			case DLOAD_1:
			case FLOAD_1:
			case ALOAD_1:
			case ILOAD_1:
				operandStack[++operandStackPointer] = locals[1];
				break;
			case LLOAD_2:
			case DLOAD_2:
			case FLOAD_2:
			case ALOAD_2:
			case ILOAD_2: {
				Value val = locals[2];
				++operandStackPointer;
				operandStack[operandStackPointer] = val;
				}
				break;
			case LLOAD_3:
			case DLOAD_3:
			case FLOAD_3:
			case ALOAD_3:
			case ILOAD_3:
				operandStack[++operandStackPointer] = locals[3];
				break;
			case LDC_W:
			case LDC: {
					if (instruction == LDC_W) {
						GET_INDEX_BY_TWO_BYTES(instPointer, index)
					} else {
						index =  (size_t) *(instPointer++) - 1;
					}
					ConstantPoolInfo* entry = &clazz->constantPoolArray[index];
					u1 tag = entry->tag;
					switch (tag) {
						case CONSTANT_Integer:
							operandStack[++operandStackPointer].val.intVal = *((int32_t*)&entry->bytes);
							operandStack[operandStackPointer].type = INT_JVM;
							break;
						case CONSTANT_Float:
							operandStack[++operandStackPointer].val.floatVal = *((float*)&entry->bytes);
							operandStack[operandStackPointer].type = FLOAT_JVM;
							break;
						case CONSTANT_String: {
								char* stringConst = getUtfStringAtIndex(clazz, (size_t) entry->stringIndex - 1);
								JavaObject* stringObj  = addToStringPoolIfNotExists(threadId, stringConst);
								operandStack[++operandStackPointer].val.pointer = stringObj;
								operandStack[operandStackPointer].type = POINTER_JVM;
							}
							break;
						case CONSTANT_Class: {
							const char* className = getUtfStringAtIndex(clazz, (size_t) entry->nameIndex - 1);
							Class* clazz = loadClassByQualifiedName(threadId, className, (int) ( currInstPointer - code));
							operandStack[++operandStackPointer].val.pointer = clazz->java_Lang_Class;
							operandStack[operandStackPointer].type = POINTER_JVM;
						}
						break;
					}
				}
				break;
			case LDC2_W: {
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					ConstantPoolInfo* entry = &clazz->constantPoolArray[index];
					getWideConstant(entry, &operandStack[++operandStackPointer]);
				} break;
			case GOTO: {
					READ_NEXT_TWO_BYTES(instPointer);
					BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
				}
				break;
			case GOTO_W: {
					u1 byte1 = *(instPointer++);
					u1 byte2 = *(instPointer++);
					u1 byte3 = *(instPointer++);
					u1 byte4 = *(instPointer++);
					instPointer = currInstPointer + (int) ((byte1 << 24) + (byte2 << 16) + (byte3 << 8) + byte4);
				}
				break;
			case BIPUSH: {
					u1 byte1 = *(instPointer++);
					operandStack[++operandStackPointer].val.intVal = (int)(*((signed char*)&byte1));
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case SIPUSH: {
					u1 byte1 = *(instPointer++);
					u1 byte2 = *(instPointer++);
					int16_t shortVal = (int16_t)((byte1 << 8 ) + byte2);
					operandStack[++operandStackPointer].val.intVal = shortVal;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case DUP: {
					int currPointer = operandStackPointer;
					operandStack[++operandStackPointer]  = operandStack[currPointer];
				}
				break;
			case DUP2: {
					if (operandStack[operandStackPointer].type == DOUBLE_JVM ||
							operandStack[operandStackPointer].type == LONG_JVM) {
						int currPointer = operandStackPointer;
						operandStack[++operandStackPointer]  = operandStack[currPointer];
						break;
					}
					Value first = operandStack[operandStackPointer--];
					Value second = operandStack[operandStackPointer--];
					operandStack[++operandStackPointer]  = second;
					operandStack[++operandStackPointer]  = first;
					operandStack[++operandStackPointer]  = second;
					operandStack[++operandStackPointer]  = first;

				}
				break;
			case DUP_X1: {
					Value first = operandStack[operandStackPointer--];
					Value second = operandStack[operandStackPointer--];
					operandStack[++operandStackPointer]  = first;
					operandStack[++operandStackPointer]  = second;
					operandStack[++operandStackPointer]  = first;
				} break;
			case DUP_X2: {
					Value first = operandStack[operandStackPointer--];
					Value second = operandStack[operandStackPointer--];
					Value third = operandStack[operandStackPointer--];
					operandStack[++operandStackPointer]  = first;
					operandStack[++operandStackPointer]  = third;
					operandStack[++operandStackPointer]  = second;
					operandStack[++operandStackPointer]  = first;
				} break;
			case WIDE: {
					wideFlag = 1;
				} break;
			case FCMPG: {
					POP_TWO_FLOATS(operandStack, operandStackPointer);
					int32_t res = 0;
					if (isnan(val1) || isnan(val2)) {
						res = 1;
					} else if (val1 == val2) {
						res = 0;
					} else if (val1 < val2) {
						res = -1;
					} else {
						res = 1;
					}
					operandStack[++operandStackPointer].val.intVal = res;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case FCMPL: {
					POP_TWO_FLOATS(operandStack, operandStackPointer);
					int32_t res = 0;
					if (isnan(val1) || isnan(val2)) {
						res = -1;
					} else if (val1 == val2) {
						res = 0;
					} else if (val1 < val2) {
						res = -1;
					} else {
						res = 1;
					}
					operandStack[++operandStackPointer].val.intVal = res;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case DCMPG: {
					POP_TWO_DOUBLES(operandStack, operandStackPointer);
					int32_t res = 0;
					if (isnan((float)val1) || isnan((float)val2)) {
						res = 1;
					} else if (val1 == val2) {
						res = 0;
					} else if (val1 < val2) {
						res = -1;
					} else {
						res = 1;
					}
					operandStack[++operandStackPointer].val.intVal = res;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case DCMPL: {
					POP_TWO_DOUBLES(operandStack, operandStackPointer);
					int32_t res = 0;
					if (isnan((float)val1) || isnan((float)val2)) {
						res = -1;
					} else if (val1 == val2) {
						res = 0;
					} else if (val1 < val2) {
						res = -1;
					} else {
						res = 1;
					}
					operandStack[++operandStackPointer].val.intVal = res;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case IFEQ: {
					READ_NEXT_TWO_BYTES(instPointer);
					int32_t val = operandStack[operandStackPointer--].val.intVal;
					if (val == 0) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IFGE: {
					READ_NEXT_TWO_BYTES(instPointer);
					int32_t val = operandStack[operandStackPointer--].val.intVal;
					if (val >= 0) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IFGT: {
					READ_NEXT_TWO_BYTES(instPointer);
					int32_t val = operandStack[operandStackPointer--].val.intVal;
					if (val > 0) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IFLE: {
					READ_NEXT_TWO_BYTES(instPointer);
					int32_t val = operandStack[operandStackPointer--].val.intVal;
					if (val <= 0) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IFLT: {
					READ_NEXT_TWO_BYTES(instPointer);
					int32_t val = operandStack[operandStackPointer--].val.intVal;
					if (val < 0) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IFNE: {
					READ_NEXT_TWO_BYTES(instPointer);
					int32_t val = operandStack[operandStackPointer--].val.intVal;
					if (val != 0) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IFNONNULL: {
					READ_NEXT_TWO_BYTES(instPointer);
					void* val = operandStack[operandStackPointer--].val.pointer;
					if (val != NULL) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IFNULL: {
					READ_NEXT_TWO_BYTES(instPointer);
					void* val = operandStack[operandStackPointer--].val.pointer;
					if (val == NULL) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IF_ICMPLT:
				{
					POP_TWO_INTS(operandStack, operandStackPointer);
					READ_NEXT_TWO_BYTES(instPointer);
					if (val1 < val2) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IF_ICMPLE:
				{
					POP_TWO_INTS(operandStack, operandStackPointer);
					READ_NEXT_TWO_BYTES(instPointer);
					if (val1 <= val2) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IF_ICMPGT:
				{
					POP_TWO_INTS(operandStack, operandStackPointer);
					READ_NEXT_TWO_BYTES(instPointer);
					if (val1 > val2) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IF_ICMPGE:
				{
					POP_TWO_INTS(operandStack, operandStackPointer);
					READ_NEXT_TWO_BYTES(instPointer);
					if (val1 >= val2) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IF_ICMPEQ:
				{
					POP_TWO_INTS(operandStack, operandStackPointer);
					READ_NEXT_TWO_BYTES(instPointer);
					if (val1 == val2) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IF_ICMPNE:
				{
					POP_TWO_INTS(operandStack, operandStackPointer);
					READ_NEXT_TWO_BYTES(instPointer);
					if (val1 != val2) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				}
				break;
			case IF_ACMPEQ: {
					POP_TWO_PTRS(operandStack, operandStackPointer);
					READ_NEXT_TWO_BYTES(instPointer);
					if (val1 == val2) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				} break;
			case IF_ACMPNE: {
					POP_TWO_PTRS(operandStack, operandStackPointer);
					READ_NEXT_TWO_BYTES(instPointer);
					if (val1 != val2) {
						BRANCH_BY_TWO_BYTES(byte1, byte2, instPointer, currInstPointer);
					}
				} break;
			case IMUL: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					int32_t res = val1 * val2;
					operandStack[++operandStackPointer].val.intVal = res;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case IDIV: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					int32_t res = val1 / val2;
					operandStack[++operandStackPointer].val.intVal = res;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case INEG: {
					int32_t val = operandStack[operandStackPointer--].val.intVal;
					operandStack[++operandStackPointer].val.intVal = -val;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case IOR: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.intVal = val1 | val2;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case IREM: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.intVal = val1 % val2;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case ISUB: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.intVal = val1 - val2;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case ISHL: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.intVal = val1 << val2;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case ISHR: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.intVal = val1 >> val2;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case IUSHR: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					uint32_t us = *((uint32_t*)&val1) >> val2;
					int32_t res = *((int32_t*)&us);
					operandStack[++operandStackPointer].val.intVal = res;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case IXOR: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.intVal = val1 ^ val2;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case IINC: {
					int16_t offset;
					if (wideFlag == 1) {
						GET_INDEX_BY_TWO_BYTES(instPointer, index)
						u1 byte1 = *(instPointer++);
						u1 byte2 = *(instPointer++);
						u2 bytes = (u2) (( byte1 <<  8) + byte2);
						offset = *((int16_t*)&bytes);
						wideFlag = 0;
					} else {
						index = *(instPointer++);
						u1 byte = *(instPointer++);
						offset = (int16_t)*((signed char*)&byte);
					}
					int32_t i = (int32_t) offset;
					locals[index].val.intVal += i;
				}
				break;
			case IADD: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.intVal = val1 + val2;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case IAND: {
					POP_TWO_INTS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.intVal = val1 & val2;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case LMUL: {
					POP_TWO_LONGS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.longVal = val1 * val2;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case LDIV: {
					POP_TWO_LONGS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.longVal = val1 / val2;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case LNEG: {
					int64_t val = operandStack[operandStackPointer--].val.longVal;
					operandStack[++operandStackPointer].val.longVal = -val;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case LOR: {
					POP_TWO_LONGS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.longVal = val1 | val2;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case LREM: {
					POP_TWO_LONGS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.longVal = val1 % val2;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case LSUB: {
					POP_TWO_LONGS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.longVal = val1 - val2;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case LSHL: {
					int32_t val2 = operandStack[operandStackPointer--].val.intVal;
					int64_t val1 = operandStack[operandStackPointer--].val.longVal;
					operandStack[++operandStackPointer].val.longVal = val1 << val2;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case LSHR: {
					int32_t val2 = operandStack[operandStackPointer--].val.intVal;
					int64_t val1 = operandStack[operandStackPointer--].val.longVal;
					operandStack[++operandStackPointer].val.longVal = val1 >> val2;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case LUSHR: {
					int32_t val2 = operandStack[operandStackPointer--].val.intVal;
					int64_t val1 = operandStack[operandStackPointer--].val.longVal;
					uint64_t us = *((uint64_t*)&val1) >> val2;
					int64_t res = *((int64_t*)&us);
					operandStack[++operandStackPointer].val.longVal = res;
					operandStack[operandStackPointer].type = LONG_JVM;
				} break;
			case LXOR: {
					POP_TWO_LONGS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.longVal = val1 ^ val2;
					operandStack[operandStackPointer].type = LONG_JVM;
				} break;
			case LADD: {
					POP_TWO_LONGS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.longVal = val1 + val2;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case LAND: {
					POP_TWO_LONGS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.longVal = val1 & val2;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case LCMP: {
				POP_TWO_LONGS(operandStack, operandStackPointer);
				int32_t res = 0;
				if (val1 == val2) {
					res = 0;
				} else if (val1 < val2) {
					res = -1;
				} else {
					res = 1;
				}
				operandStack[++operandStackPointer].val.intVal = res;
				operandStack[operandStackPointer].type = INT_JVM;
				} break;
			case FADD: {
					POP_TWO_FLOATS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.floatVal = val1 + val2;
					operandStack[operandStackPointer].type = FLOAT_JVM;
				}
				break;
			case FDIV: {
					POP_TWO_FLOATS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.floatVal = val1 / val2;
					operandStack[operandStackPointer].type = FLOAT_JVM;
				}
				break;
			case FMUL: {
					POP_TWO_FLOATS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.floatVal = val1 * val2;
					operandStack[operandStackPointer].type = FLOAT_JVM;
				}
				break;
			case FNEG: {
					float val1 = operandStack[operandStackPointer--].val.floatVal;
					operandStack[++operandStackPointer].val.floatVal = -val1;
					operandStack[operandStackPointer].type = FLOAT_JVM;
				}
				break;
			case FREM: {
					POP_TWO_FLOATS(operandStack, operandStackPointer);
					int32_t division = (int32_t) (val1 / val2);
					float res = val1 - val2 * (float) division;

					operandStack[++operandStackPointer].val.floatVal = res;
					operandStack[operandStackPointer].type = FLOAT_JVM;
				}
				break;
			case FSUB: {
					POP_TWO_FLOATS(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.floatVal = val1 - val2;
					operandStack[operandStackPointer].type = FLOAT_JVM;
				}
				break;
			case DADD: {
					POP_TWO_DOUBLES(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.doubleVal = val1 + val2;
					operandStack[operandStackPointer].type = DOUBLE_JVM;
				}
				break;
			case DDIV: {
				POP_TWO_DOUBLES(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.doubleVal = val1 / val2;
					operandStack[operandStackPointer].type = DOUBLE_JVM;
				}
				break;
			case DMUL: {
				POP_TWO_DOUBLES(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.doubleVal = val1 * val2;
					operandStack[operandStackPointer].type = DOUBLE_JVM;
				}
				break;
			case DNEG: {
					double val1 = operandStack[operandStackPointer--].val.doubleVal;
					operandStack[++operandStackPointer].val.doubleVal = -val1;
					operandStack[operandStackPointer].type = DOUBLE_JVM;
				}
				break;
			case DREM: {
					POP_TWO_DOUBLES(operandStack, operandStackPointer);
					int32_t division = (int32_t) (val1 / val2);
					double res = val1 - val2 * (double) division;

					operandStack[++operandStackPointer].val.doubleVal = res;
					operandStack[operandStackPointer].type = DOUBLE_JVM;
				}
				break;
			case DSUB: {
					POP_TWO_DOUBLES(operandStack, operandStackPointer);
					operandStack[++operandStackPointer].val.doubleVal = val1 - val2;
					operandStack[operandStackPointer].type = DOUBLE_JVM;
				}
				break;
			case I2B: {
					int32_t val1 = operandStack[operandStackPointer--].val.intVal;
					operandStack[++operandStackPointer].val.byteVal = (int8_t) val1;
					operandStack[operandStackPointer].type = BYTE_JVM;
				}
				break;
			case I2C: {
					int32_t val1 = operandStack[operandStackPointer--].val.intVal;
					operandStack[++operandStackPointer].val.charVal = (uint16_t) val1;
					operandStack[operandStackPointer].type = CHAR_JVM;
				}
				break;
			case I2D: {
					int32_t val1 = operandStack[operandStackPointer--].val.intVal;
					operandStack[++operandStackPointer].val.doubleVal = (double) val1;
					operandStack[operandStackPointer].type = DOUBLE_JVM;
				}
				break;
			case I2F: {
					int32_t val1 = operandStack[operandStackPointer--].val.intVal;
					operandStack[++operandStackPointer].val.floatVal = (float) val1;
					operandStack[operandStackPointer].type = FLOAT_JVM;
				}
				break;
			case I2L: {
					int32_t val1 = operandStack[operandStackPointer--].val.intVal;
					operandStack[++operandStackPointer].val.longVal = (int64_t) val1;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case I2S: {
					int32_t val1 = operandStack[operandStackPointer--].val.intVal;
					operandStack[++operandStackPointer].val.shortVal = (int16_t) val1;
					operandStack[operandStackPointer].type = SHORT_JVM;
				}
				break;
			case F2D: {
					float val1 = operandStack[operandStackPointer--].val.floatVal;
					operandStack[++operandStackPointer].val.doubleVal = (double) val1;
					operandStack[operandStackPointer].type = DOUBLE_JVM;
				}
				break;
			case F2I: {
					float val1 = operandStack[operandStackPointer--].val.floatVal;
					operandStack[++operandStackPointer].val.intVal = (int32_t) val1;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case F2L: {
					float val1 = operandStack[operandStackPointer--].val.floatVal;
					operandStack[++operandStackPointer].val.longVal = (int64_t) val1;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case D2F: {
					double val1 = operandStack[operandStackPointer--].val.doubleVal;
					operandStack[++operandStackPointer].val.floatVal = (float) val1;
					operandStack[operandStackPointer].type = FLOAT_JVM;
				}
				break;
			case D2I: {
					double val1 = operandStack[operandStackPointer--].val.doubleVal;
					operandStack[++operandStackPointer].val.intVal = (int32_t) val1;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case D2L: {
					double val1 = operandStack[operandStackPointer--].val.doubleVal;
					operandStack[++operandStackPointer].val.longVal = (int64_t) val1;
					operandStack[operandStackPointer].type = LONG_JVM;
				}
				break;
			case L2D: {
					int64_t val1 = operandStack[operandStackPointer--].val.longVal;
					operandStack[++operandStackPointer].val.doubleVal = (double) val1;
					operandStack[operandStackPointer].type = DOUBLE_JVM;
				}
				break;
			case L2I: {
					int64_t val1 = operandStack[operandStackPointer--].val.longVal;
					operandStack[++operandStackPointer].val.intVal = (int32_t) val1;
					operandStack[operandStackPointer].type = INT_JVM;
				}
				break;
			case L2F: {
					int64_t val1 = operandStack[operandStackPointer--].val.longVal;
					operandStack[++operandStackPointer].val.floatVal = (float) val1;
					operandStack[operandStackPointer].type = FLOAT_JVM;
				}
				break;
			case GETSTATIC:
				{
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					char* className = getClassNameByRef(clazz, index);
					//if (strcmp(className, "java/lang/System") == 0) {
					//	break;
					//}
					//The execution of any one of the Java Virtual Machine instructions new,
					//getstatic, putstatic, or invokestatic that references the class or interface
					Class* destClass = loadClassByQualifiedName(threadId, className, (int) ( currInstPointer - code));
					char* fieldName = getNameFromNameAndType(clazz, index);
					//size_t fieldSize = getFieldSize(destClass, fieldName);
					char* fieldDescriptor = getTypeFromNameAndType(clazz, index);
					//assume that uintptr_t is 8 bytes
					void* fieldReference = getFieldReference(destClass, fieldName);
					getRefValue(&operandStack[++operandStackPointer], fieldReference, fieldDescriptor); //the high bytes
				}
				break;
			case PUTSTATIC:
				{
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					char* className = getClassNameByRef(clazz, index);
					//The execution of any one of the Java Virtual Machine instructions new,
					//getstatic, putstatic, or invokestatic that references the class or interface
					Class* destClass = loadClassByQualifiedName(threadId, className, (int) ( currInstPointer - code));
					char* fieldName = getNameFromNameAndType(clazz, index);
					//assume that uintptr_t is 8 bytes
					void* fieldReference = getFieldReference(destClass, fieldName);
					char* fieldDescriptor = getTypeFromNameAndType(clazz, index);
					setFieldValue(fieldReference, &operandStack[operandStackPointer--], fieldDescriptor);
				}
				break;
			case NEW: {
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					char* className = getClassNameByClassRef(clazz, index);
					//The execution of any one of the Java Virtual Machine instructions new,
					//getstatic, putstatic, or invokestatic that references the class or interface
					Class* destClass = loadClassByQualifiedName(threadId, className, (int) ( currInstPointer - code));
					if (destClass == NULL) {
						//throw ClassNotFoundException
						//for now just
						returnCode = -2;
						isRunning = 0;
						break;
					}
					JavaObject* objectPointer = allocObject(destClass);
					if (objectPointer == (void*)0xFFFFFFFFFFFFFFFF) {
						//out of memory
						returnCode = 2;
						isRunning = 0;
						break;
					}
					operandStack[++operandStackPointer].type = POINTER_JVM;
					operandStack[operandStackPointer].val.pointer = objectPointer;
				}
				break;
			case INVOKESPECIAL: {
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					char* className = getClassNameByRef(clazz, index);
					char* methodName = getNameFromNameAndType(clazz, index);
					char* methodDescriptor = getTypeFromNameAndType(clazz, index);
					//if (strcmp(className, "java/lang/Throwable") == 0)
					//	printf(" %s.%s%s\n", className, methodName, methodDescriptor);
					if (strcmp(className, "java/lang/Object") == 0) {
						//for now there is no Object class
						//but we need to pop one reference from the stack
						//cause the method is not poping it
						operandStackPointer--;
						break;
					}
					Class* destClass = findClassByQualifiedName(className);
					MethodInfo* method = findMethodNoAccessFlags(destClass, methodName, methodDescriptor);
					//this cheat is used to record the line of the new Exception() rather than the line
					//of the current function call
					threadId->callStack[threadId->callStackPointer].ip = (int) ( currInstPointer - code);
					if ((method->accessFlags & ACC_NATIVE) != 0) {
						int ret = callJniMethod(threadId, method, operandStack, &operandStackPointer);
						if (ret != 0) {
							isRunning = 0;
							returnCode = ret;
						}
						break;
					}
					int retCode = runMethod(threadId, method, (int) ( currInstPointer - code), operandStack, &operandStackPointer);
					if (retCode == EXCEPTION_RET_CODE) {
						Value val = operandStack[operandStackPointer--];
						operandStackPointer = 0;
						operandStack[operandStackPointer] = val;
						JavaObject* exception = val.val.pointer;
						u1* newIp = exceptionJump(currMethod, exception, (int) ( currInstPointer - code));
						if (newIp != NULL) {
							instPointer = newIp;
							break;
						} else {
							*parentStackPointer = *parentStackPointer + 1;
							parentStack[*parentStackPointer] = val;
							isRunning = 0;
							returnCode = EXCEPTION_RET_CODE;
							break;
						}
					}
					if (retCode != 0) {
						returnCode = retCode;
						isRunning = 0;
						break;
					}
				}
				break;
			case INVOKEINTERFACE:
			case INVOKEVIRTUAL: {
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					if (instruction == INVOKEINTERFACE) {
						//skip the useless 2 bytes of the invokeinterface instruction
						instPointer += 2;
					}
					//char* className = getClassNameByRef(clazz, index);
					char* methodName = getNameFromNameAndType(clazz, index);
					char* methodDescriptor = getTypeFromNameAndType(clazz, index);
					//if (strcmp(className, "java/lang/Throwable") == 0)
					//	printf(" %s.%s%s\n", className, methodName, methodDescriptor);
					size_t parameterCount = getMethodParameterCount(methodDescriptor);
					Value reference = operandStack[(size_t)operandStackPointer - parameterCount];
					//Class* refClass = getClass(reference.val.pointer);

					JavaObject* obj = reference.val.pointer;
					VTable* vtable = obj->vtable;

					if (obj->flags != 0) {
						printf("[%p] FIXME!!! calling virtual method [%s(%s)] of "
								"array reference, not implemented yet!!!\n", (void*)threadId,
								methodName, methodDescriptor);
					}
					MethodInfo* method = findVirtualMethod(vtable, methodName, methodDescriptor);
					if (method == NULL) {
						printf("[%p] Opps bad things happened flags=%d className=%s\n", (void*) threadId, obj->flags, obj->clazz->className);
					}
					if ((method->accessFlags & ACC_NATIVE) != 0) {
						int ret = callJniMethod(threadId, method, operandStack, &operandStackPointer);
						if (ret != 0) {
							isRunning = 0;
							returnCode = ret;
						}
						break;
					}
					int retCode = runMethod(threadId, method, (int) ( currInstPointer - code), operandStack, &operandStackPointer);
					if (retCode == EXCEPTION_RET_CODE) {
						EXCEPTION_HIT()
					}
					if (retCode != 0) {
						returnCode = retCode;
						isRunning = 0;
						break;
					}
					//somehow get the vtable count

				}
				break;
			case INVOKESTATIC: {
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					char* className = getClassNameByRef(clazz, index);
					char* methodName = getNameFromNameAndType(clazz, index);
					char* methodDescriptor = getTypeFromNameAndType(clazz, index);
					//if (strcmp(className, "java/lang/Throwable") == 0)
					//	printf(" %s.%s%s\n", className, methodName, methodDescriptor);
					Class* destClass = loadClassByQualifiedName(threadId, className, (int) ( currInstPointer - code));
					MethodInfo* m = findMethodNoAccessFlags(destClass, methodName, methodDescriptor);
					if ((m->accessFlags & ACC_NATIVE) != 0) {
						int ret = callJniMethod(threadId, m, operandStack, &operandStackPointer);
						if (ret != 0) {
							isRunning = 0;
							returnCode = ret;
						}
						break;
					}
					int retCode = runMethod(threadId, m, (int) ( currInstPointer - code), operandStack, &operandStackPointer);
					if (retCode == EXCEPTION_RET_CODE) {
						Value val = operandStack[operandStackPointer--];
						operandStackPointer = 0;
						operandStack[operandStackPointer] = val;
						JavaObject* exception = val.val.pointer;
						u1* newIp = exceptionJump(currMethod, exception, (int) (currInstPointer - code));
						if (newIp != NULL) {
							instPointer = newIp;
							break;
						} else {
							*parentStackPointer = *parentStackPointer + 1;
							parentStack[*parentStackPointer] = val;
							isRunning = 0;
							returnCode = EXCEPTION_RET_CODE;
							break;
						}
					}
					if (retCode != 0) {
						returnCode = retCode;
						isRunning = 0;
						break;
					}
				}
				break;
			case INSTANCEOF : {
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					char* className = getClassNameByClassRef(clazz, index);
					JavaObject* objReference = operandStack[operandStackPointer--].val.pointer;
					if (objReference == NULL) {
						operandStack[++operandStackPointer].type = INT_JVM;
						operandStack[operandStackPointer].val.intVal = 0;
						break;
					}
					Class* clazz = getClass(objReference);
					int ctr;
					int result = 0;
					for (ctr = (int) clazz->classChainCount - 1; ctr >= 0; ctr--) {
						Class* currClass = clazz->classChain[ctr];
						if (strcmp(className, currClass->className) == 0) {
							result = 1;
							break;
						}
					}
					operandStack[++operandStackPointer].type = INT_JVM;
					operandStack[operandStackPointer].val.intVal = result;

				} break;
			case PUTFIELD: {
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					Value val = operandStack[operandStackPointer--];
					JavaObject* objReference = operandStack[operandStackPointer--].val.pointer;

					Class* refClazz = getClass(objReference);

					char* fieldName = getNameFromNameAndType(clazz, index);
					char* fieldDescriptor = getTypeFromNameAndType(clazz, index);
					size_t fieldOffset = getFieldOffset(refClazz, fieldName);
					setFieldValue(((u1*)objReference) + fieldOffset, &val, fieldDescriptor);
				}
				break;
			case GETFIELD: {
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					JavaObject* objReference = operandStack[operandStackPointer--].val.pointer;
					char* fieldName = getNameFromNameAndType(clazz, index);
					char* fieldDescriptor = getTypeFromNameAndType(clazz, index);
					//printf("GETFIELD fieldName = %s offset=%d\n", fieldName, (int)(currInstPointer - code));

					Class* refClazz = getClass(objReference);

					size_t fieldOffset = getFieldOffset(refClazz, fieldName);
					Value val;
					getRefValue( &val, ((u1*) objReference) + fieldOffset, fieldDescriptor);
					operandStack[++operandStackPointer].val = val.val;
					operandStack[operandStackPointer].type = val.type;
				}
				break;
			case NEWARRAY: {
					u1 type = *(instPointer++);
					int32_t size = operandStack[operandStackPointer--].val.intVal;

					JavaObject* objectPointer = allocArray(threadId, type, (u4) size, NULL);
					if (objectPointer == (void*)0xFFFFFFFFFFFFFFFF) {
						//out of memory
						returnCode = 2;
						isRunning = 0;
						break;
					}
					operandStack[++operandStackPointer].type = POINTER_JVM;
					operandStack[operandStackPointer].val.pointer = objectPointer;
				}
				break;
			case ANEWARRAY: {
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					char* className = getClassNameByClassRef(clazz, index);
					Class* elementClass = loadClassByQualifiedName(threadId, className, (int) ( currInstPointer - code));
					int32_t size = operandStack[operandStackPointer--].val.intVal;
					JavaObject* objectPointer = allocArray(threadId, ARR_TYPE_REF, (u4) size, elementClass);
					if (objectPointer == (void*)0xFFFFFFFFFFFFFFFF) {
						//out of memory
						returnCode = 2;
						isRunning = 0;
						break;
					}
					operandStack[++operandStackPointer].type = POINTER_JVM;
					operandStack[operandStackPointer].val.pointer = objectPointer;
				}
				break;
			case ARRAYLENGTH: {
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					operandStack[++operandStackPointer].type = INT_JVM;
					operandStack[operandStackPointer].val.intVal = (int32_t) ptr->extra.length;

				}
				break;
			case IALOAD: {
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					void* ptr = operandStack[operandStackPointer--].val.pointer;
					int32_t value;
					int code = loadIntFromArray(ptr, index, &value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
					operandStack[++operandStackPointer].type = INT_JVM;
					operandStack[operandStackPointer].val.intVal = value;
				}
				break;
			case IASTORE: {
					int32_t value = operandStack[operandStackPointer--].val.intVal;
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					int code = storeIntIntoArray(ptr, index, value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
				}
				break;
			case SALOAD: {
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					void* ptr = operandStack[operandStackPointer--].val.pointer;
					int16_t value;
					int code = loadShortFromArray(ptr, index, &value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
					operandStack[++operandStackPointer].type = INT_JVM;
					operandStack[operandStackPointer].val.intVal = (int32_t) value;
				}
				break;
			case SASTORE: {
					int16_t value = operandStack[operandStackPointer--].val.shortVal;
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					int code = storeShortIntoArray(ptr, index, value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
				}
				break;
			case CALOAD: {
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					void* ptr = operandStack[operandStackPointer--].val.pointer;
					uint16_t value;
					int code = loadCharFromArray(ptr, index, &value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
					operandStack[++operandStackPointer].type = CHAR_JVM;
					operandStack[operandStackPointer].val.charVal = value;
				}
				break;
			case CASTORE: {
					uint16_t value = operandStack[operandStackPointer--].val.charVal;
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					int code = storeCharIntoArray(ptr, index, value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
				}
				break;
			case BALOAD: {
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					void* ptr = operandStack[operandStackPointer--].val.pointer;
					int8_t value;
					int code = loadByteFromArray(ptr, index, &value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
					operandStack[++operandStackPointer].type = INT_JVM;
					operandStack[operandStackPointer].val.intVal = (int32_t) value;
				}
				break;
			case BASTORE: {
					int8_t value = operandStack[operandStackPointer--].val.byteVal;
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					void* ptr = operandStack[operandStackPointer--].val.pointer;
					int code = storeByteIntoArray(ptr, index, value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
				}
				break;
			case FALOAD: {
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					float value;
					int code = loadFloatFromArray(ptr, index, &value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
					operandStack[++operandStackPointer].type = FLOAT_JVM;
					operandStack[operandStackPointer].val.floatVal = value;
				}
				break;
			case FASTORE: {
					float value = operandStack[operandStackPointer--].val.floatVal;
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					int code = storeFloatIntoArray(ptr, index, value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
				}
				break;
			case DALOAD: {
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					double value;
					int code = loadDoubleFromArray(ptr, index, &value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
					operandStack[++operandStackPointer].type = DOUBLE_JVM;
					operandStack[operandStackPointer].val.doubleVal = value;
				}
				break;
			case DASTORE: {
					double value = operandStack[operandStackPointer--].val.doubleVal;
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					int code = storeDoubleIntoArray(ptr, index, value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
				}
				break;
			case LALOAD: {
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					int64_t value;
					int code = loadLongFromArray(ptr, index, &value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
					operandStack[++operandStackPointer].type = LONG_JVM;
					operandStack[operandStackPointer].val.longVal = value;
				}
				break;
			case LASTORE: {
					int64_t value = operandStack[operandStackPointer--].val.longVal;
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					int code = storeLongIntoArray(ptr, index, value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
				}
				break;
			case AALOAD: {
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					uintptr_t value;
					int code = loadRefFromArray(ptr, index, &value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
					operandStack[++operandStackPointer].type = POINTER_JVM;
					operandStack[operandStackPointer].val.pointer = (void*) value;
				}
				break;
			case AASTORE: {
					uintptr_t value = (uintptr_t) operandStack[operandStackPointer--].val.pointer;
					int32_t index = operandStack[operandStackPointer--].val.intVal;
					JavaObject* ptr = operandStack[operandStackPointer--].val.pointer;
					int code = storeRefIntoArray(ptr, index, value);
					if (code != 0) {
						//Throw array index outof bounds exception
					}
				}
				break;
			case RETURN:
				isRunning = 0;
				PRINTF("RETURN_HIT\n");
				break;
			case LRETURN:
			case DRETURN:
			case FRETURN:
			case ARETURN:
			case IRETURN: {
					Value val = operandStack[operandStackPointer--];
					*parentStackPointer = *parentStackPointer + 1;
					parentStack[*parentStackPointer] = val;
				}
				isRunning = 0;
				PRINTF("RETURN_HIT\n");
				break;
			case ATHROW: {
					Value val = operandStack[operandStackPointer--];
					operandStackPointer = 0;
					operandStack[operandStackPointer] = val;
					JavaObject* exception = val.val.pointer;
					u1* newIp = exceptionJump(currMethod, exception, (int) ( currInstPointer - code));
					if (newIp != NULL) {
						instPointer = newIp;
					} else {
						*parentStackPointer = *parentStackPointer + 1;
						parentStack[*parentStackPointer] = val;
						isRunning = 0;
						returnCode = EXCEPTION_RET_CODE;
						break;
					}
				} break;

			case CHECKCAST: {
					GET_INDEX_BY_TWO_BYTES(instPointer, index)
					JavaObject* ref = operandStack[operandStackPointer].val.pointer;
					if (ref == NULL) {
						break;
					}
					Class* currClass = currMethod->clazz;
					size_t nameIndex = currClass->constantPoolArray[index].nameIndex;
					char* descriptor = getUtfStringAtIndex(currClass, nameIndex - 1);
					Class* refClass = NULL;
					char message[256];
					if (descriptor[0] != '[') {
						refClass = findClassByQualifiedName(descriptor);
						if (refClass == NULL) {
							//throw class not found exception
						}
					}
					if ((ref->flags == 0 && descriptor[0] == '[') ||
							(ref->flags != 0 && descriptor[0] != '[')) {
						//throw class cast exception
						memset(message, 0 , 256);
						strcat(message, " imcompatible with");
						strcat(message, descriptor);
						THROW_EXCEPTION("java/lang/ClassCastException", message)
					}
					if (ref->flags == 0 && isAssignableFrom(ref->clazz, refClass)) {
						break;
					}
					if (ref->flags != 0 ) {
						int ret = checkArrays(ref, descriptor);
						if (ret == 1) {
							break;
						}
						if (ret == -1) {
							//throw class not found exception
						}
					}

					//throw class cast exception

					memset(message, 0 , 256);
					strcat(message, " imcompatible with");
					strcat(message, descriptor);
					THROW_EXCEPTION("java/lang/ClassCastException", message)


				} break;
			case MONITOREXIT:{
					JavaObject* ref = operandStack[operandStackPointer--].val.pointer;
					if (ref == NULL) {
						//throw NullPointerException
						printf("NullPointerException in MONITOREXIT");
					}
					pthread_mutex_unlock (&ref->mutex);
				}
				break;
			case MONITORENTER: {
					JavaObject* ref = operandStack[operandStackPointer--].val.pointer;
					if (ref == NULL) {
						//throw NullPointerException
						printf("NullPointerException in MONITORENTER");
					}
					pthread_mutex_lock (&ref->mutex);
				}
				break;
			case POP:
				operandStackPointer--;
				break;
			case POP2: {
					Value val = operandStack[operandStackPointer];
					if (val.type == LONG_JVM || val.type == DOUBLE_JVM) {
						operandStackPointer--;
					} else {
						operandStackPointer -=2;
					}
				}
				break;
			default:
				printf("UNKNOWN INSTRCTION %x !!!!!!!!!!!!!!!!!!!\n", instruction);
				isRunning = 0;
				returnCode = -123456789;
				break;
		}
	}
	
	if ((currMethod->accessFlags & ACC_SYNCHRONIZED) != 0) {
		pthread_mutex_unlock(&thisReference->mutex);
	}

	popStackFrame(threadId);
	return returnCode;
}

int pushStackFrame(Thread* threadId, int returnAddress, 	u2 maxLocals, u2 maxStack, MethodInfo* method) {
	threadId->callStackPointer++;
	PRINTF("pushStackFrame(%d)\n", threadId->callStackPointer);
	if (threadId->callStackPointer >= MAX_CALL_STACK) {
		printf("StackOverFlow error %d !\n", threadId->callStackPointer);
		//TODO add some cleanup code
		cleanCallStack(threadId);
		return -1;
	}
	StackFrame* frame = &threadId->callStack[threadId->callStackPointer];
	frame->returnAddress = returnAddress;
	frame->maxLocals = maxLocals;
	frame->maxStack = maxStack;
	frame->clazz = method->clazz;
	frame->method = method;
	frame->localVariables = malloc(sizeof(Value) * maxLocals);
	//we put one more stack position for an exception to be returned
	frame->operandStack = malloc(sizeof(Value) * (size_t)  (maxStack + 1));
	return 0;
}

void popStackFrame(Thread* threadId) {
	PRINTF("popStackFrame(%d)\n", threadId->callStackPointer);
	StackFrame* frame = &threadId->callStack[threadId->callStackPointer];
	if (frame->localVariables != NULL) {
		free(frame->localVariables);
		frame->localVariables = NULL;
	}
	if (frame->operandStack != NULL) {
		free(frame->operandStack);
		frame->operandStack = NULL;
	}
	threadId->callStackPointer--;
}

void cleanCallStack(Thread* threadId) {
	int i;
	if (threadId->callStackPointer >= MAX_CALL_STACK) {
		for (i = 0; i < MAX_CALL_STACK; i++) {
			StackFrame frame = threadId->callStack[threadId->callStackPointer];
			if (frame.localVariables != NULL) {
				free(frame.localVariables);
			}
			if (frame.operandStack != NULL) {
				free(frame.operandStack);
			}
		}
	}
}

void initLocals(Value* locals, Value* parentStack, int* parentStackPointer, size_t paramCount, int startIndex, u1* localIndexes) {
	int i;
	int start = (int)paramCount + startIndex - 1;
	for (i = start; i >= startIndex; i--) {
		locals[localIndexes[i]] = parentStack[(*parentStackPointer)--];
	}
}



int runClinitMethod(Thread* threadId, Class* clazz, int returnAddress) {
	MethodInfo* method = findMethod(clazz, "<clinit>", "()V", ACC_STATIC);
	if (method == NULL) {
		return 0;
	}

	return runMethod(threadId, method , returnAddress, NULL, NULL);

}

//by fieldRef index
char* getClassNameByRef(Class* clazz, size_t index) {
	ConstantPoolInfo * info = &clazz->constantPoolArray[index];//field ref
	size_t classNameIndex = clazz->constantPoolArray[info->classIndex - 1].nameIndex;//class
	return getUtfStringAtIndex(clazz, classNameIndex - 1);
}

char* getClassNameByClassRef(Class* clazz, size_t index) {
	ConstantPoolInfo * info = &clazz->constantPoolArray[index];//field ref
	return getUtfStringAtIndex(clazz, (size_t) info->nameIndex - 1);
}


void* getFieldReference(Class* clazz, char* fieldName) {
	FieldInfo* info = getField(clazz, fieldName);
	if (info == NULL) {
		return 0;
	}
	return (void*) info->staticValuePointer;
}

size_t getFieldOffset(Class* clazz, char* fieldName) {
	int i, ctr;

	//In reverse order so that the current class fields apear first
	for (ctr = (int) clazz->classChainCount - 1; ctr >= 0 ; ctr--) {
		Class* superClazz = clazz->classChain[ctr];
		for (i = 0; i < superClazz->fieldCount; i++) {
			FieldInfo* info = &superClazz->fieldArray[i];
			char* name = getUtfStringAtIndex(superClazz, (size_t) info->nameIndex - 1);
			if (strcmp(name, fieldName) == 0) {
				return info->offset;
			}
		}
	}
	return (size_t)-1;
}

void getWideConstant(ConstantPoolInfo* entry, Value* value) {
	u1 tag = entry->tag;
	int64_t ll;
	uint64_t bytes;
	switch (tag) {
		case CONSTANT_Long: {
			bytes = ((uint64_t)entry->highBytes << 32) + entry->lowBytes;
			ll = *((int64_t*)&bytes);
			value->val.longVal = ll;
			value->type = LONG_JVM;
			}
			break;
		case CONSTANT_Double: {
				bytes = ((uint64_t)entry->highBytes << 32) + entry->lowBytes;
				value->val.doubleVal = *((double*)&bytes);
				value->type = DOUBLE_JVM;
			}
			break;
	}
}

u1* exceptionJump(MethodInfo* method, JavaObject* exception, int ip) {
	int i, j;
	Class* clazz = method->clazz;
	Class* exceptionClass = getClass(exception);
	for (i = 0; i < method->exceptionTableCount; i++) {
		ExceptionTable exc = method->exceptionTable[i];
		if (!(exc.startPc <= ip && ip < exc.endPc)) {
			continue;
		}
		if(exc.catchType == 0) {
			return method->code + exc.handlerPc;
		}
		size_t index = clazz->constantPoolArray[exc.catchType - 1].nameIndex;
		char* catchClassName = getUtfStringAtIndex(clazz, index - 1);
		for (j = (int) exceptionClass->classChainCount - 1; j >= 0; j--) {

			//search for the subclasses of the exception object
			Class* currClass = exceptionClass->classChain[j];
			if (strcmp(currClass->className, catchClassName) == 0) {
				return method->code + exc.handlerPc;
			}
		}
	}

	return NULL;
}

int checkArrays(JavaObject* ref, char* descriptor) {
	int numberOfDimensions1 = arrayNumberOfDimensions(ref);
	int numberOfDimensions2 = 0;
	char* str = descriptor;
	while (*str == '[') {
		numberOfDimensions2++;
		str++;
	}
	if (numberOfDimensions1 != numberOfDimensions2) {
		return 0;
	}

	int arrayType1 = arrayType(ref);
	if (arrayType1 != ARR_TYPE_REF) {
		if (arrayType1 != letterToCode(str)) {
			return 0;
		} else {
			return 1;
		}
	}

	char newName[512];
	if (*str != 'L') {
		return 0;
	}

	str++;
	memset(newName, 0, 512);
	strcat(newName, str);
	//we remove the ;
	newName[strlen(newName) - 1] = '\0';
	Class* clazz = findClassByQualifiedName(newName);
	if (clazz == NULL) {
		return -1;
	}

	return isAssignableFrom(ref->type.elementClazz, clazz);


}

JavaObject* createException(Thread* threadId, char* className, char* message, int returnAddress) {
	Class* clazz = loadClassByQualifiedName(threadId, className, 0);
	JavaObject* obj = allocObject(clazz);
	MethodInfo* initMethod = findMethodNoAccessFlags(clazz, "<init>", "(Ljava/lang/String;)V");
	if (initMethod == NULL) {
		printf("Ops the exception %s does not have init method (String message)\n", className);
	}
	JavaObject* stringObj = charArrayToString(threadId, message);
	int parentStackPointer = 1;
	Value val[2];
	val[0].val.pointer = obj;
	val[0].type = POINTER_JVM;
	val[1].val.pointer = stringObj;
	val[1].type = POINTER_JVM;
	runMethod(threadId, initMethod, returnAddress, val, &parentStackPointer);
	return obj;
}

void printStackTrace(Thread* threadId, JavaObject* obj, int returnAddress) {
	//get the thrad group
	MethodInfo* getThreadGroupMethod = findMethodNoAccessFlags(getClass(threadId->javaThread),
			"getThreadGroup", "()java/lang/ThreadGroup;");

	Value val;
	val.val.pointer = threadId->javaThread;
	val.type = POINTER_JVM;
	int parentStackPointer = 0;
	runMethod(threadId, getThreadGroupMethod, returnAddress, &val, &parentStackPointer);

	//call the uncaughtException method
	JavaObject* threadGroup = val.val.pointer;
	MethodInfo* uncaughtExceptionMethod = findMethodNoAccessFlags(getClass(threadGroup),
			"uncaughtException", "(Ljava/lang/Thread;Ljava/lang/Throwable;)V");
	if (uncaughtExceptionMethod == NULL) {
		printf("Ops there is no uncaughtException method!!!!\n");
	}
	parentStackPointer = 2;
	Value args[3];
	args[0].val.pointer = threadGroup;
	args[0].type = POINTER_JVM;
	args[1].val.pointer = threadId->javaThread;
	args[1].type = POINTER_JVM;
	args[2].val.pointer = obj;
	args[2].type = POINTER_JVM;
	runMethod(threadId, uncaughtExceptionMethod, returnAddress, args, &parentStackPointer);
}

JavaObject* createMainJavaThread() {

	//in this method no clinit methods must be run until main java thread object is initialized
	Thread thread;
	thread.callStackPointer = -1;
	Class* threadGroupClass = loadClassByQualifiedName(&thread, "java/lang/ThreadGroup", 0);

	//system thread group
	JavaObject* systemThreadGroup = allocObject(threadGroupClass);
	Value parentStack[1];
	parentStack[0].type = POINTER_JVM;
	parentStack[0].val.pointer = systemThreadGroup;
	int parentStackPointer = 0;
	MethodInfo* initMethod = findMethodNoAccessFlags(threadGroupClass, "<init>", "()V");
	runMethod(&thread, initMethod, 0, parentStack, &parentStackPointer);

	//main thread group
	JavaObject* mainThreadGroup = allocObject(threadGroupClass);
	Value parentStack1[3];
	parentStack1[0].type = POINTER_JVM;
	parentStack1[0].val.pointer = mainThreadGroup;
	parentStack1[1].type = POINTER_JVM;
	parentStack1[1].val.pointer = systemThreadGroup;
	parentStack1[2].type = POINTER_JVM;
	parentStack1[2].val.pointer = charArrayToString(&thread, "main");
	parentStackPointer = 2;
	MethodInfo* initMethod2 = findMethodNoAccessFlags(threadGroupClass, "<init>",
			"(Ljava/lang/ThreadGroup;Ljava/lang/String;)V");
	runMethod(&thread, initMethod2, 0, parentStack1, &parentStackPointer);

	//creating the main thread object
	Class* threadClass = loadClassByQualifiedName(&thread, "java/lang/Thread", 0);
	heapThreadClass = threadClass;
	JavaObject* mainJavaThread = allocObject(threadClass);

	//setting some fields of the main java thread object
	SET_OBJECT_FIELD(mainJavaThread, mainThreadGroup, "group", "Ljava/lang/ThreadGroup;")
	SET_INT_FIELD(mainJavaThread, 0, "daemon" , "Z")
	SET_INT_FIELD(mainJavaThread, 5, "priority" , "I")
	SET_LONG_FIELD(mainJavaThread, 1L, "tid" )

	//update the sequenceNumber
	Value val;
	val.val.longVal = 2L;
	val.type = LONG_JVM;
	FieldInfo* field = findField(threadClass, "threadSeqNumber", "J");
	setFieldValue(field->staticValuePointer, &val, "J");

	//setting the name of the main thread
	char* threadName = "main";
	JavaObject* charArray = allocArray(&thread, ARR_TYPE_BYTE, (u4) strlen(threadName), NULL);
	setCharsInArray(charArray, threadName);
	SET_OBJECT_FIELD(mainJavaThread, charArray, "name", "[C")

	//FIXME set also the contextClassLoader field

	return mainJavaThread;
}

void initSystemClassLoader(Thread* thread) {
	Class* classLoader = loadClassByQualifiedName(thread, "java/lang/ClassLoader", 0);
	Value val;
	int parentPointer = -1;
	MethodInfo* method = findMethodNoAccessFlags(classLoader, "getSystemClassLoader", "()Ljava/lang/ClassLoader;");
	runMethod(thread, method, 0, &val, &parentPointer);
	printf("haha");
}

void initSystemProperties(Thread* thread) {
	Class* propertiesClass = loadClassByQualifiedName(thread, "com/geecodemonkeys/fijvm/props/SystemProps", 0);
	MethodInfo* method = findMethodNoAccessFlags(propertiesClass, "getSystemProperties", "()Ljava/util/Properties;");
	Value val;
	int parentPointer = -1;
	runMethod(thread, method, 0, &val, &parentPointer);

	Class* systemClass = loadClassByQualifiedName(thread, "java/lang/System", 0);
	FieldInfo* field = findField(systemClass, "props", "Ljava/util/Properties;");

	setFieldValue(field->staticValuePointer, &val, "Ljava/util/Properties;");

}

void fakePrintln(const char * methodDescriptor, Value* val) {
//(I)V
	char c = methodDescriptor[1];
	switch (c) {
		case 'I': {
			printf("%d\n", val->val.intVal);
			}
			break;
		case 'B':
			printf("%d\n", val->val.byteVal);
			break;
		case 'F':
			printf("%.2f\n", val->val.floatVal);
			break;
		case 'J':
			//printf("%" PRId64 "\n", val->val.longVal);
			break;
		case 'S':
			printf("%d\n", (int) val->val.shortVal);
			break;
		case 'C':
			printf("%c\n", (int) val->val.charVal);
			break;
		case 'D':
			printf("%.2f\n", val->val.doubleVal);
			break;
	}
}

