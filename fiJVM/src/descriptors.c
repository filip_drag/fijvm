#include "descriptors.h"
#include "util.h"

size_t initLocalIndexes(u1* localIndexes, char* descriptor, size_t startIndex) {
	char* ptr = &descriptor[1];
	size_t currIndex = startIndex;
	size_t realIndex = startIndex;
	for (; *ptr != 0;) {
		if (*ptr == ')') {
			break;
		}

		localIndexes[realIndex] = (u1) currIndex;
		if (*ptr == 'J' || *ptr == 'D') {
			currIndex += 2;
		} else {
			currIndex++;
		}
		ptr = parseCompoundType(ptr);

		realIndex++;
	}
	return realIndex;
}

size_t getMethodParameterCount(char* descriptor) {
	char* ptr = &descriptor[1];
	size_t realIndex = 0;
	for (; *ptr != 0;) {
		if (*ptr == ')') {
			break;
		}

		ptr = parseCompoundType(ptr);
		realIndex++;
	}
	return realIndex;
}

void methodDescriptorNoRetType(char* descriptor, char* result, size_t resultSize) {
	char* ptr = descriptor;
	char* resPtr = result;
	memset(result, 0 , resultSize);
	while (*ptr != ')') {
		*resPtr = *ptr;
		resPtr++;
		ptr++;
	}
	*resPtr = ')';
}

char* getReturnType(char* descriptor) {
	char* ptr = descriptor;
	while (*ptr != ')') {
		ptr++;
	}
	ptr++;
	return ptr;
}

char* parseCompoundType(char* str) {
	char c = *str;
	char* ptr = str;
	if (c == 'B' || c =='C' || c == 'D' || c == 'F' || c == 'I' || c == 'J' || c == 'S' || c == 'Z') {
		ptr = parseBaseType(ptr);
	} else if (c == 'L') {
		ptr = parseObjectType(ptr);
	} else if (c == '[') {
		ptr = parseArrayType(ptr);
	}
	return ptr;
}
char* parseBaseType(char* str) {
	str++;
	return str;
}

char* parseObjectType(char* ptr) {
	ptr++;
	while (*ptr != 0 && *ptr != ';') {
		ptr++;
	}
	if (*ptr == ';') {
		ptr++;
	}
	return ptr;
}

char* parseArrayType(char* ptr) {
	ptr++;
	return parseCompoundType(ptr);
}

void getMethodKey(MethodInfo* method, char* resultKey, size_t resultKeySize) {
	char* name = getUtfStringAtIndex(method->clazz, (size_t)method->nameIndex - 1);
	char* descriptor = getUtfStringAtIndex(method->clazz, (size_t)method->descriptorIndex - 1);
	getMethodKeyStr(name, descriptor, resultKey, resultKeySize);
}

void getMethodKeyStr(char* name, char* descriptor, char* resultKey, size_t resultKeySize) {
	char cutDescriptor[1024];
	methodDescriptorNoRetType(descriptor, cutDescriptor, 1024);
	memset(resultKey, 0, resultKeySize);
	strcat(resultKey, name);
	strcat(resultKey, cutDescriptor);
}


