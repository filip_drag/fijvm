/*
 * reflection.c
 *
 *  Created on: Sep 14, 2015
 *      Author: fi
 */
#include <string.h>
#include "reflection.h"
#include "heap.h"
#include "class-def.h"
#include "access-flags.h"
#include "java-object-consts.h"
#include "classes.h"
#include "array.h"
#include "jni-helper.h"
#include "descriptors.h"

JavaObject* descriptorToTypeList(Thread* thread, char* descriptor);
void initConstructor(Thread* thread, Class* constructorClass, JavaObject* constructor, MethodInfo* method, int slot);

JavaObject* getClassConstructors(Thread* thread, Class* clazz, int publicOnly) {
	int count = 0;
	int i, ctr = 0;
	for (i = 0; i < clazz->methodCount; i++) {
		MethodInfo* method = &clazz->methodArray[i];
		if (strcmp(method->methodName, "<init>") != 0) {
			continue;
		}
		if (publicOnly == 1 && (method->accessFlags & ACC_PUBLIC) == 0) {
			continue;
		}
		count++;
	}

	Class* constructorClass = loadClassByQualifiedName(thread, "java/lang/reflect/Constructor", 0);
	JavaObject* array = allocArray(thread, ARR_TYPE_REF, (u4) count, constructorClass);

	for (i = 0, ctr = 0; i < clazz->methodCount; i++) {
		MethodInfo* method = &clazz->methodArray[i];
		if (strcmp(method->methodName, "<init>") != 0) {
			continue;
		}
		if (publicOnly == 1 && (method->accessFlags & ACC_PUBLIC) == 0) {
			continue;
		}
		JavaObject* constructor = allocObject(constructorClass);
		storeRefIntoArray(array, ctr, (uintptr_t) constructor);
		ctr++;

		//add init constructor
		initConstructor(thread,constructorClass, constructor, method, i);
	}
	return array;
}

void initConstructor(Thread* thread, Class* constructorClass, JavaObject* constructor, MethodInfo* method, int slot) {

	//set the slot field which is actually the index in the method table,
	//it is needed cause constructor objects are copied and this is the link to the native object
	FieldInfo* slotField = findField(constructorClass, "slot", "I");
	Value val;
	val.val.intVal =  slot;
	setField(&val, constructor, slotField);

	//set the clazz field
	FieldInfo* clazzField = findField(constructorClass, "clazz", "Ljava/lang/Class;");
	val.val.pointer = method->clazz->java_Lang_Class;
	setField(&val, constructor, clazzField);

	//sett the access flags
	FieldInfo* flagsField = findField(constructorClass, "modifiers", "I");
	val.val.intVal = method->accessFlags;
	setField(&val, constructor, flagsField);

	JavaObject* parameterTypes = descriptorToTypeList(thread, method->descriptor);

	FieldInfo* paramListField = findField(constructorClass, "parameterTypes", "[Ljava/lang/Class;");
	val.val.pointer = parameterTypes;
	setField(&val, constructor, paramListField);

}

JavaObject* descriptorToTypeList(Thread* thread, char* descriptor) {
	char temp[2048];
	Class* javaLangClass = findClassByQualifiedName("java/lang/Class");
	int size = (int) getMethodParameterCount(descriptor);
	JavaObject* typeList = allocArray(thread, ARR_TYPE_REF, (u4) size, javaLangClass);
	int ctr = 0;
	char* ptr = descriptor;
	ptr++;
	while (*ptr != ')') {
		char c = *ptr;
		JavaObject* currClass;
		if (c == 'B' || c =='C' || c == 'D' || c == 'F' || c == 'I' || c == 'J' || c == 'S' || c == 'Z') {
			temp[0] = c;
			temp[1] = '\0';
			currClass = createIfNotExistsPrimiteClass(thread, temp);
		} else if (c == 'L') {
			int i = 0;
			while (*(++ptr) != ';') {
				temp[i++] = *ptr;
			}
			temp[++i] = '\0';
			Class* lClass = loadClassByQualifiedName(thread, temp, 0);
			currClass = lClass->java_Lang_Class;
		} else if (c == '[') {
			int i = 0;
			while (*ptr == '[') {
				temp[i++] = *ptr;
				ptr++;
			}
			if (*ptr == 'L') {
				while(*ptr != ';') {
					temp[i++] = *ptr;
					ptr++;
				}
			}
			temp[i++] = *ptr;
			temp[i] = '\0';
			currClass = createIfNotExistsPrimiteClass(thread, temp);

		}
		storeRefIntoArray(typeList, ctr, (uintptr_t) currClass);
	}
	return typeList;
}


