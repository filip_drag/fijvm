#include <inttypes.h>
#include "class-def.h"
#include "array.h"
#include "jni-helper.h"
#include "java-object-consts.h"

#define LOAD_FROM_PTR(ptr, index, value, type) 		int res = checkBounds(ptr, index);\
									if (res != 0) {\
										return res;\
									}\
									u1* ptrNoHeader = ((u1*)ptr) + ARRAY_HEADER_SIZE;\
									u4* u4ptrNoHeader = (u4*)ptrNoHeader;\
									*value = *((type*)(u4ptrNoHeader + index));\

#define STORE_TO_PTR(ptr, index, value, type)  		int res = checkBounds(ptr, index);\
									if (res != 0) {\
										return res;\
									}\
									u1* ptrNoHeader = ((u1*)ptr) + ARRAY_HEADER_SIZE;\
									u4* u4ptrNoHeader = (u4*)ptrNoHeader;\
									*(u4ptrNoHeader + index) = *((u4*)&value);\

int checkBounds(void* ptr, int32_t index) {
	u4 length = getArrayLength(ptr);
	if (index < 0 || index >= (int32_t) length) {
		return -1;
	}
	return 0;
}

int storeIntIntoArray(void* ptr, int32_t index, int32_t value) {
	STORE_TO_PTR(ptr, index, value, int32_t)
	return 0;
}

int loadIntFromArray(void* ptr, int32_t index, int32_t* value) {
	LOAD_FROM_PTR(ptr, index, value, int32_t)
	return 0;
}

int storeFloatIntoArray(void* ptr, int32_t index, float value) {
	STORE_TO_PTR(ptr, index, value, float)
	return 0;
}

int loadFloatFromArray(void* ptr, int32_t index, float* value) {
	LOAD_FROM_PTR(ptr, index, value, float)
	return 0;
}

int storeDoubleIntoArray(void* ptr, int32_t index, double value) {
	int res = checkBounds(ptr, index);
	if (res != 0) {
		return res;
	}
	u1* ptrNoHeader = ((u1*)ptr) + ARRAY_HEADER_SIZE;
	double* u4ptrNoHeader = (double*)ptrNoHeader;
	*(u4ptrNoHeader + index) = *((double*)&value);
	return 0;
}

int loadDoubleFromArray(void* ptr, int32_t index, double* value) {
	int res = checkBounds(ptr, index);
	if (res != 0) {
		return res;
	}
	u1* ptrNoHeader = ((u1*)ptr) + ARRAY_HEADER_SIZE;
	double* u4ptrNoHeader = (double*)ptrNoHeader;
	*value = *((double*)(u4ptrNoHeader + index));
	return 0;
}

int storeLongIntoArray(void* ptr, int32_t index, int64_t value) {
	int res = checkBounds(ptr, index);
	if (res != 0) {
		return res;
	}
	u1* ptrNoHeader = ((u1*)ptr) + ARRAY_HEADER_SIZE;
	int64_t* u4ptrNoHeader = (int64_t*)ptrNoHeader;
	*(u4ptrNoHeader + index) = *((int64_t*)&value);
	return 0;
}

int loadLongFromArray(void* ptr, int32_t index, int64_t* value) {
	int res = checkBounds(ptr, index);
	if (res != 0) {
		return res;
	}
	u1* ptrNoHeader = ((u1*)ptr) + ARRAY_HEADER_SIZE;
	int64_t* u4ptrNoHeader = (int64_t*)ptrNoHeader;
	*value = *((int64_t*)(u4ptrNoHeader + index));
	return 0;
}
int storeRefIntoArray(void* ptr, int32_t index, uintptr_t value) {
	int res = checkBounds(ptr, index);
	if (res != 0) {
		return res;
	}
	u1* ptrNoHeader = ((u1*)ptr) + ARRAY_HEADER_SIZE;
	uintptr_t* u4ptrNoHeader = (uintptr_t*)ptrNoHeader;
	*(u4ptrNoHeader + index) = *((uintptr_t*)&value);
	return 0;
}

int loadRefFromArray(void* ptr, int32_t index, uintptr_t* value) {
	int res = checkBounds(ptr, index);
	if (res != 0) {
		return res;
	}
	u1* ptrNoHeader = ((u1*)ptr) + ARRAY_HEADER_SIZE;
	uintptr_t* u4ptrNoHeader = (uintptr_t*)ptrNoHeader;
	*value = *((uintptr_t*)(u4ptrNoHeader + index));
	return 0;
}
int storeShortIntoArray(void* ptr, int32_t index, int16_t value) {
	STORE_TO_PTR(ptr, index, value, int16_t)
	return 0;
}

int loadShortFromArray(void* ptr, int32_t index, int16_t* value) {
	LOAD_FROM_PTR(ptr, index, value, int16_t)
	return 0;
}
int storeByteIntoArray(void* ptr, int32_t index, int8_t value) {
	STORE_TO_PTR(ptr, index, value, int8_t)
	return 0;
}

int loadByteFromArray(void* ptr, int32_t index, int8_t* value) {
	LOAD_FROM_PTR(ptr, index, value, int8_t)
	return 0;
}
int storeCharIntoArray(void* ptr, int32_t index, uint16_t value) {
	STORE_TO_PTR(ptr, index, value, uint16_t)
	return 0;
}

int loadCharFromArray(void* ptr, int32_t index, uint16_t* value) {
	int res = checkBounds(ptr, index);
	if (res != 0) {
		return res;
	}
	u1* ptrNoHeader = ((u1*)ptr) + ARRAY_HEADER_SIZE;
	uint32_t* u4ptrNoHeader = (uint32_t*)ptrNoHeader;
	*value = (uint16_t)*(u4ptrNoHeader + index);
	return 0;
}

void setCharsInArray(JavaObject* obj, char* str) {
	int i;
	for (i = 0; i < strlen(str); i++) {
		storeCharIntoArray(obj, i, (uint16_t) str[i]);
	}

}

// type = 3 means array of references
u4 arraySize(u1 type) {
	u4 elementSize = 4;
	switch(type) {
		case ARR_TYPE_REF:
			elementSize = REF_SIZE;
			break;
		case 4:
		case 5:
		case 6:
		case 8:
		case 9:
		case 10:
			elementSize = INT_SIZE;
			break;
		case 7:
		case 11:
			elementSize = DOUBLE_SIZE;
			break;
	}
	return elementSize;
}

char* arrayType2Letter(int primitiveType) {
	switch(primitiveType) {
		case ARR_TYPE_BOOL  : return "Z";
		case ARR_TYPE_CHAR :  return "C";
		case ARR_TYPE_FLOAT : return "F";
		case ARR_TYPE_DOUBLE : return "D";
		case ARR_TYPE_BYTE  :  return "B";
		case ARR_TYPE_SHORT :  return "S";
		case ARR_TYPE_INT   : return "I";
		case ARR_TYPE_LONG  : return "J";
		default: return NULL;
	}
}
