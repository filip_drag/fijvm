#include <stdio.h>
#include <pthread.h>
#include "heap.h"
#include "util.h"
#include "access-flags.h"
#include "array.h"
#include "java-object-consts.h"
#include "threads.h"
#include "classes.h"

//4 MB
#define MAX_HEAP_SIZE (1024*1024*4)

//for now the heap is only static memory which is easier doesn't need to free it
u1 heap[MAX_HEAP_SIZE];
size_t heapPointer = 0;
size_t objectId = 0;
Class* heapThreadClass = NULL;

pthread_mutex_t heapMutex = PTHREAD_MUTEX_INITIALIZER;

#define NAME_BUFF_SIZE 2048
char nameBuff[NAME_BUFF_SIZE];

JavaObject* allocObject(Class* clazz) {
	size_t i, ctr;

	pthread_mutex_lock(&heapMutex);

	if (heapPointer >= MAX_HEAP_SIZE) {
		printf("OutOfMemoeryError !!!!!!");
		pthread_mutex_unlock(&heapMutex);
		return (void*) 0xFFFFFFFFFFFFFFFF;
	}

	u1* objectPointer = heap + heapPointer;
	JavaObject* obj = (JavaObject*) objectPointer;

	//the first 2 slots are for clazz and vtable pointers
	size_t offset = sizeof(JavaObject),
			fieldsCount = 0;
	obj->flags = JAVA_OBJECT_CONST_OBJECT;

	//add a pointer to clazz
	obj->clazz = clazz;

	//and to vtable array
	obj->vtable = clazz->vtable;

	//init the mutex
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&obj->mutex, &attr);
	//init condvar
	pthread_cond_init(&obj->condVar, NULL);

	//first put the parent objects fields
	for (ctr = 0; ctr < clazz->classChainCount; ctr++) {
		Class* superClazz = clazz->classChain[ctr];

		FieldInfo* fields = superClazz->fieldArray;
		for (i = 0; i < superClazz->fieldCount; i++) {
			FieldInfo* field = &fields[i];
			if ((field->accessFlags & ACC_STATIC) == 1) {
				continue;
			}
			fieldsCount++;
			char* descriptor = getUtfStringAtIndex(superClazz, (size_t) field->descriptorIndex - 1);
			size_t byteSize = fieldSize(descriptor);
			u1* fieldPointer = objectPointer + offset;
			field->offset = offset;
			field->fieldSize = byteSize;
			offset += byteSize;
			if (heapPointer + offset >= MAX_HEAP_SIZE) {
				printf("OutOfMemoeryError !!!!!!");
				pthread_mutex_unlock(&heapMutex);
				return (void*) 0xFFFFFFFFFFFFFFFF;
			}

			Value val;
			val.val.longVal = 0;
			setFieldValue(fieldPointer, &val, descriptor);

		}
	}


	heapPointer += offset;
	obj->objectId = objectId++;

	if (clazz->byteSize == 0) {
		pthread_mutex_lock(&clazz->mutex);
		if (clazz->byteSize == 0) {
			clazz->byteSize = offset;
		}
		pthread_mutex_unlock(&clazz->mutex);
	}

	pthread_mutex_unlock(&heapMutex);

	return obj;

}

/*
 * // type = 3 means array of references
 primitiveArrayInfo(4,  0'Z, boolean, int).
primitiveArrayInfo(5,  0'C, char,    int).
primitiveArrayInfo(6,  0'F, float,   float).
primitiveArrayInfo(7,  0'D, double,  double).
primitiveArrayInfo(8,  0'B, byte,    int).
primitiveArrayInfo(9,  0'S, short,   int).
primitiveArrayInfo(10, 0'I, int,     int).
primitiveArrayInfo(11, 0'J, long,    long).
 */
JavaObject*  allocArray(Thread* thread, u1 type, u4 size, Class* class) {

	pthread_mutex_lock(&heapMutex);

	if (heapPointer >= MAX_HEAP_SIZE) {
		printf("OutOfMemoeryError !!!!!!");
		pthread_mutex_unlock(&heapMutex);
		return (void*) 0xFFFFFFFFFFFFFFFF;
	}

	//FIXME add creation and setting of array class
	//which extends Object http://docs.oracle.com/javase/specs/jls/se8/html/jls-10.html#jls-10.8
	u4 elementSize = arraySize(type);
	u4 objectByteSize;
	objectByteSize = ARRAY_HEADER_SIZE + elementSize* size;
	if (heapPointer + objectByteSize > MAX_HEAP_SIZE) {
		printf("OutOfMemoeryError !!!!!!");
		return (void*) 0xFFFFFFFFFFFFFFFF;
	}
	//first 4 bytes are array or class
	//second 4 bytes are unsigned size
	//third 4 bytes are type
	u1* objectPointer = heap + heapPointer;
	JavaObject* obj = (JavaObject*) objectPointer;
	if (class != NULL) {
		obj->type.elementClazz = class;
	} else {
		obj->type.primitiveType = type;
	}


	//init the mutex
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&obj->mutex, &attr);
	//init condvar
	pthread_cond_init(&obj->condVar, NULL);

	//0x10 means that the array will have 1 dimension this is stored in bits 32 - 4,
	//3- 0 hold the array type
	obj->flags = type | 0x10;
	obj->extra.length = size;
	u1* fieldPointer = objectPointer + ARRAY_HEADER_SIZE;
	memset(fieldPointer, 0 , elementSize * size);
	heapPointer += objectByteSize;
	obj->objectId = objectId++;

	pthread_mutex_unlock(&heapMutex);

	//set the class of the array
	memset(nameBuff, 0, NAME_BUFF_SIZE);
	if (type != ARR_TYPE_REF) {
		strcat(nameBuff, "[");
		strcat(nameBuff, arrayType2Letter(type));
	} else {
		strcat(nameBuff, "[L");
		strcat(nameBuff, class->className);
		strcat(nameBuff, ";");
	}
	Class* arrayClass = loadClassByQualifiedName(thread, nameBuff, 0);
	obj->vtable = arrayClass->vtable;
	obj->clazz = arrayClass;

	return obj;
}


