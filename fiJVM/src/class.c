#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "class.h"
#include "util.h"
#include "printf.h"
#include "descriptors.h"
#include "access-flags.h"
#include "strmap.h"

char * findSourceFile(Class* class);
u1* findLineNumberTable(Class* clazz, u2* lineNumberTableLength, u1* ptr);

Class* newClass() {
	Class* class = (Class *) malloc(sizeof(Class));
	class->vtable = NULL;
	class->superClassPtr = NULL;
	class->classChain = NULL;
	class->classChainCount = 0;
	class->interfaceChain = NULL;
	class->interfaceChainCount = 0;
	class->byteSize = 0;
	class->classLoader = NULL;
	return class;
}

Class* loadClass(char* path) {
	FILE *file;

	PRINTF("PAth is %s \n", path);
	file = fopen(path, "rb");
	if (file == NULL) {
		printf("Class not found %s \n", path);
		return NULL;
	}
	uint8_t buffer[10];
	fread(buffer, sizeof(uint8_t), 10, file);
	int i;
	for (i = 0; i < 10; i++) {
		PRINTF("byte %d %x \n",i,buffer[i]);
	}
	
	//fseek(file, offset, SEEK_SET);
	Class* class = newClass();
	class->magicNumber = BYTES_TO_U4( buffer + 0);

	//initializing recursive mutex cause in some places it is needed
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&class->mutex, &attr);
	PRINTF("Magic number =%x \n", class->magicNumber);
	if (class->magicNumber != 0xCAFEBABE) {
		PRINTF("Invalid class file %s\n", path);
		free(class);
		exit(1);
		return NULL;
	}
	class->minorVersion =  BYTES_TO_U2( buffer + 4);
	class->majorVersion =  BYTES_TO_U2(buffer + 6);
	if (class->majorVersion > 0x33) {
		PRINTF("Too high major version %d", class->majorVersion);
		return NULL;
	}
	class->constantPoolSize = BYTES_TO_U2( buffer + 8);
	PRINTF("Constant pool count = %d\n", class->constantPoolSize);
	int constantPoolSize;
	class->constantPoolArray = readConstantPool(file, class->constantPoolSize, &constantPoolSize);
	PRINTF("Constant pool size = %d\n", constantPoolSize);
	
	//--read some class specific stuff
	fread(buffer, sizeof(u1), 8, file);
	class->accessFlags = BYTES_TO_U2( buffer + 0);
	class->isInterface = ((class->accessFlags & ACC_INTERFACE) != 0) ? 1 : 0;
	class->thisClass = BYTES_TO_U2( buffer + 2);
	class->className = getUtfStringAtIndex(class, (size_t) class->constantPoolArray[class->thisClass - 1].nameIndex  - 1);
	class->superClass = BYTES_TO_U2( buffer + 4);
	
	//TODO read interfaces
	class->interfaceCount = BYTES_TO_U2( buffer + 6);
	PRINTF("Interface count = %d\n", class->interfaceCount);
	if (class->interfaceCount > 0 ) {
		u1* buff = malloc(sizeof(u2) * class->interfaceCount);
		class->interfaces = malloc(sizeof(u2) * class->interfaceCount);
		fread(buff , sizeof(u1), (size_t) (class->interfaceCount * sizeof(u2)), file);
		for ( i = 0; i < class->interfaceCount; i++) {
			class->interfaces[i] = BYTES_TO_U2(buff + i * 2);
		}
		free(buff);
	} else {
		class->interfaces = NULL;
	}

	fread(buffer, sizeof(u1), 2, file);
	class->fieldCount = BYTES_TO_U2( buffer + 0);
	PRINTF("Field count = %d\n", class->fieldCount);
	class->fieldArray = readFields(file, class->fieldCount, class);
	
	fread(buffer, sizeof(u1), 2, file);
	class->methodCount = BYTES_TO_U2( buffer + 0);
	PRINTF("Method count = %d\n", class->methodCount);
	class->methodArray = readMethods(file, class->methodCount, class);
	
	fread(buffer, sizeof(u1), 2, file);
	class->attributeCount = BYTES_TO_U2( buffer + 0);
	PRINTF("attributeCount = %d\n", class->attributeCount);
	class->attributes = readAttributes(file, class->attributeCount);

	class->sourceFile = findSourceFile(class);

	
	fclose(file);
	return class;
}


ConstantPoolInfo * readConstantPool(FILE *file, u2 constantPoolCount, int * constantPoolSize) {
	ConstantPoolInfo* constantPool = (ConstantPoolInfo *) malloc(sizeof(ConstantPoolInfo) * (size_t) (constantPoolCount - 1));
	*constantPoolSize = 0;
	u2 i,j;
	u1 buffer[32];
	for (i = 1, j = 0 ; i < constantPoolCount; i++, j++) {
		fread(buffer, sizeof(u1), 1, file);
		constantPool[j].tag = BYTES_TO_U1(buffer + 0);
		*constantPoolSize += 1;
		PRINTF("tag=%d index=%d\n",constantPool[j].tag, i);
		switch(constantPool[j].tag) {
			case CONSTANT_Class:
				fread(buffer, sizeof(u1), 2, file);
				constantPool[j].nameIndex = BYTES_TO_U2( buffer + 0);
				//PRINTF("Name index=%d\n", constantPool[j].nameIndex);
				*constantPoolSize += 2;
				break;
			case CONSTANT_Fieldref:
			case CONSTANT_Methodref:
			case CONSTANT_InterfaceMethodref:
				fread(buffer, sizeof(u1), 4, file);
				constantPool[j].classIndex = BYTES_TO_U2( buffer + 0 );
				constantPool[j].nameAndTypeIndex = BYTES_TO_U2( buffer + 2);
				*constantPoolSize += 4;
				break;
			case CONSTANT_String:
				fread(buffer, sizeof(u1), 2, file);
				constantPool[j].stringIndex = BYTES_TO_U2( buffer + 0);
				//PRINTF("   string index is %d \n", constantPool[j].stringIndex);
				*constantPoolSize += 2;
				break;
			case CONSTANT_Integer:
			case CONSTANT_Float:
				fread(buffer, sizeof(u1), 4, file);
				constantPool[j].bytes = BYTES_TO_U4( buffer + 0);
				*constantPoolSize += 4;
				break;
			case CONSTANT_Long:
			case CONSTANT_Double:
				fread(buffer, sizeof(u1), 8, file);
				constantPool[j].highBytes = BYTES_TO_U4( buffer + 0);
				constantPool[j].lowBytes = BYTES_TO_U4(buffer + 4);
				*constantPoolSize += 8;
				//This thing is because Long and double consts take to entries in the table
				//https://docs.oracle.com/javase/specs/jvms/se7/html/jvms-4.html#jvms-4.4.5
				i++;
				j++;
				//--------------------------------------------------------------------------
				break;
			case CONSTANT_NameAndType :
				fread(buffer, sizeof(u1), 4, file);
				constantPool[j].nameIndex = BYTES_TO_U2( buffer+0);
				constantPool[j].descriptorIndex = BYTES_TO_U2( buffer + 2);
				*constantPoolSize += 4;
				break;
			case CONSTANT_Utf8:
				fread(buffer, sizeof(u1), 2, file);
				*constantPoolSize += 2;
				constantPool[j].length = BYTES_TO_U2( buffer + 0 );
				constantPool[j].utfBytes = (u1*) malloc( ((size_t) constantPool[j].length + 1));
				fread(constantPool[j].utfBytes, sizeof(u1), constantPool[j].length, file);

				//insert the null char at the end
				constantPool[j].utfBytes[constantPool[j].length] = '\0';
				//PRINTF("   utf8=%s %d offset=%d\n", constantPool[j].utfBytes, constantPool[j].length, *constantPoolSize);
				*constantPoolSize += constantPool[j].length;
				break;
			case CONSTANT_MethodHandle :
				fread(buffer, sizeof(u1), 2, file);
				constantPool[j].referenceKind = BYTES_TO_U1( buffer + 0);
				constantPool[j].referenceIndex = BYTES_TO_U1( buffer + 1 );
				*constantPoolSize += 2;
				break;
			case CONSTANT_MethodType :
				fread(buffer, sizeof(u1), 2, file);
				constantPool[j].descriptorIndex = BYTES_TO_U2( buffer +0);
				*constantPoolSize += 2;
				break;
			case CONSTANT_InvokeDynamic:
				fread(buffer, sizeof(u1), 4, file);
				constantPool[j].bootstrapMethodAttrIndex = BYTES_TO_U2( buffer + 0 );
				constantPool[j].nameAndTypeIndex = BYTES_TO_U2( buffer + 2 );
				*constantPoolSize += 4;
				break;
				
		}
	}
	return constantPool;
}

FieldInfo* readFields(FILE* file, u2 fieldCount, Class* clazz) {
	FieldInfo* fields = (FieldInfo *) malloc(sizeof(FieldInfo) * fieldCount);
	int i;
	u1 buffer[32];
	for (i = 0; i < fieldCount; i++) {
		fread(buffer, sizeof(u1), 8, file);
		fields[i].accessFlags = BYTES_TO_U2(buffer + 0);
		fields[i].nameIndex = BYTES_TO_U2(buffer + 2);
		fields[i].descriptorIndex = BYTES_TO_U2(buffer + 4);
		fields[i].attributeCount = BYTES_TO_U2(buffer + 6);
		fields[i].attributes = readAttributes(file, fields[i].attributeCount);
		fields[i].staticValuePointer = NULL;
		
		fields[i].name = getUtfStringAtIndex(clazz, (size_t) fields[i].nameIndex - 1);
		fields[i].descriptor = getUtfStringAtIndex(clazz, (size_t) fields[i].descriptorIndex - 1);

	}
	return fields;
}

MethodInfo* readMethods(FILE* file, u2 methodCount, Class* clazz) {
	MethodInfo* methods;
	if (methodCount != 0) {
		methods = (MethodInfo *) malloc(sizeof(MethodInfo) * methodCount);
	} else {
		return NULL;
	}
	u2 i, maxStack, maxLocals;
	u4 codeLength;
	u1 buffer[32];
	for (i = 0; i < methodCount; i++) {
		fread(buffer, sizeof(u1), 8, file);
		methods[i].accessFlags = BYTES_TO_U2(buffer + 0);
		methods[i].nameIndex = BYTES_TO_U2(buffer + 2);
		methods[i].descriptorIndex = BYTES_TO_U2(buffer + 4);
		methods[i].attributeCount = BYTES_TO_U2(buffer + 6);
		methods[i].attributes = readAttributes(file, methods[i].attributeCount);
		methods[i].clazz = clazz;
		methods[i].exceptionTableCount = 0;
		methods[i].exceptionTable = NULL;
		methods[i].methodName = getUtfStringAtIndex(clazz, (size_t) (methods[i].nameIndex - 1));
		char* methodDescriptor = getUtfStringAtIndex(clazz, (size_t) (methods[i].descriptorIndex - 1));
		methods[i].descriptor = methodDescriptor;
		char* retDescriptor = getReturnType(methodDescriptor);
		if (strcmp(retDescriptor, "V") == 0) {
			methods[i].isVoid = 1;
		} else {
			methods[i].isVoid = 0;
		}
		u1* code = getCode(clazz, &methods[i],  &maxStack, &maxLocals, &codeLength);
		if (code != NULL) {
			methods[i].maxStack = maxStack;
			methods[i].maxLocals = maxLocals;
			methods[i].codeLength = codeLength;
			methods[i].code = code;
			methods[i].localVarsIndexes = malloc(methods[i].maxLocals * sizeof(u1));
			size_t startIndex = 0;
			if ((methods[i].accessFlags & ACC_STATIC) == 0) {
				//non static method reserve the first slot for "this" pointer
				startIndex = 1;
				methods[i].localVarsIndexes[0] = 0;
			}

			//TODO remove the local indexes
			//the locals can include also other locals which are not parameters
			size_t parameterCount = initLocalIndexes(methods[i].localVarsIndexes, methodDescriptor, startIndex);
			//if the method is non static exclude the 0-th local this from count
			methods[i].parameterCount = (u2) (parameterCount - startIndex) ;
		} else {
			methods[i].maxStack = 0;
			methods[i].maxLocals = 0;
			methods[i].codeLength = 0;
			methods[i].code = NULL;
			methods[i].localVarsIndexes = NULL;
			methods[i].parameterCount = (u2) getMethodParameterCount(methodDescriptor);
		}

	}
	return methods;
}

AttributeInfo* readAttributes(FILE* file, u2 attributeCount) {
	AttributeInfo* attributes;
	if (attributeCount != 0) {
		attributes = (AttributeInfo *) malloc(sizeof(AttributeInfo) * attributeCount);
	} else {
		return NULL;
	}
	int i;
	u1 buffer[32];
	for (i = 0; i < attributeCount; i++) {
		fread(buffer, sizeof(u1), 6, file);
		attributes[i].attributeNameIndex = BYTES_TO_U2(buffer + 0);
		attributes[i].attributeLength = BYTES_TO_U4(buffer + 2);
		attributes[i].info = (u1*) malloc(sizeof(u1) * attributes[i].attributeLength);
		fread(attributes[i].info, sizeof(u1), attributes[i].attributeLength, file);
	}
	return attributes;
}

void freeClass(Class* class) {
	int i, j;
	for (i = 1, j = 0 ; i < class->constantPoolSize; i++, j++) {
		ConstantPoolInfo poolInfo = class->constantPoolArray[j];
		if (poolInfo.tag == CONSTANT_Utf8) {
			free(poolInfo.utfBytes);
		}
	}
	if (class->constantPoolArray != NULL) {
		free(class->constantPoolArray);
	}
	
	//free fields
	for (i = 0; i < class->fieldCount; i++) {
		int attributeCount = class->fieldArray[i].attributeCount;
		AttributeInfo* attributes = class->fieldArray[i].attributes;
		for (j = 0; j < attributeCount; j++) {
			if (attributes[j].info != NULL) {
				free(attributes[j].info);
			}
		}
		free(attributes);
	}
	if (class->fieldArray != NULL) {
		free(class->fieldArray);
	}
	
	//free methods
	for (i = 0; i < class->methodCount; i++) {
		MethodInfo method = class->methodArray[i];
		int attributeCount = method.attributeCount;
		AttributeInfo* attributes = method.attributes;
		for (j = 0; j < attributeCount; j++) {
			if (attributes[j].info != NULL) {
				free(attributes[j].info);
			}
		}
		free(attributes);
		if (method.localVarsIndexes != NULL) {
			free(method.localVarsIndexes);
		}

		if (method.exceptionTable != NULL) {
			free(method.exceptionTable);
		}
	}
	if (class->methodArray != NULL) {
		free(class->methodArray);
	}
	
	//free the attributes
	int attributeCount = class->attributeCount;
	AttributeInfo* attributes = class->attributes;
	for (j = 0; j < attributeCount; j++) {
		if (attributes[j].info != NULL) {
			free(attributes[j].info);
		}
	}
	if (attributes != NULL) {
		free(attributes);
	}
	if (class->classChain != NULL) {
		free(class->classChain);
		class->classChain= NULL;
	}
	if (class->interfaceChain != NULL) {
		free(class->interfaceChain);
		class->interfaceChain= NULL;
	}
	if (class->interfaces != NULL) {
		free(class->interfaces);
		class->interfaces= NULL;
	}
	if (class->vtable != NULL) {
		StrMap *map = class->vtable->map;
		if (map != NULL) {
			sm_delete(map);
		}
		class->vtable->map = NULL;
		free(class->vtable);
		class->vtable = NULL;
	}
	if(class->className[0] == '[') {
		free(class->className);
	}

	free(class);
	PRINTF("free(class)\n");
}
u1* getCode(Class* clazz, MethodInfo* method, u2* maxStack, u2* maxLocals, u4* codeLength) {
	int i, j;
	for (i = 0; i < method->attributeCount; i++) {
		AttributeInfo* attrib = method->attributes + i;
		char* attribName = getUtfStringAtIndex(clazz, (size_t) attrib->attributeNameIndex - 1);
		if (strcmp(attribName, "Code") != 0) {
			continue;
		}
		*maxStack = BYTES_TO_U2(attrib->info + 0);
		*maxLocals = BYTES_TO_U2(attrib->info + 2);
		*codeLength = BYTES_TO_U4(attrib->info + 4);

		method->exceptionTableCount = BYTES_TO_U2(attrib->info + 8 + *codeLength);
		method->exceptionTable = NULL;
		u1* ptr = attrib->info + 10 + *codeLength;
		if (method->exceptionTableCount  != 0) {
			method->exceptionTable = malloc(sizeof(ExceptionTable) * method->exceptionTableCount);
			for (j = 0; j < method->exceptionTableCount; j++) {
				ExceptionTable* exc = &method->exceptionTable[j];
				exc->startPc = BYTES_TO_U2(ptr);
				exc->endPc = BYTES_TO_U2(ptr + 2);
				exc->handlerPc = BYTES_TO_U2(ptr + 4);
				exc->catchType = BYTES_TO_U2(ptr + 6);
				ptr += 8;
			}
		}
		//read the lineNumberTable
		method->lineNumberTable = findLineNumberTable(clazz, &method->lineNumberTableLength, ptr);

		return (attrib->info + 8);

	}
	return NULL;
}

char * findSourceFile(Class* class) {
	int i;
	for (i = 0; i < class->attributeCount; i++) {
		AttributeInfo* attrib = &class->attributes[i];
		char* attribName = getUtfStringAtIndex(class, (size_t) attrib->attributeNameIndex - 1);
		if (strcmp(attribName, "SourceFile") != 0) {
			continue;
		}
		size_t index = (size_t) BYTES_TO_U2(attrib->info) -  1;
		char* sourceFile = getUtfStringAtIndex(class, index);
		return sourceFile;
	}
	return NULL;
}

u1* findLineNumberTable(Class* clazz, u2* lineNumberTableLength, u1* ptr) {
	u2 i;
	u2 attribCount = BYTES_TO_U2(ptr);
	ptr += 2;
	for (i = 0; i < attribCount; i++) {
		u2 nameIndex = BYTES_TO_U2(ptr);
		u4 length =  BYTES_TO_U4(ptr + 2);
		ptr += 6;
		char* attribName = getUtfStringAtIndex(clazz, (size_t) nameIndex - 1);
		if (strcmp(attribName, "LineNumberTable") != 0) {
			ptr += length;
			continue;
		}
		*lineNumberTableLength = BYTES_TO_U2(ptr);
		ptr += 2;
		return ptr;

	}

	*lineNumberTableLength = 0;
	return NULL;
}

