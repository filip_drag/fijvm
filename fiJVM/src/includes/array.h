
#ifndef ARRAY_H_
#define ARRAY_H_
#include <inttypes.h>
#include "class-def.h"
#include "type-union.h"
#define ARRAY_HEADER_SIZE ((u4) sizeof(JavaObject))
#define DOUBLE_SIZE (8)
#define INT_SIZE (4)
#define REF_SIZE (sizeof(uintptr_t))

int storeIntIntoArray(void* ptr, int32_t index, int32_t value);
int loadIntFromArray(void* ptr, int32_t index, int32_t* value);
int storeFloatIntoArray(void* ptr, int32_t index, float value);
int loadFloatFromArray(void* ptr, int32_t index, float* value);
int storeDoubleIntoArray(void* ptr, int32_t index, double value);
int loadDoubleFromArray(void* ptr, int32_t index, double* value);
int storeLongIntoArray(void* ptr, int32_t index, int64_t value);
int loadLongFromArray(void* ptr, int32_t index, int64_t* value);
int storeRefIntoArray(void* ptr, int32_t index, uintptr_t value);
int loadRefFromArray(void* ptr, int32_t index, uintptr_t* value);
int storeShortIntoArray(void* ptr, int32_t index, int16_t value);
int loadShortFromArray(void* ptr, int32_t index, int16_t* value);
int storeByteIntoArray(void* ptr, int32_t index, int8_t value);
int loadByteFromArray(void* ptr, int32_t index, int8_t* value);
int storeCharIntoArray(void* ptr, int32_t index, uint16_t value);
int loadCharFromArray(void* ptr, int32_t index, uint16_t* value);
void setCharsInArray(JavaObject* obj, char* str);

u4 arraySize(u1 type);
char* arrayType2Letter(int primitiveType);
#endif
