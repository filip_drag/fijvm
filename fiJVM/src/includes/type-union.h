#ifndef TYPE_UNION_H_
#define TYPE_UNION_H_

#include <inttypes.h>
#include <pthread.h>
#include "class-def.h"

typedef struct {
	uint32_t flags;
	Class* clazz;
	VTable* vtable;
	int32_t objectId;
	union {
		Class* elementClazz;
		uint32_t primitiveType;
		void* nativeObject;
	} type;
	union {
		u4 length;
	} extra;
	//mutex for locking on class
	pthread_mutex_t mutex;
	//condition variable for wait/notify on this object
	pthread_cond_t condVar;
} JavaObject;

typedef enum {
	INT_JVM, STRING_JVM, FLOAT_JVM, DOUBLE_JVM, LONG_JVM, SHORT_JVM, BYTE_JVM, POINTER_JVM, CHAR_JVM, BOOL_JVM
} Type;

typedef struct {
	Type type;
	union {
		int32_t intVal;
		int8_t byteVal;
		int16_t shortVal;
		uint8_t boolVal;
		uint16_t charVal;

		float floatVal;
		JavaObject *pointer;
		double doubleVal;
		int64_t longVal;
	} val;
} Value;

#endif
