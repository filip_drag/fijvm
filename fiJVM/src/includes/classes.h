

#ifndef CLASSES_H_
#define CLASSES_H_
#include <inttypes.h>
#include "class.h"
#include "threads.h"


#define MAX_CLASS_COUNT 1000
#define MAX_PATH_CHARS 256
struct ClassEntry_ {
	char className[MAX_PATH_CHARS];
	Class* clazz;
};
typedef struct ClassEntry_ ClassEntry;
extern ClassEntry classList[MAX_CLASS_COUNT];
extern size_t classEntryCount;

//this is used during the creation of the main java thread
//to skip clinit methods and later run them
extern int classLoadingPhase;

void initClassHolder();
Class* findClassByQualifiedName(const char* name);
Class* loadClassByQualifiedName(Thread* threadId, const char* name, int returnAddress);
Class* loadClassByQualifiedNameInternall(Thread* threadId, const char* name, int returnAddress);
void initializeParentArray(Class* clazz);
void initializeVTable(Class* clazz);
int isVTableMethod(MethodInfo* method);
void initializeInterfaces(Thread* threadId, Class* clazz, int returnAddress);
void freeAllClasses();
void setClassSearchPath(char* pathParam);
void setSystemClassSearchPath(char* pathParam);
int runSkippedClinits(Thread* threadId, int returnAddress);
int runSkippedClassInits(Thread* threadId, int returnAddress);
JavaObject* createIfNotExistsPrimiteClass(Thread* thread, char* primitiveName);

#endif
