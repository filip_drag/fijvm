/*
 * reflection.h
 *
 *  Created on: Sep 14, 2015
 *      Author: fi
 */

#ifndef SRC_INCLUDES_REFLECTION_H_
#define SRC_INCLUDES_REFLECTION_H_
#include "type-union.h"
#include "threads.h"

JavaObject* getClassConstructors(Thread* thread, Class* clazz, int publicOnly);

#endif /* SRC_INCLUDES_REFLECTION_H_ */
