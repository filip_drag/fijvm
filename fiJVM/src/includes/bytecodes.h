#ifndef BYTECODES_H
#define BYTECODES_H

#define ICONST_M1 0x2
#define ICONST_0 0x3
#define ICONST_1 0x4
#define ICONST_2 0x5
#define ICONST_3 0x6
#define ICONST_4 0x7
#define ICONST_5 0x8

#define ACONST_NULL 0x01
#define LCONST_0 0x09
#define LCONST_1 0x0A

#define ISTORE   0x36
#define ISTORE_0 0x3b
#define ISTORE_1 0x3c
#define ISTORE_2 0x3d
#define ISTORE_3 0x3e

#define ASTORE   0x3A
#define ASTORE_0 0x4B
#define ASTORE_1 0x4C
#define ASTORE_2 0x4D
#define ASTORE_3 0x4E

#define FSTORE  0x38
#define FSTORE_0  0x43
#define FSTORE_1  0x44
#define FSTORE_2  0x45
#define FSTORE_3  0x46

#define DSTORE  0x39
#define DSTORE_0  0x47
#define DSTORE_1  0x48
#define DSTORE_2  0x49
#define DSTORE_3  0x4A

#define LSTORE  0x37
#define LSTORE_0 0x3F
#define LSTORE_1 0x40
#define LSTORE_2 0x41
#define LSTORE_3 0x42

#define ILOAD    0x15
#define ILOAD_0  0x1A
#define ILOAD_1  0x1B
#define ILOAD_2  0x1C
#define ILOAD_3  0x1D

#define FLOAD    0x17
#define FLOAD_0  0x22
#define FLOAD_1  0x23
#define FLOAD_2  0x24
#define FLOAD_3  0x25

#define DLOAD    0x18
#define DLOAD_0  0x26
#define DLOAD_1  0x27
#define DLOAD_2  0x28
#define DLOAD_3  0x29

#define LLOAD    0x16
#define LLOAD_0  0x1E
#define LLOAD_1  0x1F
#define LLOAD_2  0x20
#define LLOAD_3  0x21

#define ALOAD    0x19
#define ALOAD_0  0x2A
#define ALOAD_1  0x2B
#define ALOAD_2  0x2C
#define ALOAD_3  0x2D

#define RETURN 0xb1
#define GOTO 0xA7
#define GOTO_W 0xC8
#define IF_ICMPLT 0xA1
#define IF_ICMPGE 0xA2
#define IF_ICMPLE 0xA4
#define IF_ICMPGT 0xA3
#define IF_ICMPEQ 0x9F
#define IF_ICMPNE 0xA0
#define IFEQ      0x99
#define IFGE	0x9c
#define IFGT	0x9d
#define IFLE	0x9e
#define IFLT	0x9b
#define IFNE	0x9a
#define IFNONNULL	0xc7
#define IFNULL	0xc6

#define GETSTATIC 0xB2
#define PUTSTATIC 0xB3
#define INVOKEVIRTUAL 0xB6
#define INVOKEINTERFACE 0xB9
#define INVOKESTATIC  0xB8
#define INVOKESPECIAL 0xB7
#define PUTFIELD      0xB5
#define GETFIELD      0xB4

#define NEWARRAY  0xBC
#define ARRAYLENGTH 0xBE
#define IASTORE     0x4F
#define IALOAD      0x2E
#define LALOAD      0x2F
#define LASTORE     0x50
#define DALOAD      0x31
#define DASTORE     0x52

#define NEW       0xBB
#define LDC       0x12
#define LDC_W     0x13
#define IMUL 		0x68
#define IDIV        0x6C
#define INEG		0x74
#define IOR         0x80
#define IREM        0x70
#define ISHL        0x78
#define ISHR        0x7A
#define IUSHR       0x7C
#define ISUB        0x64
#define IINC        0x84
#define IXOR        0x82
#define IADD		0x60
#define IAND		0x7E

#define LRETURN     0xAD
#define DRETURN     0xAF
#define FRETURN     0xAE
#define ARETURN     0xB0
#define IRETURN     0xAC

#define BIPUSH     0x10
#define SIPUSH     0x11
#define POP		   0x57
#define POP2       0x58

#define DUP        0x59
#define DUP_X1     0x5A
#define DUP_X2     0x5B
#define DUP2       0x5C
#define DUP2_X1    0x5D
#define DUP2_X2    0x5E

#define WIDE       0xC4
#define RET        0xA9

#define I2B        0x91
#define I2C        0x92
#define I2D        0x87
#define I2F        0x86
#define I2L        0x85
#define I2S        0x93

#define F2D	0x8d
#define F2I	0x8b
#define F2L	0X8C
#define FALOAD	0X30
#define FASTORE	0X51
#define FCMPG	0X96
#define FCMPL	0X95
#define FCONST_0	0X0B
#define FCONST_1	0X0C
#define FCONST_2	0X0D
#define FADD	0X62
#define FDIV	0X6E
#define FMUL	0X6A
#define FNEG	0X76
#define FREM	0X72
#define FSUB	0X66

#define D2F	 0x90
#define D2I	 0x8E
#define D2L	 0x8F
#define DCMPG	 0x98
#define DCMPL	 0x97
#define DCONST_0	 0x0E
#define DCONST_1	 0x0F
#define DADD     0x63
#define DDIV	 0x6F
#define DMUL	 0x6B
#define DNEG	 0x77
#define DREM	 0x73
#define DSUB	 0x67

#define L2D	0x8a
#define L2F	0x89
#define L2I	0x88
#define LADD	0x61
#define LAND	0x7f
#define LCMP	0x94
#define LDIV	0x6d
#define LMUL	0x69
#define LNEG	0x75
#define LOR	    0x81
#define LREM	0x71
#define LSHL	0x79
#define LSHR	0x7b
#define LSUB	0x65
#define LUSHR	0x7d
#define LXOR	0x83

#define LDC2_W  0x14
#define LDC_W  0x13

#define AALOAD  0x32
#define AASTORE  0x53

#define SALOAD  0x35
#define SASTORE  0x56
#define CALOAD  0x34
#define CASTORE  0x55
#define BALOAD  0x33
#define BASTORE  0x54
#define ANEWARRAY 0xBD
#define IF_ACMPEQ 0xA5
#define IF_ACMPNE 0xA6

#define ATHROW   0xBF
#define INSTANCEOF 0xC1

#define MONITORENTER 0xC2
#define MONITOREXIT  0xC3
#define CHECKCAST    0xC0


const char* getNameOfInstruction(int code);

#endif
