#include <inttypes.h>
#include <stdint.h>
#include "class-def.h"
#include "type-union.h"

#define BYTES_TO_U4(buffer) (u4)( (u4)(buffer)[0] << 24 |  (u4)(buffer)[1] << 16 |  (u4)(buffer)[2] << 8 |  (u4)(buffer)[3])
#define BYTES_TO_U2(buffer) (u2)( (buffer)[0] << 8 | (buffer)[1])
#define BYTES_TO_U1(buffer) (u1)( (buffer)[0])

#define INT_TO_BYTES(buffer, val) {\
	(buffer)[0] = (unsigned char) (val >> 24);\
	(buffer)[1] = (unsigned char) (val >> 16) & 0xFF;\
	(buffer)[2] = (unsigned char) (val >> 8) & 0xFF;\
	(buffer)[3] = (unsigned char) (val) & 0xFF;\
}\

#define PTR_TO_BYTES(buffer, val) {\
	(buffer)[0] = (unsigned char) (val >> 56);\
	(buffer)[1] = (unsigned char) (val >> 48) & 0xFF;\
	(buffer)[2] = (unsigned char) (val >> 40) & 0xFF;\
	(buffer)[3] = (unsigned char) (val >> 32) & 0xFF;\
	(buffer)[4] = (unsigned char) (val >> 24) & 0xFF;\
	(buffer)[5] = (unsigned char) (val >> 16) & 0xFF;\
	(buffer)[6] = (unsigned char) (val >> 8) & 0xFF;\
	(buffer)[7] = (unsigned char) (val) & 0xFF;\
}\

#define SPLITUP(a,b,c) ( (b) = (unsigned long)((a) >> 32), (c) = (unsigned long)((a) & 0xffffffff) )
#define COMBINE(a,b,c) ( (a) = ((unsigned long long)(b) << 32) | (c) )
#define NATIVE_METHOD_NOT_FOUND (8)

char* getUtfStringAtIndex(Class* clazz, size_t index);
char* getClassName(Class* clazz);
char* getNameFromNameAndType(Class* clazz, size_t index);
FieldInfo* getField(Class* clazz, const char* fieldName);
size_t getFieldSize(Class* clazz, const char* fieldName);
size_t fieldSize(const char * descriptor);
void getRefValue(Value* dest, void* pointer, const char * descriptor);
char* getTypeFromNameAndType(Class* clazz, size_t index);
void setFieldValue(void* pointer, Value* value, const char* descriptor);
void encodePointerIntoString(void* ptr, char* result, size_t resSize);
void* decodePointerFromString(char* str);
char* getMethodName(MethodInfo* method) ;
int letterToCode(char* descriptor);
