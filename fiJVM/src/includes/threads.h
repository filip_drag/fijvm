#ifndef THREADS_H_
#define THREADS_H_

#include <pthread.h>
#include <signal.h>
#include "stacks.h"
#include "class-def.h"
#include "type-union.h"

#define MAX_CALL_STACK 200
typedef struct StackFrame_ StackFrame;
typedef struct Thread_ Thread;

#define THREAD_NOT_STARTED (1)
#define THREAD_RUNNING (2)
#define THREAD_STARTING (4)
#define THREAD_DEAD  (3)

extern int threadCount;
extern int maxThreadNumber;
#define MAX_THREAD_COUNT (1024)

extern Thread* threadList[MAX_THREAD_COUNT];
extern pthread_mutex_t threadListMutex;

struct StackFrame_ {
	int returnAddress;
	int ip;
	u2 maxLocals;
	Value* localVariables;
	u2 maxStack;
	Value* operandStack;
	Class* clazz;
	MethodInfo* method;
};


struct  Thread_ {
	pthread_t nativeThread;
	StackFrame callStack[MAX_CALL_STACK];
	int callStackPointer;
	int nativeExceptionFlag;
	long tid;
	JavaObject* javaThread;
	sig_atomic_t state;
	Thread* nextThread;
};

Thread* createThread();
void deleteThread(Thread* thread);
void destroyThreading();
void initThreading();

#endif
