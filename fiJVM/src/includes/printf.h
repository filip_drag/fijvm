
#ifndef PRINTF_H_
#define PRINTF_H_
#include <stdio.h>
#define DEBUG_ON 0

#define PRINTF(...) if (DEBUG_ON) printf(__VA_ARGS__);

#endif
