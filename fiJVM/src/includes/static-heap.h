#ifndef STATIC_HEAP_H_
#define STATIC_HEAP_H_
#include "class-def.h"
#include "type-union.h"

#define MAX_STATIC_HEAP_COUNT 50
//32KB for now
#define MAX_STATIC_HEAP_BYTE_COUNT (32 * 1024)

extern HeapEntry staticHeapList[MAX_STATIC_HEAP_COUNT];

extern u1 staticHeap[MAX_STATIC_HEAP_BYTE_COUNT];

extern size_t staticHeapEntryCount;

extern size_t staticHeapByteCount;

void initializeStaticFields(Class* clazz);
size_t fieldSize(const char * descriptor);
void getFieldValue(Value* value, FieldInfo* field, Class* clazz);

#endif
