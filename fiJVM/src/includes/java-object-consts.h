#ifndef JAVA_OBJECT_CONSTS_H_
#define JAVA_OBJECT_CONSTS_H_

 /* // type = 3 means array of references
 primitiveArrayInfo(4,  0'Z, boolean, int).
primitiveArrayInfo(5,  0'C, char,    int).
primitiveArrayInfo(6,  0'F, float,   float).
primitiveArrayInfo(7,  0'D, double,  double).
primitiveArrayInfo(8,  0'B, byte,    int).
primitiveArrayInfo(9,  0'S, short,   int).
primitiveArrayInfo(10, 0'I, int,     int).
primitiveArrayInfo(11, 0'J, long,    long).
		*/

#define ARR_TYPE_REF   (3)
#define ARR_TYPE_BOOL  (4)
#define ARR_TYPE_CHAR (5)
#define ARR_TYPE_FLOAT (6)
#define ARR_TYPE_DOUBLE (7)
#define ARR_TYPE_BYTE  (8)
#define ARR_TYPE_SHORT (9)
#define ARR_TYPE_INT   (10)
#define ARR_TYPE_LONG  (11)

#define JAVA_OBJECT_CONST_OBJECT (0)

#define JAVA_OBJECT_ARRAY_TYPES_COUNT (9)
#define JAVA_OBJECT_1D_REFS (3)
#define JAVA_OBJECT_1D_BOOL (4)
#define JAVA_OBJECT_1D_CHAR (5)
#define JAVA_OBJECT_1D_FLOAT (6)
#define JAVA_OBJECT_1D_DOUBLE (7)
#define JAVA_OBJECT_1D_BYTE (8)
#define JAVA_OBJECT_1D_SHORT (9)
#define JAVA_OBJECT_1D_INT (10)
#define JAVA_OBJECT_1D_LONG (11)

#endif
