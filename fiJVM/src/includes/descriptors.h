#ifndef DESCRIPTORS_H_
#define DESCRIPTORS_H_
#include "class-def.h"

size_t initLocalIndexes(u1* localIndexes, char* descriptor, size_t startIndex);
size_t getMethodParameterCount(char* descriptor);
char* parseArrayType(char* ptr);
char* parseObjectType(char* ptr);
char* parseBaseType(char* str);
char* parseCompoundType(char* str);
void methodDescriptorNoRetType(char* descriptor, char* result, size_t resultSize);
void getMethodKey(MethodInfo* method, char* resultKey, size_t resultKeySize);
void getMethodKeyStr(char* name, char* descriptor, char* resultKey, size_t resultKeySize);
char* getReturnType(char* descriptor);
#endif
