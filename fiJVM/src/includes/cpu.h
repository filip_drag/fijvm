#ifndef CPU_H
#define CPU_H
#include <inttypes.h>
#include <stdint.h>
#include "class-def.h"
#include "type-union.h"
#include "threads.h"

typedef struct {
	Thread* thread;
	MethodInfo* method;
	Value* parentStack;
	int parentStackPointer;
} ThreadParams;

int startMain(char* className);
void* start(void* params);
void startMethodInThread(void* params);
int runMethod(Thread* threadId, MethodInfo* currMethod , int returnAddress, Value* parentStack, int* parentStackPointer);
MethodInfo* findMethod(Class* class, const char* name, const char* paramDescriptors, u2 accessFlags);
int pushStackFrame(Thread* threadId, int returnAddress, 	u2 maxLocals, u2 maxStack, MethodInfo* method) ;
int pushStackFrame(Thread* threadId, int returnAddress, 	u2 maxLocals, u2 maxStack, MethodInfo* method);
void popStackFrame(Thread* threadId);
void cleanCallStack(Thread* threadId);
void initLocals(Value* locals, Value* parentStack, int* parentStackPointer, size_t paramCount, int startIndex, u1* localIndexes);
char* getClassNameByRef(Class* clazz, size_t index);
char* getClassNameByClassRef(Class* clazz, size_t index);
void* getFieldReference(Class* clazz, char* fieldName);
int runClinitMethod(Thread* threadId, Class* clazz, int returnAddress);
void initThread(Thread* threadId);
size_t getFieldOffset(Class* clazz, char* fieldName);
void getWideConstant(ConstantPoolInfo* entry, Value* value);
u1* exceptionJump(MethodInfo* method, JavaObject* exception, int ip) ;
JavaObject* createException(Thread* threadId, char* className, char* message, int returnAddress);
void fakePrintln(const char * methodDescriptor, Value* val);
#endif
