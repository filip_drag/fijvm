#ifndef CLASS_H
#define CLASS_H
#include <stdio.h>
#include "class-def.h"

Class* loadClass(char * path);
ConstantPoolInfo * readConstantPool(FILE *file, u2 constantPoolCount, int * constantPoolSize);
FieldInfo* readFields(FILE* file, u2 fieldCount, Class* clazz);
MethodInfo* readMethods(FILE* file, u2 methodCount, Class* clazz);
AttributeInfo* readAttributes(FILE* file, u2 attributeCount);
void freeClass(Class* class);

u1* getCode(Class* clazz, MethodInfo* method, u2* maxStack, u2* maxLocals, u4* codeLength);
Class* newClass();
#endif
