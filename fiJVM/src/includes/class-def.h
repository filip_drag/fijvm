#ifndef CLASS_DEF_H
#define CLASS_DEF_H
#include <stdint.h>
#include <pthread.h>
#include "strmap.h"
typedef uint8_t u1;
typedef uint16_t u2;
typedef uint32_t u4;
#define CONSTANT_Class	7
#define CONSTANT_Fieldref	9
#define CONSTANT_Methodref	10
#define CONSTANT_InterfaceMethodref	11
#define CONSTANT_String	8
#define CONSTANT_Integer	3
#define CONSTANT_Float	4
#define CONSTANT_Long	5
#define CONSTANT_Double	6
#define CONSTANT_NameAndType	12
#define CONSTANT_Utf8	1
#define CONSTANT_MethodHandle	15
#define CONSTANT_MethodType	16
#define CONSTANT_InvokeDynamic	18

struct AttributeInfo_;
typedef struct AttributeInfo_ AttributeInfo;

struct HeapEntry_;
typedef struct HeapEntry_ HeapEntry;

struct Class_;
typedef struct Class_ Class;

struct VTable_;
typedef struct VTable_ VTable;

struct ExceptoinTable_;
typedef struct ExceptionTable_ ExceptionTable;

struct MethodInfo_ {
	u2  accessFlags;
    u2  nameIndex;
    u2  descriptorIndex;
    u2  attributeCount;
    AttributeInfo* attributes;

    u1* code;
    u4 codeLength;
    u2 maxStack;
    u2 maxLocals;

    u2 exceptionTableCount;
    ExceptionTable* exceptionTable;

    u2 lineNumberTableLength;
    u1* lineNumberTable;

    int isVoid;
    u1* localVarsIndexes;
    u2  parameterCount;
    Class* clazz;
    char* methodName;
    char* descriptor;
};
typedef struct MethodInfo_ MethodInfo;

struct ExceptionTable_ {
	u2 startPc;
	u2 endPc;
	u2 handlerPc;
	u2 catchType;
};
struct ConstantPoolInfo_ {
	u1 tag;
	u2 nameIndex;
	u2 classIndex;
	u2 nameAndTypeIndex;
	u2 stringIndex;
	u4 bytes;
	u4 highBytes;
    u4 lowBytes;
	u2 descriptorIndex;
	u2 length;
	u1* utfBytes;
	u1 referenceKind;
	u2 referenceIndex;
	u2 bootstrapMethodAttrIndex;
};
typedef struct ConstantPoolInfo_ ConstantPoolInfo;

struct AttributeInfo_ {
	u2 attributeNameIndex;
    u4 attributeLength;
	u1* info;
};


struct FieldInfo_ {
	u2 accessFlags;
	u2 nameIndex;
	u2 descriptorIndex;
	u2 attributeCount;
	AttributeInfo* attributes;

	char* name;
	char* descriptor;
	u1* staticValuePointer;
	size_t offset;
	size_t fieldSize;
};
typedef struct FieldInfo_ FieldInfo;

struct Class_ {
	u4 magicNumber;
	u2 minorVersion;
	u2 majorVersion;
	u2 constantPoolSize;
	ConstantPoolInfo * constantPoolArray;
	u2 accessFlags;
	u2 thisClass;
	u2 superClass;
	u2 interfaceCount;
	u2* interfaces;
	u2 fieldCount;
	FieldInfo * fieldArray;
	u2 methodCount;
	MethodInfo * methodArray;
	u2 attributeCount;
	AttributeInfo* attributes;

	//className
	char* className;

	//sourceFile
	char* sourceFile;

	//java.lang.Class object
	void* java_Lang_Class;

	//the defining classLoader
	void* classLoader;

	//mutex for locking on class
	pthread_mutex_t mutex;

	//additional properties
	HeapEntry* staticHeapEntry;
	size_t byteSize;
	VTable* vtable;
	Class* superClassPtr;
	size_t classChainCount;
	Class** classChain;

	size_t interfaceChainCount;
	Class** interfaceChain;
	int isInterface;
};

struct Code {
	u2 attributeNameIndex;
    u4 attributeLength;
    u2 maxStack;
    u2 maxLocals;
    u4 codeLength;
    u1* code;
};

typedef struct Code_ Code;

struct HeapEntry_ {
	Class* clazz;
	size_t length;
	u1* pointer;
};


struct VTable_ {
	u4 vtableSize;
	StrMap *map;
};

#endif
