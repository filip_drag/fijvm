#ifndef SRC_INCLUDES_DEBUGGER_H_
#define SRC_INCLUDES_DEBUGGER_H_
#include "class-def.h"
#include "threads.h"

extern int DEBUGGER_ON;
extern int IS_DEBUGGER_READY;

extern int threadStartEvent, threadEndEvent;

void initDebugger(uint16_t port);
void checkClassPrepare(Thread* thread, Class* clazz);

void closeDebugger();

#define THREAD_START_EVENT_KIND 6
#define THREAD_END_EVENT_KIND 7
#define CLASS_PREPARE_EVENT_KIND 8
#define THREAD_START_REQUEST_ID 60
#define THREAD_END_REQUEST_ID 70

#endif
