#ifndef JNI_METHODS_H_
#define JNI_METHODS_H_
#include "class-def.h"
#include "type-union.h"
#include "threads.h"

int callJniMethod(Thread* threadId, MethodInfo* method, Value* parentStack, int* parentStackPointer);
void initJni();
void destroyJni();
#endif
