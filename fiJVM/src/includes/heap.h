#ifndef HEAP_H_
#define HEAP_H_

#include "class-def.h"
#include "type-union.h"
#include "threads.h"

JavaObject* allocObject(Class* clazz);
JavaObject*  allocArray(Thread* thread, u1 type, u4 size, Class* class);
extern Class* heapThreadClass;

//4 MB
#define MAX_HEAP_SIZE (1024*1024*4)

extern size_t heapPointer;

#endif
