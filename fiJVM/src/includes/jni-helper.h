#ifndef JNI_HELPER_H_
#define JNI_HELPER_H_
#include "class-def.h"
#include "type-union.h"
#include "threads.h"

MethodInfo* findMethod(Class* class, const char* name, const char* paramDescriptors, u2 accessFlags);
MethodInfo* findMethodNoAccessFlags(Class* class, char* name, char* paramDescriptors);
Class* getClass(JavaObject* ptr);
FieldInfo* findField(Class* class, const char* name, const char* descriptor);
void fieldValue(Value* value, void* ptr, FieldInfo* field);
JavaObject* charArrayToString(Thread* threadId, char* str);
void setField(Value* value, void* ptr, FieldInfo* field);
u4 getArrayLength(JavaObject* ptr);
int hasSuperClass(Class* subClass, Class* superClass);
int hasSuperInterface(Class* subClass, Class* superInterface);
int isAssignableFrom(Class* subClass, Class* superClass);
int isArrayOfRefs(JavaObject* obj);
int arrayNumberOfDimensions(JavaObject* obj);
int arrayType(JavaObject* obj);
MethodInfo* findVirtualMethod(VTable* vtable, char* methodName, char* methodDescriptor);
#endif
