#ifndef STRING_POOL_H
#define STRING_POOL_H
#include "class-def.h"
#include "type-union.h"
#include "threads.h"

extern u4 stringPoolSize;

JavaObject* addToStringPoolIfNotExists(Thread* threadId, char* str);
void initStringPool();
void destroyStringPool();
size_t getBytesLength(JavaObject* array);
char* convertStringToNativeCharArr(void* ptr);
#endif
