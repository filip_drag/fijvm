#include <string.h>
#include "static-heap.h"
#include "access-flags.h"
#include "util.h"
#include "printf.h"

HeapEntry staticHeapList[MAX_STATIC_HEAP_COUNT];
u1 staticHeap[MAX_STATIC_HEAP_BYTE_COUNT];
size_t staticHeapEntryCount = 0;
size_t staticHeapByteCount = 0;


void initializeStaticFields(Class* clazz) {
	u1* currStaticHeapPointer = staticHeap + staticHeapByteCount;
	size_t i;
	size_t offset = 0,
			staticFieldsCount = 0;
	FieldInfo* fields = clazz->fieldArray;
	for (i = 0; i < clazz->fieldCount; i++) {
		FieldInfo* field = &fields[i];
		if ((field->accessFlags & ACC_STATIC) == 0) {
			continue;
		}
		staticFieldsCount++;
		char* descriptor = getUtfStringAtIndex(clazz, (size_t) field->descriptorIndex - 1);
		size_t byteSize = fieldSize(descriptor);
		field->staticValuePointer = currStaticHeapPointer + offset;


		//FIXME THIS doesn't work for Strings
		Value val;
		val.val.longVal = 0;
		getFieldValue(&val, field, clazz);
		setFieldValue(field->staticValuePointer, &val, descriptor);
		//printf("fieldValue=%d %x %ld\n", *field->staticValuePointer, *((u4*)field->staticValuePointer), ll);
		//only static fields
		char* ptr = getUtfStringAtIndex(clazz, (size_t) field->nameIndex - 1);
		PRINTF("Add static field for class %s - with name %s\n", getClassName(clazz), ptr);
		offset += byteSize;
	}
	if (staticFieldsCount > 0) {
		HeapEntry entry = staticHeapList[staticHeapEntryCount];
		entry.pointer = currStaticHeapPointer;
		entry.clazz = clazz;
		clazz->staticHeapEntry = &entry;
		staticHeapEntryCount++;
		staticHeapByteCount += offset;
	}
}



void getFieldValue(Value* value, FieldInfo* field, Class* clazz) {
	size_t i;
	for (i = 0; i < field->attributeCount; i++) {
		AttributeInfo* attrInfo = &field->attributes[i];
		char* attributeName = getUtfStringAtIndex(clazz, (size_t) attrInfo->attributeNameIndex - 1);
		if (strcmp(attributeName, "ConstantValue") != 0) {
			continue;
		}
		size_t constValIndex = BYTES_TO_U2(attrInfo->info);
		PRINTF("constant value index=%d\n", (int) constValIndex);
		ConstantPoolInfo * constInfo = &clazz->constantPoolArray[constValIndex - 1];
		switch (constInfo->tag) {

			case CONSTANT_Integer:
				value->val.intVal = *((int32_t*)&constInfo->bytes);
				break;
			case CONSTANT_Float:
				value->val.floatVal = *((float*)&constInfo->bytes);
				break;
			case CONSTANT_Long: {
					uint64_t val;
					COMBINE(val, constInfo->highBytes, constInfo->lowBytes);
					value->val.longVal = *((int64_t*) &val);
				}
				break;
			case CONSTANT_Double: {
					uint64_t val;
					COMBINE(val, constInfo->highBytes, constInfo->lowBytes);
					value->val.doubleVal = *((double*) &val);
				}
				break;


		}
	}

}


