#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include "string-pool.h"
#include "strmap.h"
#include "heap.h"
#include "util.h"
#include "classes.h"
#include "array.h"
#include "cpu.h"
#include "type-union.h"
#include "jni-helper.h"
#include "threads.h"

StrMap *stringPool;
pthread_mutex_t stringMutex = PTHREAD_MUTEX_INITIALIZER;

JavaObject* addToStringPoolIfNotExists(Thread* threadId, char* str) {

	char ptrBuffer[sizeof(uintptr_t) * 2 + 1];
	unsigned int size = sizeof(uintptr_t) * 2 + 1;
	pthread_mutex_lock(&stringMutex);
	int result = sm_get(stringPool, str, ptrBuffer, size);
	pthread_mutex_unlock(&stringMutex);

	if (result != 0) {
		void* ptr = decodePointerFromString(ptrBuffer);
		return ptr;
	}

	JavaObject* stringPointer = charArrayToString(threadId, str);
	encodePointerIntoString(stringPointer, ptrBuffer, size);

	pthread_mutex_lock(&stringMutex);
	sm_put(stringPool, str, ptrBuffer);
	pthread_mutex_unlock(&stringMutex);

	return stringPointer;
}


char* convertStringToNativeCharArr(void* ptr) {
	Class* stringClass = getClass(ptr);
	FieldInfo* field = findField(stringClass, "value", "[C");
	Value val;
	fieldValue(&val, ptr, field);
	JavaObject* array = val.val.pointer;
	pthread_mutex_lock(&array->mutex);

	int32_t length = (int32_t) getArrayLength(array);
	size_t bytes =  getBytesLength(array);
	char* str = malloc(bytes + 1);
	str[bytes] = '\0';
	size_t j = 0;
	int32_t i;
	uint16_t c;
	for (i = 0; i < length; i++) {
		loadCharFromArray(array, i, &c);
		if (c >= 0x01 && c <= 0x7F) {
			str[j++] = (char) c;
		} else if ( (c >= 0x80 && c <= 0x7FF) || c == 0x0) {
			str[j++] = (char)(((c >> 6) & 0x1F) | 0xC0);
			str[j++] = (char)((c & 0x3F) | 0x80);
		}
//FIXME add for the more types
	}
	pthread_mutex_unlock(&array->mutex);
	return str;
}

size_t getBytesLength(JavaObject* array) {
	size_t length = (size_t) getArrayLength(array);
	size_t sum = 0, i;
	uint16_t c;
	for (i = 0 ; i < length; i++) {
		loadCharFromArray(array, (int32_t) i, &c);
		if (c >= 0x01 && c <= 0x7F) {
			sum += 1;
		} else if ( (c >= 0x80 && c <= 0x7FF) || c == 0x0) {
			sum += 2;
//FIXME revisit this stuff later
		} else if (c >= 0x800 && c <= 0xFFFF) {
			sum += 3;
		} else if (c > 0xFFFF) {
			sum += 6;
		}
	}
	return sum;
}



void initStringPool() {
	stringPool = sm_new(100);
	if (stringPool == NULL) {
	   printf("String pool init error\n");
	}
}

void destroyStringPool() {
	if (stringPool != NULL) {
		sm_delete(stringPool);
	}
	pthread_mutex_destroy(&stringMutex);
}



