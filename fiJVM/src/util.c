#include <string.h>
#include "printf.h"
#include "util.h"
#include "java-object-consts.h"

char* getUtfStringAtIndex(Class* clazz, size_t index) {
	ConstantPoolInfo poolInfo = clazz->constantPoolArray[index];
	if (poolInfo.tag != CONSTANT_Utf8) {
		PRINTF("Opps the tag is not UTF8_info\n");
	}
	char* bytes = (char*)poolInfo.utfBytes;
	return bytes;
}

char* getClassName(Class* clazz) {
	ConstantPoolInfo * info = &clazz->constantPoolArray[clazz->thisClass - 1];//field ref
	return getUtfStringAtIndex(clazz, (size_t) (info->nameIndex - 1));
}

char* getMethodName(MethodInfo* method) {
	return getUtfStringAtIndex(method->clazz, (size_t) (method->nameIndex - 1));
}

char* getNameFromNameAndType(Class* clazz, size_t index) {
	ConstantPoolInfo * info = &clazz->constantPoolArray[index];//field ref
	size_t nameIndex = clazz->constantPoolArray[info->nameAndTypeIndex - 1].nameIndex;//name and type
	return getUtfStringAtIndex(clazz, nameIndex - 1);
}

char* getTypeFromNameAndType(Class* clazz, size_t index) {
	ConstantPoolInfo * info = &clazz->constantPoolArray[index];//field ref
	size_t nameIndex = clazz->constantPoolArray[info->nameAndTypeIndex - 1].descriptorIndex;//name and type
	return getUtfStringAtIndex(clazz, nameIndex - 1);
}

FieldInfo* getField(Class* clazz, const char* fieldName) {
	size_t i;
	int k;
	for (k = (int) clazz->classChainCount - 1; k >= 0; k--) {
		Class* currClazz = clazz->classChain[k];
		for (i = 0; i < currClazz->fieldCount; i++) {
			FieldInfo* info = &currClazz->fieldArray[i];
			char * name = getUtfStringAtIndex(currClazz, (size_t) info->nameIndex - 1);
			if (strcmp(name, fieldName) != 0) {
				continue;
			}
			return info;
		}
	}
	return 0;
}

size_t getFieldSize(Class* clazz, const char* name) {
	FieldInfo* field = getField(clazz, name);
	char* descriptor = getUtfStringAtIndex(clazz, (size_t) field->descriptorIndex - 1);
	return fieldSize(descriptor);
}

size_t fieldSize(const char * descriptor) {
	if (strcmp(descriptor, "I") == 0) {
		return 4;
	}
	if (strcmp(descriptor, "S") == 0) {
		return 2;
	}
	if (strcmp(descriptor, "B") == 0) {
		return 1;
	}
	if (strcmp(descriptor, "C") == 0) {
		return 2;
	}
	if (strcmp(descriptor, "J") == 0) { //long
		return 8;
	}
	if (strcmp(descriptor, "F") == 0) { //float
		return 4;
	}
	if (strcmp(descriptor, "D") == 0) { //double
		return 8;
	}
	if (strcmp(descriptor, "Z") == 0) { //boolean
		return 1;
	}

	//some reference
	//8 bytes to support 64 bit operating systems
	return sizeof(uintptr_t);

}

int letterToCode(char* descriptor) {
	if (strcmp(descriptor, "I") == 0) {
		return ARR_TYPE_INT;
	}
	if (strcmp(descriptor, "S") == 0) {
		return ARR_TYPE_SHORT;
	}
	if (strcmp(descriptor, "B") == 0) {
		return ARR_TYPE_BYTE;
	}
	if (strcmp(descriptor, "C") == 0) {
		return ARR_TYPE_CHAR;
	}
	if (strcmp(descriptor, "J") == 0) { //long
		return ARR_TYPE_LONG;
	}
	if (strcmp(descriptor, "F") == 0) { //float
		return ARR_TYPE_FLOAT;
	}
	if (strcmp(descriptor, "D") == 0) { //double
		return ARR_TYPE_DOUBLE;
	}
	if (strcmp(descriptor, "Z") == 0) { //boolean
		return ARR_TYPE_BOOL;
	}
	return -1;
}

void setFieldValue(void* ptr, Value* dest, const char* descriptor) {
	if (strcmp(descriptor, "I") == 0) {
		*((int32_t*)ptr) = dest->val.intVal;
		return;
	}
	if (strcmp(descriptor, "S") == 0) {
		*((int16_t*)ptr) = dest->val.shortVal;
		return;
	}
	if (strcmp(descriptor, "B") == 0) {
		*((int8_t*)ptr) = dest->val.byteVal;
		return;
	}
	if (strcmp(descriptor, "C") == 0) {
		*((uint16_t*)ptr) = dest->val.charVal;
		return;
	}
	if (strcmp(descriptor, "J") == 0) { //long
		*((int64_t*)ptr) = dest->val.longVal;
		return;
	}
	if (strcmp(descriptor, "F") == 0) { //float
		*((float*)ptr) = dest->val.floatVal;
		return;
	}
	if (strcmp(descriptor, "D") == 0) { //double
		*((double*)ptr) = dest->val.doubleVal;
		return;
	}
	if (strcmp(descriptor, "Z") == 0) { //boolean
		*((uint8_t*)ptr) = dest->val.boolVal;
		return;
	}

	uintptr_t ptrVal = (uintptr_t) dest->val.pointer;
	*((uintptr_t*)ptr) = ptrVal;

}

void getRefValue(Value* dest, void* ptr, const char * descriptor) {
	if (strcmp(descriptor, "I") == 0) {
		dest->val.intVal = *((int32_t*)ptr);
		dest->type = INT_JVM;
		return;
	}
	if (strcmp(descriptor, "S") == 0) {
		dest->val.shortVal = *((int16_t*)ptr);
		dest->type = SHORT_JVM;
		return;
	}
	if (strcmp(descriptor, "B") == 0) {
		dest->val.byteVal = *((int8_t*)ptr);
		dest->type = BYTE_JVM;
		return;
	}
	if (strcmp(descriptor, "C") == 0) {
		dest->val.charVal = *((uint16_t*)ptr);
		dest->type = CHAR_JVM;
		return;
	}
	if (strcmp(descriptor, "J") == 0) { //long
		dest->val.longVal = *((int64_t*)ptr);
		dest->type = LONG_JVM;
		return;
	}
	if (strcmp(descriptor, "F") == 0) { //float
		dest->val.floatVal = *((float*)ptr);
		dest->type = FLOAT_JVM;
		return;
	}
	if (strcmp(descriptor, "D") == 0) { //double
		dest->val.doubleVal = *((double*)ptr);
		dest->type = DOUBLE_JVM;
		return;
	}
	if (strcmp(descriptor, "Z") == 0) { //boolean
		dest->val.intVal = *((uint8_t*)ptr);
		dest->type = BOOL_JVM;
		return;
	}
	uintptr_t ptrVal = *((uintptr_t*) ptr);
	dest->val.pointer = (JavaObject*)ptrVal;
	dest->type = POINTER_JVM;
}

void encodePointerIntoString(void* ptr, char* result, size_t resSize) {
	memset(result, 0, resSize);
	uintptr_t val = (uintptr_t) ptr;
	int i, j;
	for ( i = 0, j = sizeof(uintptr_t) * 8 - 4 ; j >= 0; i++, j -= 4) {
		char c = (char) (((val >> j) & 0xF) + 'A');
		result[i] = c;
	}
}
void* decodePointerFromString(char* str) {
	char c;
	char* ptr = str;
	uintptr_t res = 0;
	int j = sizeof(uintptr_t) * 8 - 4 ;
	while (*ptr != 0) {
		c = (char) (*ptr - 'A');
		res |= (uintptr_t)(((uintptr_t)c) << j);
		ptr++;
		j -= 4;
	}
	return (void*) res;
}


