#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include "debugger.h"
#include "util.h"
#include "threads.h"

#define HEADER_SIZE 11

int sockfd, newsockfd;
int IS_DEBUGGER_READY;

int threadStartEvent = 0, threadEndEvent = 0;
int dummyRequestEvent = 333;

int sendSizes(unsigned char buffer[]);
int dispatchCommand(unsigned char buffer[]);
int group1(unsigned char buffer[]);
int group15(unsigned char buffer[]);
void debuggerLoop();
int eventRequestSet(unsigned char buffer[]);
int sendVersionInfo(unsigned char buffer[]);
int sendAllThreads(unsigned char buffer[]);
char* getQualifiedName(char* className);
void sendClassPrepare(Thread* thread, Class* clazz, int eventId);
char* getJNISignature(char* className);

unsigned char largeBuff[1024];
int eventCounter = 100;
int idCounter = 4000;


#define MAX_NAME_LENGTH 512
#define MAX_CLASS_PATTERNS 50
char classPreparePatternList[MAX_CLASS_PATTERNS][MAX_NAME_LENGTH];
int classPrepareEventIds[MAX_CLASS_PATTERNS];
int classPreparePatternCount = 0;

pthread_t debuggerThread;


int dispatchCommand(unsigned char buffer[]) {
	switch(buffer[9]) {
		case 0x1:
			return group1(buffer);
		case 0xF:
			return group15(buffer);

	}
	return 0;
}

int group1(unsigned char buffer[]) {
	switch(buffer[10]) {
		case 0x1:
			return sendVersionInfo(buffer);
		case 0x4:
			return sendAllThreads(buffer);
		case 0x7:
			return sendSizes(buffer);
			break;
	}
	return 0;
}
int group15(unsigned char buffer[]) {
	switch(buffer[10]) {
		case 0x1:
			return eventRequestSet(buffer);
	}
	return 0;
}

int sendAllThreads(unsigned char buffer[]) {

	size_t size = 11 + 4;
	unsigned char response[11 + 4];
	int id = (int) BYTES_TO_U4(buffer + 4);
	INT_TO_BYTES(response, size)
	INT_TO_BYTES(response + 4, id)
	//reply flag raised
	response[8] = 0x80;
	//error code success eg. 0
	response[9] = 0;
	response[10] = 0;
	INT_TO_BYTES(response + HEADER_SIZE, 0);

	write(newsockfd, response, size);
	return 0;
}

int sendVersionInfo(unsigned char buffer[]) {

#define RESP_SIZE (11 + 3*4 + 2*4 + 2*5 + 10)
	unsigned char response[RESP_SIZE];
	const char* description = "fiJVM";
	const char* version = "1.7u40-b43";
	size_t size = RESP_SIZE;
	int id = (int) BYTES_TO_U4(buffer + 4);

	INT_TO_BYTES(response, size)
	INT_TO_BYTES(response + 4, id)
	//reply flag raised
	response[8] = 0x80;
	//error code success eg. 0
	response[9] = 0;
	response[10] = 0;

	int offset = HEADER_SIZE;
	INT_TO_BYTES(response + offset, strlen(description))
	offset += 4;
	memcpy(response + offset, description, strlen(description));
	offset += (int) strlen(description);

	INT_TO_BYTES(response + offset, 51)
	offset += 4;
	INT_TO_BYTES(response + offset, 49)
	offset += 4;

	INT_TO_BYTES(response + offset, strlen(version))
	offset += 4;
	memcpy(response + offset, version, strlen(version));
	offset += (int) strlen(version);

	INT_TO_BYTES(response + offset, strlen(description))
	offset += 4;
	memcpy(response + offset, description, strlen(description));
	offset += (int) strlen(description);
	write(newsockfd, response, size);

	return 0;
}

int eventRequestSet(unsigned char buffer[]) {

	int i;
	unsigned char response[11 + 4];
	size_t size = 11 + 4 * 5;
	int id = (int) BYTES_TO_U4(buffer + 4);
	int length = (int) BYTES_TO_U4(buffer);
	if (length - 11 >= 1024) {
		printf("OVERFLOWWWWWWWWWWWWWW of buffer");
	}
	read(newsockfd, largeBuff, (size_t) length - 11);

	for (i = 0; i < length - 11; i++) {
		printf("%x ", largeBuff[i]);
	}
	printf("\n");
	switch(largeBuff[0]) {
		case THREAD_START_EVENT_KIND:
			INT_TO_BYTES(response + HEADER_SIZE, THREAD_START_REQUEST_ID)
			threadStartEvent = 1;
			break;
		case THREAD_END_EVENT_KIND:
			INT_TO_BYTES(response + HEADER_SIZE, THREAD_END_REQUEST_ID)
			threadEndEvent = 1;
			break;
		case CLASS_PREPARE_EVENT_KIND: {
				int modKind = largeBuff[6];
				if (modKind == 5) {
					int len = (int) BYTES_TO_U4(largeBuff + 7);
					memcpy(classPreparePatternList[classPreparePatternCount],
							&largeBuff[11], (size_t) len);
					classPrepareEventIds[classPreparePatternCount] = eventCounter++;
					INT_TO_BYTES(response + HEADER_SIZE, classPrepareEventIds[classPreparePatternCount])
					classPreparePatternCount++;
					largeBuff[11 + len] = '\0';
					printf("class = %s \n", largeBuff + 11);
					if (strcmp("java.lang.Error", (char*)largeBuff + 11) == 0) {
						return -1;
					}
				}
			}
			break;
		default:
			break;
	}

	INT_TO_BYTES(response, size)
	INT_TO_BYTES(response + 4, id)
	//reply flag raised
	response[8] = 0x80;
	//error code success eg. 0
	response[9] = 0;
	response[10] = 0;

	write(newsockfd, response, size);
	return 0;
}

void sendClassPrepare(Thread* thread, Class* clazz, int eventId) {
	unsigned char response[256];
	INT_TO_BYTES(response + 4, idCounter++);
	//reply flag
	response[8] = 0x0;
	//comandset
	response[9] = 64;
	//command
	response[10] = 100;
	int offset = 11;
	//suspend policy
	response[offset++] = 0;
	//event count
	INT_TO_BYTES(response + offset, 1)
	offset += 4;
	//eventKind
	response[offset++] = CLASS_PREPARE_EVENT_KIND;

	INT_TO_BYTES(response + offset, eventId)
	offset += 4;
	PTR_TO_BYTES(response + offset, ((uint64_t)thread->javaThread));
	offset += 8;
	//refTypeTag
	response[offset++] = clazz->isInterface == 0 ? 1 : 2;
	//referenceTypeID
	PTR_TO_BYTES(response + offset, ((uint64_t)clazz));
	offset += 8;

	//JNI signature
	char* signature = getJNISignature(clazz->className);
	INT_TO_BYTES(response + offset, strlen(signature));
	offset +=4;
	memcpy(response + offset, signature, strlen(signature));
	offset += (int) strlen(signature);
	free(signature);

	//class state eg. prepared
	INT_TO_BYTES(response + offset, 2);
	offset += 4;

	INT_TO_BYTES(response, offset)
	write(newsockfd, response, (size_t) offset);
}

int sendSizes(unsigned char buffer[]) {
	size_t size = 11 + 4 * 5;
	unsigned char response[11 + 4 * 5];
	int id = (int) BYTES_TO_U4(buffer + 4);
	INT_TO_BYTES(response, size)
	INT_TO_BYTES(response + 4, id)
	//reply flag raised
	response[8] = 0x80;
	//error code success eg. 0
	response[9] = 0;
	response[10] = 0;


	//fieldIDSize
	INT_TO_BYTES(response + HEADER_SIZE, sizeof(uintptr_t));
	//methodIDSize
	INT_TO_BYTES(response + HEADER_SIZE + 4, sizeof(uintptr_t));
	//objectIDSize
	INT_TO_BYTES(response + HEADER_SIZE + 8, sizeof(uintptr_t));
	//referenceTypeIDSize
	INT_TO_BYTES(response + HEADER_SIZE + 12, sizeof(uintptr_t));
	//frameIDSize
	INT_TO_BYTES(response + HEADER_SIZE + 16, sizeof(uintptr_t));

	write(newsockfd, response,size);
	return 0;
}

char* getQualifiedName(char* className) {
	char* newName = malloc(strlen(className) + 1);
	memcpy(newName, className, strlen(className) + 1);
	int i;
	for (i = 0; i < (int) strlen(className); i++) {
		if (className[i] == '/') {
			newName[i] = '.';
		}
	}
	return newName;
}

char* getJNISignature(char* className) {
	size_t size = strlen(className) + 3;
	char* newName = malloc(strlen(className) + 3);
	memset( newName, 0, size);
	strcat(newName,"L");
	strcat(newName, className);
	strcat(newName, ";");
	return newName;
}

void checkClassPrepare(Thread* thread, Class* clazz) {
	int i;
	for (i = 0; i < classPreparePatternCount; i++) {
		char* pattern = classPreparePatternList[i];
		char* qualifiedName = getQualifiedName(clazz->className);
		if (pattern[strlen(pattern) - 1] == '*') {

		} else if (strcmp(pattern, qualifiedName) == 0) {
			sendClassPrepare(thread, clazz, classPrepareEventIds[i]);
		}
		free(qualifiedName);
	}
}

void initDebugger(uint16_t port) {
	socklen_t clilen;
	unsigned char buffer[16];

	IS_DEBUGGER_READY = 0;

	memset(buffer, 0 , 16);
	struct sockaddr_in serv_addr, cli_addr;
	ssize_t n;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		printf("ERROR opening socket\n");
	}
	memset((char *) &serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	if (bind(sockfd, (struct sockaddr *) &serv_addr,
		  sizeof(serv_addr)) < 0)
	 printf("ERROR on binding");
	listen(sockfd,5);
	clilen = sizeof(cli_addr);
	newsockfd = accept(sockfd,
			 (struct sockaddr *) &cli_addr,
			 &clilen);
	if (newsockfd < 0) printf("ERROR on accept");

	n = write(newsockfd,"JDWP-Handshake",14);
	if (n < 0) {
		printf("Error sending JDWP-Handshake\n");
	}
	n = read(newsockfd,buffer,14);
	if (n < 0) {
		printf("Error reading JDWP-Handshake\n");
	}
	if (strcmp((char*)buffer, "JDWP-Handshake") != 0) {
		printf("Error debugger JDWP-Handshake\n");
		DEBUGGER_ON = 0;
	}

	memset(classPreparePatternList, 0, MAX_CLASS_PATTERNS * MAX_NAME_LENGTH);

	debuggerLoop();
	pthread_create(&debuggerThread, NULL, (void * (*)(void *)) debuggerLoop, (void *) NULL);

}
//	portno = atoi(port);

void debuggerLoop() {

	ssize_t n;
	int i;
	unsigned char buffer[11];
	int run = 1;
	while (run) {
		n = read(newsockfd,buffer,11);
		if (n < 0) {
			printf("Error reading Command\n");
		}
		for (i = 0; i < 11; i++) {
			printf("%x ", buffer[i]);
		}
		printf("\n");
		int flag = dispatchCommand(buffer);
		if (flag < 0) {
			return;
		}

	}
}

void closeDebugger() {
	close(newsockfd);
	close(sockfd);

}
