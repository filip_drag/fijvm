#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <pthread.h>
#include "jni-methods.h"
#include "strmap.h"
#include "util.h"
#include "jni.h"
#include "access-flags.h"
#include "threads.h"
#include "descriptors.h"


#include "java_io_PrintStream.h"
#include "java_lang_System.h"
#include "java_lang_Float.h"
#include "java_lang_Double.h"
#include "java_lang_String.h"
#include "java_lang_Object.h"
#include "java_lang_Throwable.h"
#include "java_lang_Class.h"
#include "java_lang_Thread.h"
#include "java_security_AccessController.h"
#include "sun_misc_Unsafe.h"
#include "java_io_FileDescriptor.h"
#include "java_lang_ClassLoader.h"
#include "sun_misc_VM.h"
#include "sun_reflect_Reflection.h"
#include "java_lang_Runtime.h"
#include "java_io_FileSystem.h"
#include "java_io_UnixFileSystem.h"
#include "sun_reflect_NativeConstructorAccessorImpl.h"


StrMap *methodMap;
typedef void (*VOID_FUNCTION)(Thread*, Value*, Value*);


void getFunctionName(MethodInfo* method, char* name, int length);
int callJniMethod(Thread* threadId, MethodInfo* method, Value* parentStack, int* parentStackPointer) {
	char functionName[1024];
	char ptrBuffer[sizeof(uintptr_t) * 2 + 1];
	unsigned int size = sizeof(uintptr_t) * 2 + 1;
	JavaObject* lockObject;
	getFunctionName(method, functionName, 1024);
	int result = sm_get(methodMap, functionName, ptrBuffer, size);
	if (result == 0) {
		printf("native method not found %s\n", functionName);
		return NATIVE_METHOD_NOT_FOUND;
	}

	if ((method->accessFlags & ACC_SYNCHRONIZED) != 0) {
		if ((method->accessFlags & ACC_STATIC) == 0) {
			Value reference = parentStack[*parentStackPointer - method->parameterCount];
			lockObject = reference.val.pointer;
		} else {
			lockObject = method->clazz->java_Lang_Class;
		}
		pthread_mutex_lock (&lockObject->mutex);
	}


	char* ptr = (char*) decodePointerFromString(ptrBuffer);
	VOID_FUNCTION func = (VOID_FUNCTION) ptr;
	Value retVal;
	func(threadId, &retVal, &parentStack[*parentStackPointer]);

	if ((method->accessFlags & ACC_SYNCHRONIZED) != 0) {
		pthread_mutex_unlock(&lockObject->mutex);
	}

	*parentStackPointer -= method->parameterCount;
	if ((method->accessFlags & ACC_STATIC) == 0) {
		(*parentStackPointer)--;
	}
	if (method->isVoid == 0) {
		(*parentStackPointer)++;
		parentStack[(*parentStackPointer)] = retVal;
	}
	return 0;

}

void getFunctionName(MethodInfo* method, char* name, int length) {
	memset(name, 0, (size_t) length);
	char* className = getClassName(method->clazz);
	char* methodName = getMethodName(method);
	char* descriptor = getUtfStringAtIndex(method->clazz, (size_t)method->descriptorIndex - 1);
	strcat(name, className);
	strcat(name, "_");
	strcat(name, methodName);
	strcat(name, "_");
	strcat(name, descriptor);
}

void initJni() {
	char ptrBuffer[sizeof(uintptr_t) * 2 + 1];
	unsigned int size = sizeof(uintptr_t) * 2 + 1;
	methodMap = sm_new(100);
	if (methodMap == NULL) {
	   printf("methodMap init error\n");
	}
	encodePointerIntoString((void*) Java_java_io_PrintStream_print, ptrBuffer, size);
	sm_put(methodMap, "java/io/PrintStream_print_(C)V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_System_arraycopy, ptrBuffer, size);
	sm_put(methodMap, "java/lang/System_arraycopy_(Ljava/lang/Object;ILjava/lang/Object;II)V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_System_currentTimeMillis, ptrBuffer, size);
	sm_put(methodMap, "java/lang/System_currentTimeMillis_()J", ptrBuffer);

	encodePointerIntoString((void*) Java_java_lang_System_nanoTime, ptrBuffer, size);
	sm_put(methodMap, "java/lang/System_nanoTime_()J", ptrBuffer);

	encodePointerIntoString((void*) Java_java_lang_System_identityHashCode, ptrBuffer, size);
	sm_put(methodMap, "java/lang/System_identityHashCode_(Ljava/lang/Object;)I", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_System_registerNatives, ptrBuffer, size);
	sm_put(methodMap, "java/lang/System_registerNatives_()V", ptrBuffer);


	encodePointerIntoString((void*) Java_sun_misc_Unsafe_registerNatives, ptrBuffer, size);
	sm_put(methodMap, "sun/misc/Unsafe_registerNatives_()V", ptrBuffer);

	encodePointerIntoString((void*) Java_sun_misc_Unsafe_arrayBaseOffset, ptrBuffer, size);
	sm_put(methodMap, "sun/misc/Unsafe_arrayBaseOffset_(Ljava/lang/Class;)I", ptrBuffer);
	encodePointerIntoString((void*) Java_sun_misc_Unsafe_arrayIndexScale, ptrBuffer, size);
	sm_put(methodMap, "sun/misc/Unsafe_arrayIndexScale_(Ljava/lang/Class;)I", ptrBuffer);
	encodePointerIntoString((void*) Java_sun_misc_Unsafe_addressSize, ptrBuffer, size);
	sm_put(methodMap, "sun/misc/Unsafe_addressSize_()I", ptrBuffer);




	encodePointerIntoString((void*) Java_java_lang_Float_floatToRawIntBits, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Float_floatToRawIntBits_(F)I", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Float_intBitsToFloat, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Float_intBitsToFloat_(I)F", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Double_doubleToRawLongBits, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Double_doubleToRawLongBits_(D)J", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Double_longBitsToDouble, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Double_longBitsToDouble_(J)D", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_String_intern, ptrBuffer, size);
	sm_put(methodMap, "java/lang/String_intern_()Ljava/lang/String;", ptrBuffer);


	encodePointerIntoString((void*) Java_java_lang_Object_getClass, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Object_getClass_()Ljava/lang/Class;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Object_wait, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Object_wait_(J)V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Object_notify, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Object_notify_()V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Object_notifyAll, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Object_notifyAll_()V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Object_registrerNatives, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Object_registerNatives_()V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Object_registrerNatives, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Object_hashCode_()I", ptrBuffer);

	encodePointerIntoString((void*) Java_java_lang_Object_clone, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Object_clone_()Ljava/lang/Object;", ptrBuffer);





	encodePointerIntoString((void*) Java_java_lang_Throwable_fillInStackTrace, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Throwable_fillInStackTrace_(I)Ljava/lang/Throwable;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Throwable_getStackTraceDepth, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Throwable_getStackTraceDepth_()I", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Throwable_getStackTraceElement, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Throwable_getStackTraceElement_(I)Ljava/lang/StackTraceElement;", ptrBuffer);

	encodePointerIntoString((void*) Java_java_lang_Class_getName0, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_getName0_()Ljava/lang/String;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_registerNatives, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_registerNatives_()V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_getPrimitiveClass, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_getPrimitiveClass_(Ljava/lang/String;)Ljava/lang/Class;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_getClassLoader0, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_getClassLoader0_()Ljava/lang/ClassLoader;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_desiredAssertionStatus0, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_desiredAssertionStatus0_(Ljava/lang/Class;)Z", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_isAssignableFrom, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_isAssignableFrom_(Ljava/lang/Class;)Z", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_getSuperclass, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_getSuperclass_()Ljava/lang/Class;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_forName0, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_forName0_(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_isInterface, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_isInterface_()Z", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_getDeclaredConstructors0, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_getDeclaredConstructors0_(Z)[Ljava/lang/reflect/Constructor;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_getModifiers, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_getModifiers_()I", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_isArray, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_isArray_()Z", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_getComponentType, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_getComponentType_()Ljava/lang/Class;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_isPrimitive, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_isPrimitive_()Z", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Class_getDeclaredFields0, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Class_getDeclaredFields0_(Z)[Ljava/lang/reflect/Field;", ptrBuffer);







	encodePointerIntoString((void*) Java_java_lang_ClassLoader_registerNatives, ptrBuffer, size);
	sm_put(methodMap, "java/lang/ClassLoader_registerNatives_()V", ptrBuffer);


	encodePointerIntoString((void*) Java_java_lang_Thread_start0, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Thread_start0_()V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Thread_registerNatives, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Thread_registerNatives_()V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Thread_currentThread, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Thread_currentThread_()Ljava/lang/Thread;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Thread_setPriority0, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Thread_setPriority0_(I)V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_lang_Thread_isAlive, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Thread_isAlive_()Z", ptrBuffer);

	encodePointerIntoString((void*) Java_java_security_AccessController_getStackAccessControlContext, ptrBuffer, size);
	sm_put(methodMap, "java/security/AccessController_getStackAccessControlContext_()Ljava/security/AccessControlContext;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_security_AccessController_doPrivileged, ptrBuffer, size);
	sm_put(methodMap, "java/security/AccessController_doPrivileged_(Ljava/security/PrivilegedAction;)Ljava/lang/Object;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_security_AccessController_doPrivileged1, ptrBuffer, size);
	sm_put(methodMap, "java/security/AccessController_doPrivileged_(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;", ptrBuffer);


	encodePointerIntoString((void*) Java_java_io_FileDescriptor_initIDs, ptrBuffer, size);
	sm_put(methodMap, "java/io/FileDescriptor_initIDs_()V", ptrBuffer);

	encodePointerIntoString((void*) Java_sun_misc_VM_initialize, ptrBuffer, size);
	sm_put(methodMap, "sun/misc/VM_initialize_()V", ptrBuffer);

	encodePointerIntoString((void*) Java_sun_reflect_Reflection_getCallerClass, ptrBuffer, size);
	sm_put(methodMap, "sun/reflect/Reflection_getCallerClass_()Ljava/lang/Class;", ptrBuffer);
	encodePointerIntoString((void*) Java_sun_reflect_Reflection_getClassAccessFlags, ptrBuffer, size);
	sm_put(methodMap, "sun/reflect/Reflection_getClassAccessFlags_(Ljava/lang/Class;)I", ptrBuffer);

	encodePointerIntoString((void*) Java_java_lang_Runtime_freeMemory, ptrBuffer, size);
	sm_put(methodMap, "java/lang/Runtime_freeMemory_()J", ptrBuffer);

	encodePointerIntoString((void*) Java_java_io_FileSystem_getFileSystem, ptrBuffer, size);
	sm_put(methodMap, "java/io/FileSystem_getFileSystem_()Ljava/io/FileSystem;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_io_UnixFileSystem_initIDs, ptrBuffer, size);
	sm_put(methodMap, "java/io/UnixFileSystem_initIDs_()V", ptrBuffer);
	encodePointerIntoString((void*) Java_java_io_UnixFileSystem_canonicalize0, ptrBuffer, size);
	sm_put(methodMap, "java/io/UnixFileSystem_canonicalize0_(Ljava/lang/String;)Ljava/lang/String;", ptrBuffer);
	encodePointerIntoString((void*) Java_java_io_UnixFileSystem_getBooleanAttributes0, ptrBuffer, size);
	sm_put(methodMap, "java/io/UnixFileSystem_getBooleanAttributes0_(Ljava/io/File;)I", ptrBuffer);



	encodePointerIntoString((void*) Java_sun_reflect_NativeConstructorAccessorImpl_newInstance0, ptrBuffer, size);
	sm_put(methodMap, "sun/reflect/NativeConstructorAccessorImpl_newInstance0_(Ljava/lang/reflect/Constructor;[Ljava/lang/Object;)Ljava/lang/Object;", ptrBuffer);





}

void destroyJni() {
	if (methodMap != NULL) {
	   sm_delete(methodMap);
	}
}

