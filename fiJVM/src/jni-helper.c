#include "jni-helper.h"
#include "util.h"
#include "printf.h"
#include "cpu.h"
#include "classes.h"
#include "heap.h"
#include "array.h"
#include "type-union.h"
#include "java-object-consts.h"
#include "descriptors.h"
#include "threads.h"

void initializeString(Thread* threadId, JavaObject* stringPointer, const char* strConst, Class* stringClass);

MethodInfo* findMethod(Class* class, const char* name, const char* paramDescriptors, u2 accessFlags) {
	if (class->methodCount == 0) {
		return NULL;
	}
	MethodInfo* methodArray = class->methodArray;
	int i;
	for (i = 0; i < class->methodCount; i++) {
		MethodInfo* info = &methodArray[i];
		u2 nameIndex = info->nameIndex;
		ConstantPoolInfo poolInfo = class->constantPoolArray[nameIndex - 1];
		if (poolInfo.tag != CONSTANT_Utf8) {
			PRINTF("Opps the tag is not UTF8_info\n");
		}
		char* bytes = (char*)poolInfo.utfBytes;
		ConstantPoolInfo poolInfoDesc = class->constantPoolArray[info->descriptorIndex - 1];
		if (poolInfoDesc.tag != CONSTANT_Utf8) {
			PRINTF("Opps the tag is not UTF8_info poolInfoDesc \n");
			return NULL;
		}
		char* descriptor = (char*) poolInfoDesc.utfBytes;
		int b1 = strcmp(name, bytes);
		int b2 = info->accessFlags == accessFlags;
		int b3 = strcmp(paramDescriptors, descriptor);
		PRINTF("%s %s %d\n", name, bytes, b1);
		if (b1 == 0 &&
			b2 &&
			b3 == 0) {
			return info;

		}
	}
	return NULL;
}

MethodInfo* findMethodNoAccessFlags(Class* class, char* name, char* paramDescriptors) {
	if (class->methodCount == 0) {
		return NULL;
	}
	MethodInfo* methodArray = class->methodArray;
	int i;
	for (i = 0; i < class->methodCount; i++) {
		MethodInfo* info = &methodArray[i];
		int b1 = strcmp(name, info->methodName);
		int b3 = strcmp(paramDescriptors, info->descriptor);
		if (b1 == 0 && b3 == 0) {
			return info;

		}
	}
	MethodInfo* info = findVirtualMethod(class->vtable, name, paramDescriptors);
	return info;
}
FieldInfo* findField(Class* class, const char* name, const char* descriptor) {
	size_t i;
	for (i = 0; i < class->fieldCount; i++) {
		FieldInfo* field = &class->fieldArray[i];
		if (strcmp(name, field->name) == 0 &&
				strcmp(descriptor, field->descriptor) == 0) {
			return field;
		}
	}
	return NULL;
}

void fieldValue(Value* value, void* ptr, FieldInfo* field) {
	getRefValue( value, (u1*)ptr + field->offset, field->descriptor);
}

void setField(Value* value, void* ptr, FieldInfo* field) {
	setFieldValue((u1*)ptr + field->offset, value, field->descriptor);
}

JavaObject* charArrayToString(Thread* threadId, char* str) {
	Class* stringClass = findClassByQualifiedName("java/lang/String");
	JavaObject* stringPointer = allocObject(stringClass);
	initializeString(threadId, stringPointer, str, stringClass);
	return stringPointer;
}

void initializeString(Thread* threadId, JavaObject* stringPointer, const char* strConst, Class* stringClass) {
	size_t i, len = strlen(strConst);
	//8 means byte array
	JavaObject* byteArray = allocArray(threadId, ARR_TYPE_CHAR, (u4) len, NULL);
	//copy the bytes aligned to 4 bytes to a char
	u1* ptr = ((u1*) byteArray) + ARRAY_HEADER_SIZE;
	u4* ptrU4 = (u4*) ptr;
	for ( i = 0; i < len; i++) {
		ptrU4[i] = (u4) strConst[i];
	}

	//FieldInfo* field = findField(stringPointer->clazz, "count", "I");
	//Value val;
	//val.val.intVal = (int32_t) len;
	//val.type = INT_JVM;
	//setField(&val, stringPointer, field);

	FieldInfo* field = findField(stringPointer->clazz, "value", "[C");
	Value val;
	val.val.pointer = byteArray;
	val.type = POINTER_JVM;
	setField(&val, stringPointer, field);

}

u4 getArrayLength(JavaObject* ptr) {
	return ptr->extra.length;
}

Class* getClass(JavaObject* ptr) {
	return ptr->clazz;
}

int hasSuperClass(Class* subClass, Class* superClass) {
	size_t j;
	for (j = 0; j < subClass->classChainCount; j++) {
		Class* currClass = subClass->classChain[j];
		if (currClass == superClass) {
			return 1;
		}
	}

	return 0;

}

int hasSuperInterface(Class* subClass, Class* superInterface) {
	size_t j;
	if (superInterface->isInterface == 0) {
		return 0;
	}

	for (j = 0; j < subClass->interfaceChainCount; j++) {
		Class* currClass = subClass->interfaceChain[j];
		if (currClass == superInterface) {
			return 1;
		}
	}

	return 0;

}

int isAssignableFrom(Class* subClass, Class* superClass) {
	return hasSuperClass(subClass, superClass) || hasSuperInterface(subClass, superClass);
}

int isArrayOfRefs(JavaObject* obj) {
	return arrayType(obj) == ARR_TYPE_REF;
}

int arrayNumberOfDimensions(JavaObject* obj) {
	return (int) (obj->flags >> 4);
}

int arrayType(JavaObject* obj) {
	return (int) (obj->flags & 0xF);
}

MethodInfo* findVirtualMethod(VTable* vtable, char* methodName, char* methodDescriptor) {
#define KEY_BUFF_SIZE 1024
	char keyBuffer[KEY_BUFF_SIZE];
	char ptrBuffer[sizeof(uintptr_t) * 2 + 1];
	unsigned int size = sizeof(uintptr_t) * 2 + 1;
	getMethodKeyStr(methodName, methodDescriptor, keyBuffer, KEY_BUFF_SIZE);
	StrMap* sM = vtable->map;

	int result = sm_get(sM,
			keyBuffer,
			ptrBuffer,
			size);
	if (result == 0) {
		//throw Exception method not found
		printf("Method %s not found\n", methodName);
		return NULL;
	}
	void* ptr = decodePointerFromString(ptrBuffer);

	return (MethodInfo*) ptr;
}
