package java.lang;

public class String implements CharSequence {
	
	private char[] chars;
	
	public String(byte[] bytes) {
		this.setChars(String.decodeBytes(bytes));
	}

	public String(char[] buf, int offset, int count) {
		chars = new char[count];
		for (int i = 0, j = offset; i < count; i++, j++) {
			chars[i] = buf[j];
		}
	}

	String(char[] buf, boolean b) {
		this.chars = buf;
	}

	public String(StringBuffer result) {
		// TODO Auto-generated constructor stub
	}
	

	public String(String str) {
		chars = new char[str.length()];
		System.arraycopy(str.getChars(), 0, chars, 0, chars.length);
	}

	private static char[] decodeBytes(byte[] bytes) {
		//now works only for ASCII chars no cyrillic chars
		char[] array = new char[bytes.length];
		for (int i = 0; i < array.length; i++) {
			byte b = bytes[i];
			array[i] = (char) (b & 0xFF);
		}
		return array;
	}

	public char[] getChars() {
		return chars;
	}

	public void setChars(char[] chars) {
		this.chars = chars;
	}

	public int length() {
		if (chars != null) {
			return chars.length;
		}
		return 0;
	}

	public char charAt(int i) {
		return chars[i];
	}

	public static String valueOf(Object obj) {
		return obj.toString();
	}
	
	public boolean startsWith(java.lang.String string, int index) {
		// TODO Auto-generated method stub
		return false;
	}

	public java.lang.String substring(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public java.lang.String replaceFirst(java.lang.String string,
			java.lang.String string2) {
		// TODO Auto-generated method stub
		return null;
	}

	public String trim() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean equals(String string) {
		// TODO Auto-generated method stub
		return false;
	}

	public java.lang.String substring(int i, int j) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public java.lang.CharSequence subSequence(int start, int end) {
		// TODO Auto-generated method stub
		return null;
	}

}
