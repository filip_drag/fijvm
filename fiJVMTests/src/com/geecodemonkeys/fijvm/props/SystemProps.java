package com.geecodemonkeys.fijvm.props;

import java.util.Properties;

public class SystemProps {


	public static Properties getSystemProperties() {
		Properties props = new Properties();
		props.setProperty("java.version", "1.7");
		props.setProperty("java.vendor", "Fito");
		props.setProperty("java.vendor.urljava.home", "http://haha.com");
		props.setProperty("java.vm.specification.version", "1");
		props.setProperty("java.vm.specification.vendor", "1");
		props.setProperty("java.vm.specification.name", "1");
		props.setProperty("java.vm.version", "1");
		props.setProperty("java.vm.vendor", "Fito");
		props.setProperty("java.vm.name", "fiJvm");
		props.setProperty("java.specification.version", "1");
		props.setProperty("java.specification.vendor", "1");
		props.setProperty("java.specification.name", "1");
		props.setProperty("java.class.version", "1");
		props.setProperty("java.class.path", "/home/fi/PROJECTS/eclipse-cdt-workspace/fiJVM/classlib");
		props.setProperty("java.library.path", "/home/fi/PROJECTS/eclipse-cdt-workspace/fiJVM/classlib");
		props.setProperty("java.io.tmpdir", "");
		props.setProperty("java.compiler", "");
		props.setProperty("java.ext.dirs", "");
		props.setProperty("os.name", "Linux");
		props.setProperty("os.arch", "x64");
		props.setProperty("os.version", "Mint 16");
		props.setProperty("file.separator", ":");
		props.setProperty("path.separator", "/");
		props.setProperty("line.separator", "\n");
		props.setProperty("user.name", "");
		props.setProperty("user.home", "");
		props.setProperty("user.dir", "");
		return props;
		
	}
}
