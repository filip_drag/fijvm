package com.geecodemonkeys.fijvm.tests.source;

public class Test35 implements III {

	public static void main(String[] args) {
		III tst = new Test35();
		tst.someCoolMethod("interface_method");
	}

	@Override
	public void someCoolMethod(String someWord) {
		System.out.println(someWord);
	}

}

interface III {
	void someCoolMethod(String someWord);
}
