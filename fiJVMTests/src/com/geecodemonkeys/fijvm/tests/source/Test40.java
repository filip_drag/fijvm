package com.geecodemonkeys.fijvm.tests.source;

public class Test40 {

	public static int counter = 0;
	
	public static void main(String[] args) {
		int threadCount = 4;
		Thread[] thread = new Thread[threadCount];
		for (int i = 0; i < threadCount; i++) {
			thread[i] = new Thread(new Runnable() {

				@Override
				public void run() {
					for (int i = 0; i < 50; i++) {						
						inc();
					}
				}
				
			});
			thread[i].start();
		}
		for (int i = 0; i < threadCount; i++) {
			try {
				thread[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println(counter);
	}
	
	public static synchronized void inc() {
		counter++;
	}

}
