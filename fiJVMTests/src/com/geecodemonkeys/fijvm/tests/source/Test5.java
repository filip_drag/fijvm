package com.geecodemonkeys.fijvm.tests.source;

public class Test5 {
	private static int CONST = -456455;

	public static void main(String[] args) {
		System.out.println(plus5(CONST));
		System.out.println(plus(CONST, 3415343));
	}
	
	public static int plus5(int number) {
		return number + 5;
	}
	
	public static int plus(int number, int number2) {
		return number + number2;
	}

}
