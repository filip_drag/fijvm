package com.geecodemonkeys.fijvm.tests.source;

public class Test13 {

	static float[] someArray = new float[50];
	public static void main(String[] args) {
		float res = 3.0f;
		
		for (int i = 0; i < someArray.length; i++) {
			someArray[i] = i * 5.0f / 45.f - 23.f;
		}
		for (int i = 1; i < someArray.length - 1; i++) {
			if (someArray[i] % 5.0f < 30.0f) {
				res += someArray[i - 1] * someArray[i + 1] / -someArray[i]  ;
				
			} else {
				int a = 4;
				res += (someArray[i - 1] + someArray[i + 1] - (float)a ) / -someArray[i]  ;
			}
		}
		System.out.println(res);
	}

}
