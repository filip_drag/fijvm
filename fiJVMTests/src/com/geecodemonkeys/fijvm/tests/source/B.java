package com.geecodemonkeys.fijvm.tests.source;

public class B extends A {

	public int someMethod(){
		return 4;
	}
	
	public final int bFinalPublic() {
		return 4;
	}
}
