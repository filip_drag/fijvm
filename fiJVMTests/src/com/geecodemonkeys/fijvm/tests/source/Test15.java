package com.geecodemonkeys.fijvm.tests.source;

public class Test15 {

	public static long l1 = 45134;
	public static long l3 = -128;
	public static long l2 = 12543;
	public static void main(String[] args) {
		System.out.println(l3 >>> 3);
		System.out.println(l3 >> 3);
		System.out.println(l1 ^ l2);
		System.out.println(l1 & l2);
		System.out.println(l1 | l2);
		System.out.println(l1 >> 4);
		System.out.println(l1 << 5);
		System.out.println(l1 >>> 3);
		System.out.println(-l1);
		System.out.println(l1 - l2);
		System.out.println(l1 + l2);
		System.out.println(l1 / l2);
		System.out.println(l1 * l2);
		System.out.println(l1 % l2);
		if (l1 < l2) {
			System.out.println('R');
		} else {
			System.out.println('F');
		}

	}

}
