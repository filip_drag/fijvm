package com.geecodemonkeys.fijvm.tests.source;

public class Test6 extends C{
	private int prop1;
	public int prop2;
	
	public Test6(int param1, int param2) {
		prop1 = param1;
		prop2 = param2;
	}
	
	public int getProp1() {
		
		return prop1;
	}
	
	public int getS() {
		
		return s;
	}

	public static void main(String[] args) {
		Test6 tst = new Test6(-25454, -34);
		System.out.println(tst.getProp1());
		System.out.println(tst.getS());
		System.out.println(tst.prop2);
	}

}
class C {
	protected int s = -555;
}
