package com.geecodemonkeys.fijvm.tests.source;

public class Test32 {

	public static void main(String[] args) {
		Test32 t = null;
		try {
			t = new Test32(1);
			
		} catch (Exception e) {
			System.out.println("");
		}
		try {
			new Test32(2);
		} catch (Exception e) {
			System.out.print("ExcpConstr ");
		}
		
		try {
			t.someMethod();
		} catch (Exception e) {
			System.out.println("eee");
		} finally {
			System.out.println("finally");
		}
		
	}
	
	public Test32(int f) throws Exception {
		if (f %2 == 0) {
			throw new Exception();
		}
	}
	
	public void someMethod() throws Exception {
		throw new Exception();
	}

}
