package com.geecodemonkeys.fijvm.tests.source;

public class Test11 {
	
	public static int[] arr = new int[30];

	public static void main(String[] args) {
		for (int i =0; i < arr.length; i++) {
			arr[i] = i + 1;
		}
		int sum = 0 ;
		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		System.out.println(sum);
	}

}
