package com.geecodemonkeys.fijvm.tests.source;

public class Test33 {

	public static void main(String[] args) {

		// nested static class doesn't need instantiation of the outer class
		Test33.NestedStaticClass nestedStatic = new Test33.NestedStaticClass();
		nestedStatic.printMethodNestedStatic();
		// inner class needs instantiation of the outer class
		Test33 outer = new Test33();
		Test33.InnerClass inner = outer.new InnerClass();
		inner.printMethodInner();
	}

	// Static nested class
	public static class NestedStaticClass {
		public void printMethodNestedStatic() {
			System.out.println("inner_static_class");
		}
	}

	// inner (non-static nested) class
	public class InnerClass {
		public void printMethodInner() {
			System.out.println("inner_non_static_class");
		}
	}

}
