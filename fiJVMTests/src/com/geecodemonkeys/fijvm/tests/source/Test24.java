package com.geecodemonkeys.fijvm.tests.source;

public class Test24 {

	static byte[] arr = new byte[10];
	public static void main(String[] args) {
		for (int i = -5, j = 0; i < 5; i ++, j++) {
			arr[j] = (byte) i;
		}
		
		for (int i = 0; i < 10; i ++) {
			System.out.println(arr[i]);
		}
	}

}
