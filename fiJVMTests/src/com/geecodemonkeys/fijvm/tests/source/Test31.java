package com.geecodemonkeys.fijvm.tests.source;

public class Test31 {

	static int i;
	static {
		i = 4;
	}
	public static void main(String[] args) {
		try {
			someMethod();
		} catch (Exception e) {
			System.out.println("Exception..");
		}
		System.out.println("eho");
	}
	
	public static void someMethod() throws Exception {
		throwException();
	}
	public static void throwException() throws Exception {
		if (i == 4) {
			throw new Exception("my_first_exception");
		}
		
	}

}
