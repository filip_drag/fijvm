package com.geecodemonkeys.fijvm.tests.source;

public class Test9 {

	public static void main(String[] args) {
		Test9_A a1 = new Test9_B();
		Test9_A a2 = new Test9_C();
		Test9_A a3 = new Test9_A();
		System.out.println(a1.returnInt());
		System.out.println(a2.returnInt());
		System.out.println(a3.returnInt());
	}

}

class Test9_A {
	
	private int localInt = -11111;
	
	protected int returnInt() {
		return localInt;
	}
	
}

class Test9_B extends Test9_A {
	protected int returnInt() {
		return localInt;
	}
	private int localInt = -222222;
}

class Test9_C extends Test9_A {
	protected int returnInt() {
		return localInt;
	}
	private int localInt = -333333;
}