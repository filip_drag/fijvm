package com.geecodemonkeys.fijvm.tests.source;

public class Test17 implements SomeInterface, SomeInterface3 {

	public static void main(String[] args) {
		Test17 tst = new Test17();
		tst.doStuff();
		tst.someMethod1();
		tst.someMethod3();
		System.out.println(SomeInterface2.SOME_CONST_1);
		System.out.println(SomeInterface3.SOME_CONST_3);
		System.out.println(tst.SOME_INT_CONST);
	}

	@Override
	public void doStuff() {
		System.out.println('A');
	}

	@Override
	public void someMethod1() {
		System.out.println('B');
	}

	@Override
	public void someMethod3() {
		System.out.println('C');
	}

}
