package com.geecodemonkeys.fijvm.tests.source;

public class Test8 {

	public static void main(String[] args) {
		Test8_A a1 = new Test8_B();
		Test8_A a2 = new Test8_C();
		System.out.println(a1.returnInt());
		System.out.println(a2.returnInt());
	}

}

class Test8_A {
	
	protected int returnInt() {
		return -1;
	}
	
}

class Test8_B extends Test8_A {
	protected int returnInt() {
		return -2;
	}
}

class Test8_C extends Test8_A {
	protected int returnInt() {
		return -3;
	}
}