package com.geecodemonkeys.fijvm.tests.source;

public class Test41 {
	
	public static int flag = 0;

	public static void main(String[] args) {
		
		Thread th = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("before wait");
				synchronized(Test41.class) {
					while(flag == 0) {
						try {
							Test41.class.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
					System.out.println("super after wait.");
				}
			}
			
		});
		
		Thread th2 = new Thread(new Runnable() {

			@Override
			public void run() {
				int sum = 0;
				for (int i = 0; i < 500000; i++) {
					sum += i;
				}
				
				synchronized(Test41.class) {
					flag = 1;
					Test41.class.notify();
					System.out.println("after notify.");
				}
			}
			
		});
		th.start();
		th2.start();

	}

}
