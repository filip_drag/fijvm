package com.geecodemonkeys.fijvm.tests.source;

public class Test21 {

	static Test21[] arr = new Test21[10];
	public static void main(String[] args) {
		for (int i = 0; i < 10; i ++) {
			arr[i] = new Test21(i);
		}
		
		for (int i = 0; i < 10; i ++) {
			System.out.println(arr[i].getNumber());
		}
	}
	
	private int number;
	
	public Test21(int n) {
		number = n;
	}
	
	public int getNumber() {
		return number;
	}

}
