package com.geecodemonkeys.fijvm.tests.source;

public class Test14 {

	public static void main(String[] args) {
		int val4 = 34;
		int val3 = 17;
		Test14 tst = new Test14();
		if (tst != null) {
			tst.someMethod(30.0f, 31);
			if (val4 / val3 > 1) {
				tst = null;
			}
			if (tst == null) {
				System.out.println(45);
			}
		}
	}
	
	public void someMethod(float f, int i) {
		if (f > i) {
			System.out.println(-1);
		} else if (f <= i) {
			System.out.println(5);
		} else if (f >= i) {
			System.out.println(10);
		}
	}

}
