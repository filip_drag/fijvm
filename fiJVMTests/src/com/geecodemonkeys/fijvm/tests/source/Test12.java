package com.geecodemonkeys.fijvm.tests.source;

public class Test12 {

	public static void main(String[] args) {
		int someInt = 45;
		byte byteVal = (byte) someInt;
		short shortVal = (short) someInt;
		char charVal = (char) someInt;
		float floatVal = (float) someInt;
		double doubleVal = (double)someInt;
		long longVal = (long) someInt;
		
		System.out.println(byteVal);
		System.out.println(shortVal);
		System.out.println(charVal);
		System.out.println(floatVal);
		System.out.println(doubleVal);
		System.out.println(longVal);

	}

}
