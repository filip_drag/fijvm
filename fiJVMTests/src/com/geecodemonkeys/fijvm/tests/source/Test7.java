package com.geecodemonkeys.fijvm.tests.source;

public class Test7 extends Clazz4 {
	
	private int num2;
	public Test7(int num, int num2) {
		clazz0prop = num;
		this.num2 = num2;
	}

	public static void main(String[] args) {
		Test7 ts1 = new Test7(-34, -732);
		Test7 ts2 = new Test7(-534, -8542);
		System.out.println(ts1.returnProp());
		System.out.println(ts2.returnProp());
		System.out.println(ts2.returnProp(-56));
		System.out.println(ts1.num2);
				
	}

}

class Clazz0 {
	protected int clazz0prop;
}

class Clazz1 extends Clazz0 {
	
	public int returnProp() {
		return clazz0prop;
	}
}

class Clazz2 extends Clazz1 {
	public int returnProp() {
		return clazz0prop + 1;
	}
}

class Clazz3 extends Clazz2 {
	public int returnProp() {
		return clazz0prop + 3;
	}
}

class Clazz4 extends Clazz3 {
	public int returnProp() {
		return clazz0prop + 4;
	}
	
	public int returnProp(int i) {
		return i + 10;
	}
}
