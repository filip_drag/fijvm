package com.geecodemonkeys.fijvm.tests.source;

public class Test16 {

	public static double d1 = -8345.3443;
	public static double d2 = -343.2343;
	public static void main(String[] args) {
		System.out.println(d1 + d2);
		System.out.println(d1 - d2);
		System.out.println(d1 * d2);
		System.out.println(d1 / d2);
		System.out.println(-d1);
		System.out.println(d1 % d2);
		m1(d1, 10);
		if (d1 < d2) {
			System.out.println('S');
		}
	}
	
	static void m1(double d, int i) {
		System.out.println(d);
		System.out.println(i);
	}

}
