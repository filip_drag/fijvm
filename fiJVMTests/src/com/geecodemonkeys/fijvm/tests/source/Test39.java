package com.geecodemonkeys.fijvm.tests.source;

public class Test39 {

	public static void main(String[] args) {
		
		System.out.println(Thread.currentThread().getName());
		
		Thread th = new Thread(new Runnable() {

			@Override
			public void run() {
				throw new RuntimeException("Exception_in_thread");
				
			}
			
		});
		
		th.start();
	}

}
