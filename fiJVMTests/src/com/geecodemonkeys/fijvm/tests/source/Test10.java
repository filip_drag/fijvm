package com.geecodemonkeys.fijvm.tests.source;

public class Test10 {

	public static void main(String[] args) {
		Test10_A a1 = new Test10_B(-111111);
		Test10_A a2 = new Test10_C(-2222222);
		Test10_A a3 = new Test10_A(-333333);
		System.out.println(a1.returnInt());
		System.out.println(a2.returnInt());
		System.out.println(a3.returnInt());
		
		System.out.println(a1.A);
		System.out.println(a2.A);
		System.out.println(a3.A);
	}

}

class Test10_A {
	
	public static int A;
	static {
		System.out.println(-12345);
		A = -314542;
	}
	
	protected int localInt = -11111;
	
	public Test10_A(int val) {
		localInt = val;
	}
	
	protected int returnInt() {
		return localInt;
	}
	
}

class Test10_B extends Test10_A {
	public static int A;
	public Test10_B(int val) {
		super(val);
	}
	
	static {
		System.out.println(-87829051);
		A = -2736;
	}
}

class Test10_C extends Test10_A {
	public static int A;
	public Test10_C(int val) {
		super(val);
	}
	
	static {
		System.out.println(-628572);
		A = -2657295;
	}
}