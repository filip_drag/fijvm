package java.io;

public class PrintStream {

	public  synchronized void println(String str2) {
		print(str2);
		print('\n');
	}

	public void println(byte s) {
		println((int)s);
	}
	
	public void println(int i) {
		String str = Integer.toString(i);
		println(str);
	}
	
	public void println(float f) {
		String str = Float.toString(f);
		println(str);
	}
	
	public void println(double f) {
		String str = Double.toString(f);
		println(str);
	}
	
	public void println(char c) {
		print(c);
		print('\n');
	}
	
	public void println(boolean b) {
		if (b) {
			println("true");
		} else {
			println("false");
		}
	}
	
	
	public void println(short s) {
		println(Short.toString(s));
	}
	
	public void println(long l) {
		println(Long.toString(l));
	}
	
	public native void print(char c);

	public void print(String str2) {
		char[] chars = str2.toCharArray();
		for (int i = 0; i < str2.length(); i++) {
			print(chars[i]);
		}
	}

	public void println(java.lang.Object obj) {
		if (obj == null) {
			println("null");
			return;
		}
		println(obj.toString());
		
	}

	public void println() {
		print('\n');
	}

}
