package sun.security.util;

public final class SecurityConstants {

	public static final java.lang.RuntimePermission STOP_THREAD_PERMISSION = null;
	public static final java.lang.RuntimePermission GET_STACK_TRACE_PERMISSION = null;
	public static final java.lang.RuntimePermission MODIFY_THREADGROUP_PERMISSION = null;
	public static final RuntimePermission CREATE_ACC_PERMISSION = null;
	public static final RuntimePermission GET_COMBINER_PERMISSION = null;

}
